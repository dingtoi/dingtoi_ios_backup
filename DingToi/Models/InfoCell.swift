//
//  InfoCell.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/31/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation

class InfoCell: NSObject {
    var key: String?
    var value: String?
    var imageName: String?
    var isFailed: Bool?
    var items: Items?
    
    init(key: String?, value: String?, imageName: String?, isFailed: Bool?, items: Items?) {
        self.key = key
        self.value = value
        self.imageName = imageName
        self.isFailed = isFailed
        self.items = items
    }
}

public class Items: NSObject {
    var key1: String?
    var value1: String?
    var key2: String?
    var value2: String?
    var key3: String?
    var value3: String?
    
    init(key1: String?, value1: String?, key2: String?, value2: String?, key3: String?, value3: String?) {
        self.key1 = key1
        self.value1 = value1
        self.key2 = key2
        self.value2 = value2
        self.key3 = key3
        self.value3 = value3
    }
}
