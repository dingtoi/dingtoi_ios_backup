//
//  MainVC.swift
//  TemplaceProject
//
//  Created by Tran Viet Thuc on 1/6/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import CoreBluetooth
import AVFoundation

class ScannerViewController: BaseViewController {
    var cbManager: CBCentralManager = CBCentralManager()
    var methodStatusDelegate: MethodStatusDelegate?
    var frameDefaultIphoneX:CGRect?
    
    @IBOutlet weak var _baseScroll: UIScrollView!
    @IBOutlet weak var _baseView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad(true)
        let handler = ScannerController()
        self.cbManager.delegate = self
        var frame = self.frameDefaultIphone!
        if #available(iOS 11.0, *) {
            frame.size.height += UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        } else {
            // Fallback on earlier versions
        }
//        self.checkPermissionCamera()
        let mainView = ScannerDetailView.init(frame: frame, rootVC: self, handler: handler)
        self.methodStatusDelegate = mainView
        handler.delegate = mainView
        handler.dataSource = mainView
        pushView(mainView)
    }
    
    override func rootScrollView() -> UIScrollView {
        return self._baseScroll
    }
    
    override func rootView() -> UIView {
        return self._baseView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkPermissionCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if UIImagePickerController.isCameraDeviceAvailable(.rear), authStatus != .authorized {
            // Create Alert
            let alert = UIAlertController(title: "Camera", message: "Camera access is absolutely necessary to use this app", preferredStyle: .alert)

            // Add "OK" Button to alert, pressing it will bring you to the settings app
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                } else {
                    // Fallback on earlier versions
                }
            }))
            // Show the alert with animation
            self.present(alert, animated: true)
        }
    }
}

extension ScannerViewController: CBCentralManagerDelegate {
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("didFailToConnect")
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("didConnect")
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if #available(iOS 10.0, *) {
            self.methodStatusDelegate?.bluetoothScanned(status: central.state)
        } else {
            
        }
    }
}
