//
//  RightMenuVC.swift
//  TemplaceProject
//
//  Created by Tran Viet Thuc on 1/6/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

class RightMenuVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    var frameDefaultIphoneX:CGRect?
    
    @IBOutlet weak var _baseScroll: UIScrollView!
    @IBOutlet weak var _baseView: UIView!
    
    let titles: [String] = ["Home", "Calendar"]
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.frame = CGRect(x: 180, y: (self.view.frame.size.height - CGFloat(54*self.titles.count))/2.0, width: self.view.frame.size.width, height: CGFloat(54*self.titles.count) )
        tableView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.isOpaque = false
        tableView.backgroundColor = UIColor.clear
        tableView.backgroundView = nil
        tableView.bounces = false
        return tableView
    }()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 21)
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.text  = self.titles[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        switch indexPath.row {
//        case 0:
//            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: MainVC())
//            sideMenuViewController?.hideMenuViewController()
//            break
//        case 1:
//            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: MainVC())
//            sideMenuViewController?.hideMenuViewController()
//            break
//        default:
//            break
//        }
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad(false)
        _baseView.backgroundColor = UIColor.clear
        _baseView.addSubview(tableView)
    }
    
    override func rootScrollView() -> UIScrollView {
        return self._baseScroll
    }
    
    override func rootView() -> UIView {
        return self._baseView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
