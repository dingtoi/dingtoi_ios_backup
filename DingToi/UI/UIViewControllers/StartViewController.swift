//
//  StartViewController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/1/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class StartViewController: BaseViewController {
    var frameDefaultIphoneX:CGRect?
    
    @IBOutlet weak var _baseScroll: UIScrollView!
    @IBOutlet weak var _baseView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad(true)
        let handler = TransactionController(rootVC: self)
        let startView  = StartView.init(frame: self.frameDefaultIphone!, rootVC: self, handler: handler)
        pushView(startView)
    }

    override func rootScrollView() -> UIScrollView {
        return self._baseScroll
    }
    
    override func rootView() -> UIView {
        return self._baseView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
