//
//  ScreenViewController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/15/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class ScreenViewController: BaseViewController {

    var frameDefaultIphoneX:CGRect?
    
    @IBOutlet weak var _baseScroll: UIScrollView!
    @IBOutlet weak var _baseView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad(true, isFull: true)
        var frame = self.frameDefaultIphone!
        if #available(iOS 11.0, *) {
            frame.size.height += UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            frame.size.height += UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
        } else {
            // Fallback on earlier versions
        }
//        view.backgroundColor = .blue
        let screenView  = ScreenView.init(frame: frame, rootVC: self)
        pushView(screenView)
//        let cavasView = CavasView()
//        cavasView.frame = self.view.frame
//        self.view.addSubview(cavasView)
    }

    override func rootScrollView() -> UIScrollView {
        return self._baseScroll
    }
    
    override func rootView() -> UIView {
        return self._baseView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
