//
//  PhysicalGradingView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/13/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class PhysicalGradingView: BaseView {
    @IBOutlet weak var _lbTitle1: UILabel!
    @IBOutlet weak var _lbTitle2: UILabel!
    @IBOutlet weak var _lbTitle3: UILabel!
    @IBOutlet weak var _btnNext: UIButton!
    @IBOutlet weak var _tableView: UITableView!
    var indexPathSelected: IndexPath = IndexPath(row: 0, section: 0)
    var data: Array<PhysicalCellModel> = []
    
    @IBAction func pressNext(_ sender: Any) {
        self.rootVC?.pushView(DiamondRatingView(frame: self.frame, rootVC: self.rootVC!))
    }
    @IBAction func pressBack(_ sender: Any) {
        self.rootVC?.popView()
    }
    init(frame: CGRect, rootVC: BaseViewController) {
        super.init(frame: frame)
        self.rootVC = rootVC
        xibSetup(frame: frame)
        self.initTableView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setControlView() {
        self._lbTitle1.setTextSpacingBy(value: 2)
        self._lbTitle2.setTextSpacingBy(value: 2)
        self._lbTitle3.setTextSpacingBy(value: 2)
        if let isIphoneX = self.rootVC?.checkIphoneX(), isIphoneX {
            self._tableView.frame.height += 10
            self._btnNext.frame.height -= 10
            self._btnNext.frame.origin.y += 10
        }
    }
    
    func initTableView() {
        self.initData()
        self._tableView.delegate = self
        self._tableView.dataSource = self
        
        self._tableView.register(UINib(nibName: "PhysicalGradingCell", bundle: nil), forCellReuseIdentifier: "physicalGradingCell")
    }
    
    func initData() {
        self.data.removeAll()
        self.data.append(PhysicalCellModel(title: "GRADE A", value: ["Almost new", "Very few or imperceptible scatches", "Very mininal usage"], isCheck: true))
        self.data.append(PhysicalCellModel(title: "GRADE B", value: ["Almost Grade A but a few more visible minor scratches"], isCheck: false))
        self.data.append(PhysicalCellModel(title: "GRADE C", value: ["Obvious signs of visible wear", "Many scratches, but no dents or cracks anywhere"], isCheck: false))
        self.data.append(PhysicalCellModel(title: "GRADE D", value: ["Visible cracks on screen and/or casing dents", "Deep scratches and scuffs"], isCheck: false))
    }
    
}

extension PhysicalGradingView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 140
        case 1:
            return 100
        default:
            return 120
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "physicalGradingCell") as! PhysicalGradingCell
        cell.loadData(data: self.data[indexPath.row])
        cell.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected \(indexPath.section) - \(indexPath.row)")
        let indexPathSelectedOld = self.indexPathSelected
        self.indexPathSelected = indexPath
        self.data[indexPathSelectedOld.row].isCheck = false
        self.data[indexPath.row].isCheck = true
        tableView.reloadRows(at: [indexPathSelectedOld, indexPath], with: .none)
    }
}
