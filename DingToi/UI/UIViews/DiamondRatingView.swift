//
//  DiamondRatingView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/14/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit
import Cosmos

class DiamondRatingView: BaseView {
    @IBOutlet weak var _lbTitle1: UILabel!
    @IBOutlet weak var _lbTitle2: UILabel!
    @IBOutlet weak var _lbTitle3: UILabel!
    @IBOutlet weak var _lbDeviceName: UILabel!
    @IBOutlet weak var _lbTransactionCode: UILabel!
    @IBOutlet weak var _lbValueRate: UILabel!
    @IBOutlet weak var _viewRate: CosmosView!
    @IBOutlet weak var _btnSubmit: UIButton!
    @IBOutlet weak var _imgShadow: UIImageView!
    
    @IBAction func pressBack(_ sender: Any) {
        self.rootVC?.popView()
    }
    @IBAction func pressSubmit(_ sender: Any) {
        Share.isAgain = true
        UIApplication.shared.windows.first?.rootViewController = UINavigationController(rootViewController: StartViewController())
    }
    init(frame: CGRect, rootVC: BaseViewController) {
        super.init(frame: frame)
        self.rootVC = rootVC
        xibSetup(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setControlView() {
        self._lbTitle1.setTextSpacingBy(value: 2)
        self._lbTitle2.setTextSpacingBy(value: 2)
        self._lbTitle3.setTextSpacingBy(value: 2)
        self._btnSubmit.roundedBottom(size: 10)
        self._lbTransactionCode.text = Share.transactionCode
        self._lbDeviceName.text = UIDevice.modelName
    }
}
