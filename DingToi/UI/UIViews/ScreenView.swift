//
//  ScreenView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/15/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class ScreenView: BaseView {
    var line = [CGPoint]()
    var isTouch = false
    var location = CGPoint(x: 0, y: 0)
    @IBOutlet weak var _imgTouch: UIImageView!
    @IBOutlet weak var _lbDescription: UILabel!
    @IBOutlet weak var _btnNext: UIButton!
    @IBOutlet weak var _btnAgain: UIButton!
    @IBAction func pressNext(_ sender: Any) {
        self.line.removeAll()
        self.setNeedsDisplay()
        let scannerViewController = ScannerViewController()
        self.rootVC?.pushToViewController(viewController: scannerViewController)
    }
    @IBAction func pressAgain(_ sender: Any) {
        self._imgTouch.image = UIImage(named: "ic_touch_app")
//        self.isTouched = false
        self._lbDescription.isHidden = false
        self._btnNext.isHidden = true
        self._btnAgain.isHidden = true
        self.backgroundColor = .white
        self.setNeedsDisplay()
    }
    
    init(frame: CGRect, rootVC: BaseViewController) {
        super.init(frame: frame)
        self.backgroundColor = .white
        self.rootVC = rootVC
        xibSetup(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touchesBegan")
        let touch : UITouch! = touches.first! as UITouch
        location = touch.location(in: self)
        if location.x < self._imgTouch.frame.maxX && location.x > self._imgTouch.frame.minX && location.y > self._imgTouch.frame.minY && location.y < self._imgTouch.frame.maxY {
            self.isTouch = true
            self._lbDescription.isHidden = true
            self._imgTouch.isHidden = true
        }
        self._btnNext.isHidden = true
//        self._imgTouch.center = center
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touchesMoved")
        if self.isTouch {
            let touch : UITouch! =  touches.first! as UITouch
            location = touch.location(in: self)
//            self._imgTouch.center = location
            self.line.append(location)
            self.setNeedsDisplay()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touchesEnded")
        if self.isTouch {
//            self.isTouch = false
//            self.line.removeAll()
            self._imgTouch.center = self.center
//            self._imgTouch.isHidden = false
//            self._btnAgain.isHidden = false
            self._btnNext.isHidden = false
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touchesCancelled")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        context.setStrokeColor(UIColor.blue.cgColor)
        context.setLineWidth(self._imgTouch.frame.width)
//        context.setLineWidth(70)
        context.setLineCap(.butt)
        for (i, p) in self.line.enumerated() {
            if i == 0 {
                context.move(to: p)
            } else {
                context.addLine(to: p)
            }
        }
        
        context.strokePath()
    }
    
    override func setControlView() {
        if let isIphoneX = self.rootVC?.checkIphoneX(), isIphoneX {
//            self._btnNext.frame.height -= 10
            self._btnNext.frame.origin.y += 10
        }
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow == nil {
            self._imgTouch.center = self.center
            self._imgTouch.isHidden = false
            self.backgroundColor = .white
            self._lbDescription.isHidden = false
            self.isTouch = false
            self._btnNext.isHidden = true
        }
    }
}
