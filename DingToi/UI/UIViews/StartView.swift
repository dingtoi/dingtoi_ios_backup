//
//  StartView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/1/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class StartView: BaseView {
    var handler: TransactionHandler?
    @IBOutlet weak var _lbWelcome: UILabel!
    @IBOutlet weak var _btnStart: UIButton!
    @IBAction func pressStart(_ sender: Any) {
        let inputCodeView = InputCodeView(frame: UIScreen.main.bounds, rootVC: self.rootVC!, handler: self.handler!)
        self.rootVC?.pushView(inputCodeView)
    }
    init(frame: CGRect, rootVC: BaseViewController, handler: TransactionHandler) {
        super.init(frame: frame)
        self.rootVC = rootVC
        xibSetup(frame: frame)
        self.handler = handler
    }
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        xibSetup(frame: frame)
//    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func setControlView() {
        if Share.isAgain {
//            self._btnStart.frame.width = self.frame.width/2
            self._lbWelcome.text = "THANK YOU"
            self._btnStart.setTitle("SCANNER AGAIN", for: .normal)
        } else {
//            self._btnStart.frame.width = self.frame.width/3
            self._lbWelcome.text = "WELCOME DINGTOI"
            self._btnStart.setTitle("START", for: .normal)
        }
        self._btnStart.titleLabel?.font.withSize(FontSizes.FONT_MENU_CELL_TITLE)
        self._btnStart.frame.midX = self.frame.midX
        self._btnStart.layer.cornerRadius = self._btnStart.frame.height/2
        self._lbWelcome.setTextSpacingBy(value: 5)
    }

}
