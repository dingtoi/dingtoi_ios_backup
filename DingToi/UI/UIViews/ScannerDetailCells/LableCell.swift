//
//  LableCell.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/31/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class LableCell: UITableViewCell {

    @IBOutlet weak var _lbKey: UILabel!
    @IBOutlet weak var _lbValue: UILabel!
    @IBOutlet weak var _img: UIImageView!
    @IBOutlet weak var _viewContent: UIView!
    @IBOutlet weak var _imgStatus: UIImageView!
    @IBOutlet weak var _imgShadow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self._viewContent.layer.cornerRadius = 10
//        self._viewShadow.layer.cornerRadius = 10
//        self._viewShadow.layer.shadowOffset = CGSize(width: 0, height: 1)
//        self._viewShadow.frame.y = self._viewContent.frame.minY - 0.2
//        self._viewShadow.frame.height = self._viewContent.frame.height + 1.5
//        self._viewContent.layer.shadowOpacity = 0.7
//        self._viewContent.layer.shadowOffset = CGSize(width: 1, height: 1)
//        self._viewContent.layer.shadowColor = #colorLiteral(red: 0.08235294118, green: 0.09803921569, blue: 0.2784313725, alpha: 1)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(infoCell: InfoCell) {
        self._lbKey.text = infoCell.key
        self._lbKey.sizeToFit()
//        self._img.frame.midY = self._lbKey.frame.midY
        self._lbValue.text = infoCell.value
        self._lbValue.sizeToFit()
        _img.image = UIImage(named: infoCell.imageName ?? "")
        if let isFailed = infoCell.isFailed, isFailed {
//            self._viewContent.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
            self._imgShadow.image = UIImage(named: "bg_not_working_1")
            
            self._imgStatus.image = UIImage(named: "failed")
        } else {
//            self._viewContent.backgroundColor = .clear
            self._imgShadow.image = UIImage(named: "bg_working_1")
            self._imgShadow.isHidden = false
            self._imgStatus.image = UIImage(named: "checked")
        }
        self._lbKey.frame.y = self._img.frame.midY - self._lbKey.frame.height
        self._lbValue.frame.height = self._lbKey.frame.height
        self._lbValue.frame.y = self._img.frame.midY
        self._imgStatus.frame.origin.x = self._lbKey.frame.maxX + 10
        self._imgStatus.frame.midY = self._lbKey.frame.midY
    }
    
    static var className: String {
        return String(describing: self)
    }
}
