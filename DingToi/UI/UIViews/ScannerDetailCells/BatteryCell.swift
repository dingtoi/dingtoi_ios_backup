//
//  BatteryCell.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/6/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class BatteryCell: UITableViewCell {
    @IBOutlet weak var _viewContent: UIView!
    @IBOutlet weak var _img: UIImageView!
    @IBOutlet weak var _lbKey: UILabel!
    @IBOutlet weak var _lbValue: UILabel!
    @IBOutlet weak var _imgCheckAll: UIImageView!
    @IBOutlet weak var _lbKey1: UILabel!
    @IBOutlet weak var _lbValue1: UILabel!
    @IBOutlet weak var _lbValue2: UILabel!
    @IBOutlet weak var _lbKey2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self._viewContent.layer.cornerRadius = 10
        self._viewContent.layer.shadowOpacity = 0.7
        self._viewContent.layer.shadowOffset = CGSize(width: 1, height: 1)
        self._viewContent.layer.shadowColor = #colorLiteral(red: 0.08235294118, green: 0.09803921569, blue: 0.2784313725, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(infoCell: InfoCell) {
        self._lbKey.text = infoCell.key
        self._lbKey.sizeToFit()
        self._lbValue.text = infoCell.value
        self._lbValue.sizeToFit()
        self._imgCheckAll.frame.origin.x = self._lbKey.frame.maxX + 10
        self._lbKey1.sizeToFit()
        self._lbValue1.sizeToFit()
        self._lbKey2.sizeToFit()
        self._lbValue2.sizeToFit()
    }
    
    static var className: String {
        return String(describing: self)
    }
}
