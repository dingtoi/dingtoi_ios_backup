//
//  CameraCell.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/6/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class CameraCell: UITableViewCell {
    @IBOutlet weak var _viewContent: UIView!
    @IBOutlet weak var _lbKey: UILabel!
    @IBOutlet weak var _img: UIImageView!
    @IBOutlet weak var _imgCheckAll: UIImageView!
    @IBOutlet weak var _lbValue: UILabel!
    @IBOutlet weak var _lbName1: UILabel!
    @IBOutlet weak var _imgCheck1: UIImageView!
    @IBOutlet weak var _lbName2: UILabel!
    @IBOutlet weak var _imgCheck2: UIImageView!
    @IBOutlet weak var _lbLine1: UILabel!
    @IBOutlet weak var _lbLine2: UILabel!
    @IBOutlet weak var _viewShadow: UIView!
    @IBOutlet weak var _imgShadow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self._viewContent.layer.cornerRadius = 10
//        self._viewShadow.layer.cornerRadius = 10
//        self._viewShadow.layer.shadowOffset = CGSize(width: 0, height: 1)
//        self._viewShadow.frame.y = self._viewContent.frame.minY - 0.2
//        self._viewShadow.frame.height = self._viewContent.frame.height + 1.5
//        self._viewContent.layer.shadowOpacity = 0.7
//        self._viewContent.layer.shadowOffset = CGSize(width: 1, height: 1)
//        self._viewContent.layer.shadowColor = #colorLiteral(red: 0.08235294118, green: 0.09803921569, blue: 0.2784313725, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(infoCell: InfoCell, isValue: Bool = true) {
        if let imageName = infoCell.imageName {
            self._img.image = UIImage(named: imageName)
        }
//        self._lbValue.isHidden = !isValue
        
        self._lbKey.text = infoCell.key
        self._lbKey.sizeToFit()
        if (isValue) {
            self._lbValue.isHidden = false
            self._lbKey.frame.origin.y = self._img.frame.minY - 5
            self._lbValue.frame.origin.y = self._lbKey.frame.maxY + 5
        } else {
            self._lbValue.isHidden = true
            self._lbKey.frame.midY = self._img.frame.midY
            
        }
        self._imgCheckAll.frame.midY = self._lbKey.frame.midY
//        self._lbValue.text = infoCell.value
//        self._lbValue.sizeToFit()
        self._lbName1.text = infoCell.items?.key1
        self._lbName1.sizeToFit()
        self._lbName2.text = infoCell.items?.key2
        self._lbName2.sizeToFit()
        
        if infoCell.items?.value1 == "true" && infoCell.items?.value2 == "true" {
            self._lbValue.text = infoCell.value
            self._lbValue.isHidden = true
            self._lbKey.frame.midY = _img.frame.midY
            self._imgCheckAll.image = UIImage(named: "checked")
//            self._viewContent.backgroundColor = .white
            self._imgShadow.image = UIImage(named: "bg_working_1")
            self._lbLine1.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            self._lbLine2.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
        } else {
            self._lbValue.isHidden = false
//            self._lbKey.frame.midY = _img.frame.midY
            if infoCell.items?.value1 == "true" {
                self._lbValue.text = "\(self._lbName2.text ?? "") not working"
            } else if infoCell.items?.value2 == "true" {
                self._lbValue.text = "\(self._lbName1.text ?? "") not working"
            } else {
                self._lbValue.text = "\(self._lbName1.text ?? ""), \(self._lbName2.text ?? "") not working"
            }
//            self._viewContent.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
            self._imgShadow.image = UIImage(named: "bg_not_working_1")
            self._lbLine1.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.8117647059, blue: 0.8117647059, alpha: 1)
            self._lbLine2.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.8117647059, blue: 0.8117647059, alpha: 1)
            self._imgCheckAll.image = UIImage(named: "failed")
        }
        self._lbValue.sizeToFit()
        if let value1 = infoCell.items?.value1, value1 == "true" {
            self._imgCheck1.image = UIImage(named: "checked")
        } else {
            self._imgCheck1.image = UIImage(named: "failed")
        }
        if let value2 = infoCell.items?.value2, value2 == "true" {
            self._imgCheck2.image = UIImage(named: "checked")
        } else {
            self._imgCheck2.image = UIImage(named: "failed")
        }
        
        if infoCell.value == nil || infoCell.value == "" {
            self._lbKey.frame.midY = self._img.frame.midY
            self._lbValue.isHidden = true
        } else {
            self._lbValue.isHidden = false
            self._lbKey.frame.y = self._img.frame.midY - self._lbKey.frame.height
            self._lbValue.frame.height = self._lbKey.frame.height
            self._lbValue.frame.y = self._img.frame.midY
        }
        
        self._imgCheckAll.frame.midY = self._lbKey.frame.midY
        self._imgCheckAll.frame.origin.x = self._lbKey.frame.maxX + 10
    }
    
    static var className: String {
        return String(describing: self)
    }
}
