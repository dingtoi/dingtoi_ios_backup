//
//  ScannerDetailView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/28/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit
import CoreBluetooth
import SystemConfiguration

class ScannerDetailView: BaseView {
    var hanlder: ScannerHandler?
    var cbManager: CBCentralManager!
    var data: Array<Array<InfoCell>> = Array()
    let networkStatus = NetworkStatus.shared
    let fingerStatus = FingerprintStatus.shared
    let batteryInfo = BatteryInfo.shared
    var bluetoothSupport: Bool?
//    var indexPathSelected: IndexPath?

    @IBOutlet weak var _btnConfirm: UIButton!
    @IBOutlet weak var _imgPhone: UIImageView!
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _lbDeviceName: UILabel!
    @IBOutlet weak var _lbTransactionCode: UILabel!
    @IBOutlet weak var _lbTitle: UILabel!
    
    @IBAction func pressBack(_ sender: Any) {
        self.rootVC?.popView()
    }
    @IBAction func pressConfirm(_ sender: Any) {
        print("Confirm")
        self.rootVC?.pushView(PhysicalGradingView(frame: self.frame, rootVC: self.rootVC!))
    }
    
    init(frame: CGRect, rootVC: BaseViewController, handler: ScannerHandler) {
        super.init(frame: frame)
        self.rootVC = rootVC
        self.hanlder = handler
        xibSetup(frame: frame)
        self.initTableView()
        self._lbTransactionCode.text = Share.transactionCode ?? ""
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow != nil {
            print("move")
            self.scanner()
        } else {
            print("remove")
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func setControlView() {
        if let isIphoneX = self.rootVC?.checkIphoneX(), isIphoneX {
            self._tableView.frame.height += 10
            self._btnConfirm.frame.height -= 10
            self._btnConfirm.frame.origin.y += 10
        }
    }
    
    
    func initTableView() {
        self._tableView.delegate = self
        self._tableView.dataSource = self
        self._tableView.setContentOffset(CGPoint.zero, animated: true)
        _tableView.sectionHeaderHeight = 0
        _tableView.sectionFooterHeight = 0
        _tableView.separatorColor = UIColor.clear
        _tableView.backgroundColor = UIColor.clear
        _tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: _tableView.frame.size.width, height: 0.001))
        
        self._tableView.register(UINib(nibName: LableCell.className, bundle: nil), forCellReuseIdentifier: "infoCell")
        self._tableView.register(UINib(nibName: HardwareCell.className, bundle: nil), forCellReuseIdentifier: "hardwareCell")
        self._tableView.register(UINib(nibName: BatteryCell.className, bundle: nil), forCellReuseIdentifier: "batteryCell")
        self._tableView.register(UINib(nibName: CameraCell.className, bundle: nil), forCellReuseIdentifier: "cameraCell")
    }
    
}

extension ScannerDetailView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
//            case FunctionName.HARDWARE where self.indexPathSelected != nil && self.indexPathSelected?.section == 0 && self.indexPathSelected?.row == FunctionName.HARDWARE:
            case FunctionName.HARDWARE:
                return 200
//            case FunctionName.CAMERA where self.indexPathSelected != nil && self.indexPathSelected?.section == 0 && self.indexPathSelected?.row == FunctionName.CAMERA:
            case FunctionName.CAMERA:
                return 170
            case FunctionName.VOLUME:
                return 170
            default:
                return 90
            }
        case 2:
            return 170
        default:
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self._tableView.frame.width, height: 60))
        let label = PaddingLabel(frame: CGRect(x: view.frame.x, y: view.frame.minY + 10, width: view.frame.width, height: view.frame.height - 15))
        switch section {
        case 1:
            label.text = "WIFI WORKING"
        case 2:
            label.text = "ADVANCED"
        default:
            break
        }
//        view.backgroundColor = .red
//        label.textColor = .white
        label.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        switch indexPath.section {
        case 0:
            switch indexPath.row {
//            case FunctionName.HARDWARE where self.indexPathSelected != nil && self.indexPathSelected?.section == indexPath.section && self.indexPathSelected?.row == indexPath.row:
            case FunctionName.HARDWARE:
                cell = tableView.dequeueReusableCell(withIdentifier: "hardwareCell", for: indexPath) as! HardwareCell
                (cell as! HardwareCell).loadData(infoCell: data[indexPath.section][indexPath.row])
//            case FunctionName.CAMERA where self.indexPathSelected != nil && self.indexPathSelected?.section == indexPath.section && self.indexPathSelected?.row == indexPath.row:
            case FunctionName.CAMERA:
                cell = tableView.dequeueReusableCell(withIdentifier: "cameraCell", for: indexPath) as! CameraCell
                (cell as! CameraCell).loadData(infoCell: data[indexPath.section][indexPath.row])
            case FunctionName.VOLUME:
                cell = tableView.dequeueReusableCell(withIdentifier: "cameraCell", for: indexPath) as! CameraCell
                (cell as! CameraCell).loadData(infoCell: data[indexPath.section][indexPath.row])
            default:
                cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath) as! LableCell
                (cell as! LableCell).loadData(infoCell: data[indexPath.section][indexPath.row])
            }
        case 2:
            if indexPath.row == FunctionName.SPECIAL_APP {
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "cameraCell")
                (cell as! CameraCell).loadData(infoCell: data[indexPath.section][indexPath.row], isValue: false)
            }
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath) as! LableCell
            (cell as! LableCell).loadData(infoCell: data[indexPath.section][indexPath.row])
        }
//        cell.tag = indexPath.row
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelected section \(indexPath.section) row \(indexPath.row)")
        self._tableView.reloadRows(at: [indexPath], with: .fade)
//        let indexPathSelectedOld = self.indexPathSelected
//        if self.indexPathSelected?.section == indexPath.section && self.indexPathSelected?.row == indexPath.row {
//            self.indexPathSelected = nil
//        } else {
//            self.indexPathSelected = indexPath
//        }
//        if let indexPathSelected = indexPathSelectedOld {
//            self._tableView.reloadRows(at: [indexPathSelected, indexPath], with: .fade)
//        } else {
//            self._tableView.reloadRows(at: [indexPath], with: .fade)
//        }
    }
}

extension ScannerDetailView: MethodStatusDelegate {
    
    
    @available(iOS 10.0, *)
    func bluetoothScanned(status: CBManagerState) {
        print("delegate bluetooth")
        if status == .unknown || status == .unsupported {
            self.bluetoothSupport = false
        } else {
            self.bluetoothSupport = true
        }
        
        self.data[1][FunctionName.BLUETOOTH].value = "Bluetooth \(self.bluetoothSupport! ? "working" : "not working")"
        self.data[1][FunctionName.BLUETOOTH].isFailed = !self.bluetoothSupport!
        self._tableView.reloadData()
        switch status {
        case .unknown:
            print("Bluetooth unknown")
        case .unsupported:
            print("Bluetooth unsupport")
        case .unauthorized:
            print("Bluetooth unauthorized")
        case .resetting:
            print("Bluetooth resetting")
        case .poweredOff:
            print("Bluetooth poweredOff")
        case .poweredOn:
            print("Bluetooth poweredOn")
        default:
            print("default")
        }
    }
}

extension ScannerDetailView: ScannerProtocol {
    func scanner() {
        self.hanlder?.scanner()
    }
}

extension ScannerDetailView: ScannerDataSource, ScannerDelegate {
    func scanned(deviceInfo: DeviceInfo , data: Array<Array<InfoCell>>) {
        self._lbDeviceName.text = deviceInfo.deviceName
        self.data = data
        self._tableView.reloadData()
    }
}
