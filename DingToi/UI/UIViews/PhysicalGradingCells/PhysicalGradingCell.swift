//
//  PhysicalGradingCell.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/14/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class PhysicalGradingCell: UITableViewCell {
    @IBOutlet weak var _background: UIImageView!
    @IBOutlet weak var _imgCheckBox: UIImageView!
    @IBOutlet weak var _lbTitle: UILabel!
    @IBOutlet weak var _lbValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadData(data: PhysicalCellModel) {
        self._lbTitle.text = data.title
        if data.isCheck {
            self._imgCheckBox.image = UIImage(named: "icon_checkbox")
        } else {
            self._imgCheckBox.image = UIImage(named: "icon_non_checkbox")
        }
//        self._lbTitle.frame.midY = self._imgCheckBox.frame.midY
        self._lbValue.font.withSize(FontSizes.FONT_TEXT)
        self._lbValue.text = data.value.joined(separator: "\n")
    }
}
