//
//  InputCode.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/1/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class InputCodeView: BaseView, TransactionProtocol {
    @IBOutlet weak var _lbTitle1: UILabel!
    @IBOutlet weak var _lbTitle2: UILabel!
    @IBOutlet weak var _lbTitle3: UILabel!
    @IBOutlet weak var _btnNext: UIButton!
    @IBOutlet weak var _tfTransactionCode: UITextField!
    
    var handler: TransactionHandler?
    
    @IBAction func pressNext(_ sender: Any) {
        if let transactionCode = self._tfTransactionCode.text, transactionCode != "" {
            self.checkTransactionCode(transactionCode: transactionCode)
        } else {
            self.rootVC?.showDialog("Warning", "Transaction code is required")
        }
    }
    init(frame: CGRect, rootVC: BaseViewController, handler: TransactionHandler) {
        super.init(frame: frame)
        self.rootVC = rootVC
        xibSetup(frame: frame)
        self.handler = handler
        self.handler?.delegate = self
        self.handler?.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setControlView() {
        self._lbTitle1.setTextSpacingBy(value: 2)
        self._lbTitle2.setTextSpacingBy(value: 2)
        self._lbTitle3.setTextSpacingBy(value: 2)
        self._tfTransactionCode.frame.height = 50
        self._btnNext.layer.cornerRadius = self._btnNext.frame.height/2
        #if DEBUG
        self._tfTransactionCode.text = "8nDWEVqPe"
        #endif
    }

    func checkTransactionCode(transactionCode: String) {
        self.handler?.checkTransactionCode(transactionCode: transactionCode)
    }
}

extension InputCodeView: TransactionDelegate, TransactionDataSource {
    func onSuccess(response: TransactionCodeResponse) {
        if (response.isSuccess) {
            let screenViewController = ScreenViewController()
            Share.transactionCode = _tfTransactionCode.text
            self.rootVC?.pushToViewController(viewController: screenViewController)
        } else {
            self.rootVC?.showDialog("Error", "Incorrect transaction code")
        }
    }
    
    func onError(error: ResponseErrorModel) {
        self.rootVC?.showDialog("Error", error.error?.code ?? "")
    }
    
    
}
