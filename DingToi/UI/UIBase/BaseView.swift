//  Created by boys vip on 5/29/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit
import Persei
import SkyFloatingLabelTextField

class BaseView: UIView, UITextFieldDelegate, MenuViewDelegate {
    
    var rootVC: BaseViewController?
    var stackV: StackView?
    
    //Called when the user click on the view (outside the UITextField).
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    //Called when 'return' key pressed. return NO to ignore.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //Show keyboard move layout with position viewcontroller --> frameCurrentFocus
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.getRootViewController()?.frameCurrentFocus = textField //get frame when focous textField
    }
    
    func className() -> String {
        return String(describing: type(of: self)) // NOTES: --> xib Name from self
    }
    
    func xibSetup(frame: CGRect) {
        let view = UINib(nibName: className(), bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = frame
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        setControlView()
        applyFont()
        adjustView()
        setActionView()
        self.initObserverLanguage()
    }
    
    func setControlView() {}
    func setActionView() {}
    func adjustView() {}
    func applyFont() {}
    func updateUI() {}
    func updateUIEdit() {}
    
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
    func getRootViewController() -> BaseViewController? {
        return self.rootVC
    }
    
    func showMessage(_ message: String) {
        self.getRootViewController()?.showDialog(String.kErrorMessageTitle, message)
    }
    
    func setBorderButton(_ button: UIButton) {
        button.layer.borderWidth = 2.0
        button.layer.cornerRadius = 5
        button.backgroundColor = UIColor.init(red: 0.80, green: 0.00, blue: 0.00, alpha: 1.0)
        button.layer.borderColor = UIColor.init(red: 0.80, green: 0.00, blue: 0.00, alpha: 1.0).cgColor
        button.layer.masksToBounds = true
    }
    
    func setPlaceHolder(_ txt:UITextField, _ title:String) {
        let placeHolderAttributes = [
            NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
            NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
        txt.attributedPlaceholder = NSAttributedString(string: title, attributes:placeHolderAttributes)
    }
    
    func setPlaceHolder(_ txt:UITextField, _ title:String,_ color: UIColor,_ font: UIFont) {
        let placeHolderAttributes = [
            NSAttributedStringKey.foregroundColor: color,
            NSAttributedStringKey.font : font]
        txt.attributedPlaceholder = NSAttributedString(string: title, attributes:placeHolderAttributes)
    }
    
    func setFocus(_ textField: UITextField,_ labelTop:UILabel) {
        if let temp = textField.text?.count {
            if temp > 0 {
                labelTop.isHidden = false
            }else{
                labelTop.isHidden = true
            }
        }else{
            labelTop.isHidden = true
        }
    }
    
    private var loadingView: LoadingView = LoadingView(frame: CGRect(origin: .zero, size: CGSize.init(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)))
    public func showProgressOnView(){
        loadingView.tag = 999989
        addSubview(loadingView)
    }
    
    public func hideProgressOnView(){
        if let viewWithTag = viewWithTag(999989) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    //Validation
    func isValidated() -> Bool {
        return true
    }
    
    func isValidateRequire(_ args: Any...) -> Bool {
        var countError: Int = 0
        for item in args {
            if item is UITextField {
                let object = item as? CustomTextField
                if !CheckTextFieldIsValue(textField: object) {
                    countError = countError + 1
                }
            } else if item is ButtonDropDownWidget {
                let object = item as? ButtonDropDownWidget
                if !CheckDropDownList(value: object?.selectedValue, button: object!) {
                    countError = countError + 1
                }
            }
        }
        
        return countError > 0 ? false : true
    }
    
    func isValidateRequireStyle2(_ args: Any...) -> Bool {
        var countError: Int = 0
        for item in args {
            if item is UITextField {
                let object = item as? CustomTextField
                if !CheckTextFieldIsValueStyle2(textField: object) {
                    countError = countError + 1
                }
            } else if item is ButtonDropDownWidget {
                let object = item as? ButtonDropDownWidget
                if !CheckDropDownList(value: object?.selectedValue, button: object!) {
                    countError = countError + 1
                }
            } 
        }
        
        return countError > 0 ? false : true
    }
    
    func isValidatePhoneFormat(_ args: Any...) -> Bool {
        var countError: Int = 0
        for item in args {
            if item is UITextField {
                let object = item as? CustomTextField
                if !CheckPhoneFormat(textField: object) {
                    countError = countError + 1
                }
            }
        }
        
        return countError > 0 ? false : true
    }
    
    func isValidatePhoneTableFormat(_ args: Any...) -> Bool {
        var countError: Int = 0
        for item in args {
            if item is UITextField {
                let object = item as? CustomTextField
                if !CheckPhoneTableFormat(textField: object) {
                    countError = countError + 1
                }
            }
        }
        
        return countError > 0 ? false : true
    }
    
    func isValidateEmailFormat(_ args: Any...) -> Bool {
        var countError: Int = 0
        for item in args {
            if item is UITextField {
                let object = item as? CustomTextField
                if !CheckEmailFormat(object) {
                    countError = countError + 1
                }
            }
        }
        
        return countError > 0 ? false : true
    }
    
    func CheckTextFieldIsValue(textField: CustomTextField?) -> Bool {
        if !( (textField?.text?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            //let placeHolderAttributes = [
            //    NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
            //    NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            //textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            textField?.textValidate = ""
            textField?.setIsValidate(true)
            return true
        }else{
            //let placeHolderAttributes = [
            //    NSAttributedStringKey.foregroundColor: UIColor.red,
            //    NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            //textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            textField?.textValidate = "Is required."
            textField?.setIsValidate(false)
            return false
        }
    }
    
    func CheckTextFieldIsValueStyle2(textField: CustomTextField?) -> Bool {
        if !( (textField?.text?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            textField?.attributedPlaceholder = NSAttributedString(string: "", attributes:placeHolderAttributes)
            //textField?.textValidate = ""
            //textField?.setIsValidate(true)
            return true
        }else{
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#FF0000"),
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            textField?.attributedPlaceholder = NSAttributedString(string: "Is required.", attributes:placeHolderAttributes)
            //textField?.textValidate = "Invalid input value!"
            //textField?.setIsValidate(false)
            return false
        }
    }
    
    func CheckTextFieldIsValue(textField: UITextField?) -> Bool {
        if !( (textField?.text?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT_IN_LB_AND_TXT_OF_INFOR)!]
            textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            return true
        }else{
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.red,
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT_IN_LB_AND_TXT_OF_INFOR)!]
            textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            return false
        }
    }
    
    func CheckPhoneFormat(textField: UITextField?) -> Bool {
        textField?.layoutIfNeeded()
        if !( (textField?.text?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT_IN_LB_AND_TXT_OF_INFOR)!]
            textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            if ( (textField?.text?.replacingOccurrences(of: " ", with: "").isPhoneNumber) ?? false) {
                textField?.textColor = UIColor.black
                return true
            }else{
                let temp = textField?.text
                textField?.text = ""
                textField?.textColor = UIColor.red
                textField?.text = temp
                showMessage(String.kPhoneFormat)
                return false
            }
        }else{
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.red,
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT_IN_LB_AND_TXT_OF_INFOR)!]
            textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            return false
        }
    }
    
    func CheckPhoneTableFormat(textField: UITextField?) -> Bool {
        textField?.layoutIfNeeded()
        if !( (textField?.text?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT_IN_LB_AND_TXT_OF_INFOR)!]
            textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            if ( (textField?.text?.replacingOccurrences(of: " ", with: "").isPhoneTableNumber) ?? false) {
                textField?.textColor = UIColor.black
                return true
            }else{
                let temp = textField?.text
                textField?.text = ""
                textField?.textColor = UIColor.red
                textField?.text = temp
                showMessage(String.kPhoneFormat)
                return false
            }
        }else{
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.red,
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT_IN_LB_AND_TXT_OF_INFOR)!]
            textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            return false
        }
    }
    
    func CheckEmailFormat(_ textField: UITextField?) -> Bool {
        textField?.layoutIfNeeded()
        if !( (textField?.text?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT_IN_LB_AND_TXT_OF_INFOR)!]
            textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            if (textField?.text?.replacingOccurrences(of: " ", with: "").isEmail)!{
                textField?.textColor = UIColor.black
                return true
            }else{
                let temp = textField?.text
                textField?.text = ""
                textField?.textColor = UIColor.red
                textField?.text = temp
                showMessage(String.kEmailFormat)
                return false
            }
            
        }else {
            let placeHolderAttributes = [
                NSAttributedStringKey.foregroundColor: UIColor.red,
                NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT_IN_LB_AND_TXT_OF_INFOR)!]
            textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            return false
        }
    }
    
    func CheckDropDownList(value:String?, button:ButtonDropDownWidget) -> Bool {
        if !( (value?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            button.setValidate()
            return true
        }else{
            //button.setUnValidate("Is required.")
            button.setUnValidate()
            return false
        }
    }
    
    func FormatNumberPhone(_ value:String?) -> String? {
        if let temp = value {
            if temp.count > 4 {
                let begin = temp.substring(location: 0, length: 4)
                let end = temp.substring(location: 4, length: temp.count - 4)
                return ((begin ?? "") + " " + (end ?? ""))
            }
        }
        return value
    }
    
    func CheckPhoneFormat(textField: CustomTextField?) -> Bool {
        textField?.layoutIfNeeded()
        if !( (textField?.text?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            //let placeHolderAttributes = [
            //    NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
            //    NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            //textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            textField?.textValidate = ""
            textField?.setIsValidate(true)
            
            if ( (textField?.text?.replacingOccurrences(of: " ", with: "").isPhoneNumber) ?? false) {
                //textField?.textColor = UIColor.black
                textField?.textValidate = ""
                textField?.setIsValidate(true)
                return true
            }else{
                //let temp = textField?.text
                //textField?.text = ""
                //textField?.textColor = UIColor.red
                //textField?.text = temp
                textField?.textValidate = "The number of wrong format phone!"
                textField?.setIsValidate(false)
                //showMessage(String.kPhoneFormat)
                return false
            }
        }else{
            //let placeHolderAttributes = [
            //    NSAttributedStringKey.foregroundColor: UIColor.red,
            //    NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            //textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            textField?.textValidate = "Is required."
            textField?.setIsValidate(false)
            return false
        }
    }
    
    func CheckPhoneTableFormat(textField: CustomTextField?) -> Bool {
        textField?.layoutIfNeeded()
        if !( (textField?.text?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            //let placeHolderAttributes = [
            //    NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
            //    NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            //textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            textField?.textValidate = ""
            textField?.setIsValidate(true)
            
            if ( (textField?.text?.replacingOccurrences(of: " ", with: "").isPhoneTableNumber) ?? false) {
                //textField?.textColor = UIColor.black
                textField?.textValidate = ""
                textField?.setIsValidate(true)
                return true
            }else{
                //let temp = textField?.text
                //textField?.text = ""
                //textField?.textColor = UIColor.red
                //textField?.text = temp
                textField?.textValidate = "The number of wrong format phone!"
                textField?.setIsValidate(false)
                //showMessage(String.kPhoneFormat)
                return false
            }
        }else{
            //let placeHolderAttributes = [
            //    NSAttributedStringKey.foregroundColor: UIColor.red,
            //    NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            //textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            textField?.textValidate = "Is required."//"Invalid input value!"
            textField?.setIsValidate(false)
            return false
        }
    }
    
    func CheckEmailFormat(_ textField: CustomTextField?) -> Bool {
        textField?.layoutIfNeeded()
        if !( (textField?.text?.replacingOccurrences(of: " ", with: "").isBlank) ?? true) {
            //let placeHolderAttributes = [
            //    NSAttributedStringKey.foregroundColor: UIColor.rgb(fromHexString: "#2B2B2B"),
            //    NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            //textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            textField?.textValidate = ""
            textField?.setIsValidate(true)
            
            if (textField?.text?.replacingOccurrences(of: " ", with: "").isEmail)!{
                //textField?.textColor = UIColor.black
                textField?.textValidate = ""
                textField?.setIsValidate(true)
                return true
            }else{
                //let temp = textField?.text
                //textField?.text = ""
                //textField?.textColor = UIColor.red
                //textField?.text = temp
                textField?.textValidate = "Email wrong format!"
                textField?.setIsValidate(false)
                //showMessage(String.kEmailFormat)
                return false
            }
        }else {
            //let placeHolderAttributes = [
            //    NSAttributedStringKey.foregroundColor: UIColor.red,
            //    NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: FontSizes.FONT_TEXT)!]
            //textField?.attributedPlaceholder = NSAttributedString(string: (textField?.placeholder) ?? "", attributes:placeHolderAttributes)
            textField?.textValidate = "Is required."
            textField?.setIsValidate(false)
            return false
        }
    }
    //Validation ./
    
    
    //---------- Language case 1
    var listLanguage_View = [UIView]()
    
    func initObserverLanguage() {
        self.setupLanguage()
        // Register ObserverLanguage
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerDidChangeLanguage), name: NSNotification.Name(rawValue: "MobiAppsLanguages"), object: nil)
    }
    
    @objc func observerDidChangeLanguage(_ notification: Notification) {
        print("Receiver ObserverLanguage --> \(Language.getLanguageName(Language.getCurrentLanguageCode()))")
        self.setupLanguage()
    }
    
    deinit {
        // Stop listening notification
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MobiAppsLanguages"), object: nil);
    }
    
    func setupLanguage() {
        self.listLanguage_View.removeAll()
        self.getLanguageFor(self)
        
        for i in 0..<listLanguage_View.count {
            if let label = listLanguage_View[i] as? UILabel {
                if Language.getTextWithKey(label.getKey_Lang()) != "" {
                    label.text = Language.getTextWithKey(label.getKey_Lang())
                }
            } else if let textField = listLanguage_View[i] as? UITextField {
                if Language.getTextWithKey(textField.getKey_Lang()) != "" {
                    textField.placeholder = Language.getTextWithKey(textField.getKey_Lang())
                }
            } else if let button = listLanguage_View[i] as? UIButton {
                if Language.getTextWithKey(button.getKey_Lang()) != "" {
                    button.setTitle(Language.getTextWithKey(button.getKey_Lang()), for: .normal)
                }
            }
        }
    }
    
    func getLanguageFor(_ parentView: UIView) {
        for view in parentView.subviews {
            if let label = view as? UILabel {
                listLanguage_View.append(label)
            } else if let textField = view as? UITextField {
                listLanguage_View.append(textField)
            } else if let button = view as? UIButton {
                listLanguage_View.append(button)
            } else {
                self.getLanguageFor(view)
            }
        }
    }
    //---------- Language case 1 ./
    
    //---------- Menu
    //Style1
//    func showMenuStyle1() {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        
//        let transition = CATransition()
//        transition.duration = 0.5
//        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transition.type = kCATransitionMoveIn //kCATransitionMoveIn - kCATransitionPush
//        transition.subtype = kCATransitionFromLeft
//        
//        var frm = self.rootVC?.frameDefaultIphone ?? UIScreen.main.bounds
//        //let heightStatusBar = UIApplication.shared.statusBarFrame.size.height/2
//        if UIDevice.current.screenType != .iPhoneX {
//            frm = CGRect.init(x: 0, y: 0, width: frm.width, height: frm.height)
//        }
//        
//        let menuView = Menu_View.init(frame: frm, delegate: self)
//        menuView.rootVC = self.rootVC
//        
//        let view:UIView = UIView.init(frame: UIScreen.main.bounds)
//        view.backgroundColor = UIColor.clear
//        view.addSubview(menuView)
//        view.tag = Constants.TAG_MENU_VIEW
//        view.layer.add(transition, forKey: "kCATransition")
//        //appDelegate.window?.layer.add(transition, forKey: "kCATransition")
//        appDelegate.window!.endEditing(true)
//        appDelegate.window!.addSubview(view)
//    }
    
    func selectedIndex(index: Int){
        print("Click Menu index: \(index)")
        if index == 0 { //SummaryReport
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        }else if index == 1 { //PropertyNotebook
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        }else if index == 2 { //MyLeads
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        }else if index == 3 { //MyDeals
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        }else if index == 4 { //PropertyDicrectory
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        }else if index == 5 { //MyCalendar
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        }else if index == 6 { //ReportView
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        }else {
            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        }
    }
    
    //Style2
    enum TypeMenu {
        case LEFT
        case RIGHT
    }
    
    func showMenuStyle2(_ type: TypeMenu) {
        if type == .LEFT {
            self.rootVC?.presentLeftMenuViewController()
        }else if type == .RIGHT {
            self.rootVC?.presentRightMenuViewController()
        }
    }
    
    
    //Style3
    fileprivate var _imageViewMenu: UIImageView?
    fileprivate var _titleMenu: UILabel?
    fileprivate var _menu: MenuView!
    fileprivate var _listItemsMenu: [MenuItem] = []
    
    func showMenuStyle3(_ titleMenu: UILabel?,_ imageView: UIImageView?,_ tableView: UITableView,_ selectedIndex: Int? = 0) {
        let backgroundColor = UIColor(red: 50.0 / 255.0, green: 49.0 / 255.0, blue: 73.0 / 255.0, alpha: 1.0)
        let highlightedBackgroundColor = UIColor.green
        
        var m1 = MenuItem.init(image: UIImage(named: "tempicon.png")!, highlightedImage: UIImage(named: "tempicon.png")!)
        m1.backgroundColor = backgroundColor
        m1.highlightedBackgroundColor = highlightedBackgroundColor
        var m2 = MenuItem.init(image: UIImage(named: "sign_out.png")!, highlightedImage: UIImage(named: "sign_out.png")!)
        m2.backgroundColor = backgroundColor
        m2.highlightedBackgroundColor = highlightedBackgroundColor
        var m3 = MenuItem.init(image: UIImage(named: "tempicon.png")!, highlightedImage: UIImage(named: "tempicon.png")!)
        m3.backgroundColor = backgroundColor
        m3.highlightedBackgroundColor = highlightedBackgroundColor
        var m4 = MenuItem.init(image: UIImage(named: "sign_out.png")!, highlightedImage: UIImage(named: "sign_out.png")!)
        m4.backgroundColor = backgroundColor
        m4.highlightedBackgroundColor = highlightedBackgroundColor
        
        self._listItemsMenu.removeAll()
        self._listItemsMenu.append(m1)
        self._listItemsMenu.append(m2)
        self._listItemsMenu.append(m3)
        self._listItemsMenu.append(m4)
        
        _menu = {
            let menu = MenuView()
            menu.delegate = self
            menu.items = self._listItemsMenu
            return menu
        }()
        
        //_menu.backgroundImage = UIImage(named: "tooltip2black.png")
        _menu.backgroundColor = UIColor.groupTableViewBackground
        _menu.selectedIndex = selectedIndex
        _titleMenu = titleMenu
        _titleMenu?.text = modelTypeMenu.description
        _imageViewMenu = imageView
        _imageViewMenu?.image = modelTypeMenu.image
        tableView.addSubview(_menu)
    }
    
    func hideMenuStyle3() {
        self._menu.setRevealed(_menu.revealed, animated: true)
    }
    
    fileprivate var modelTypeMenu: ContentType = .music {
        didSet {
            self._titleMenu?.text = modelTypeMenu.description
            if self.rootVC?.isViewLoaded ?? false {
                let center: CGPoint = {
                    let itemFrame = _menu.frameOfItem(at: _menu.selectedIndex!)
                    let itemCenter = CGPoint(x: itemFrame.midX, y: itemFrame.midY)
                    var convertedCenter = _imageViewMenu?.convert(itemCenter, from: _menu)
                    convertedCenter?.y = 0
                    return convertedCenter ?? CGPoint.zero
                }()
                if let layer = self._imageViewMenu?.layer {
                    let transition = CircularRevealTransition(layer: layer, center: center)
                    transition.start()
                }
                self._imageViewMenu?.image = self.modelTypeMenu.image
            }
        }
    }
    
    func menu(_ menu: MenuView, didSelectItemAt index: Int) {
        self.modelTypeMenu = self.modelTypeMenu.next(index)
    }
    //---------- Menu ./
    
    func autoLogoutIfOtherDeviceLogin() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.rootVC = appDelegate.rootNavigationController.visibleViewController as? BaseViewController
        
        self.rootVC?.showDialogOK(Language.getTextWithKey("poplogout.4"), Language.getTextWithKey("poplogout.5"), completionOK: { () -> (Void) in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.switchLoginNC()
        })
    }
    
}
