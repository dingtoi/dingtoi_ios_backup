//  Created by boys vip on 5/29/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit
import DeviceKit
import ContactsUI
import Contacts
import CoreData
import CoreBluetooth

class BaseViewController: UIViewController, UIScrollViewDelegate {
    var SIZE_TABBAR_HEIGHT: CGFloat = 0.0
    var frameDefaultIphone:CGRect?
    var stack : NSMutableArray = []
    var cache : NSMutableArray = []
    
    public var scrollView: UIScrollView?
    public var frameCurrentFocus: UIView?
    
    func pushView(_ object : UIView, isRemove: Bool = true) {
        stack.insert(object, at: 0)
        if isRemove {
            self.clearSubView(rootView())
        }
        rootView().addSubview(object)
    }
    
    func popView(isSave: Bool = false) {
        if stack.count > 0 {
            if isSave {
                cache.insert(stack[0], at: 0)
            }
            stack.removeObject(at: 0)
            //get top object
            if stack.count > 0 {
                let topObject = stack[0] as! UIView
                self.clearSubView(rootView())
                rootView().addSubview(topObject)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func popView(_ toIndex : Int, isSave: Bool = false) {
        while toIndex < stack.count - 1 {
            popView(isSave: isSave)
        }
    }
    
    func popView<T>(myType: T.Type, isSave: Bool = false) {
        var isExitsView = false
        var toIndex = 0
        for i in 0..<stack.count {
            if stack[i] is T {
                isExitsView = true
                toIndex = stack.count - i - 1
                break
            }
        }
        if isExitsView {
            popView(toIndex, isSave: isSave)
        }
    }
    
    func getViewStack(_ toIndex : Int) -> UIView? {
        if toIndex < stack.count {
            let topObject = stack[toIndex] as? UIView
            return topObject
        }
        return nil
    }
    
    func getViewCache(_ toIndex : Int) -> UIView? {
        if toIndex < cache.count {
            let topObject = cache[toIndex] as? UIView
            return topObject
        }
        return nil
    }
    
    func rootScrollView() -> UIScrollView {
        return UIScrollView.init(frame: self.view.frame)
    }
    
    func rootView() -> UIView {
        return self.view
    }
    
    func viewDidLoad(_ removeView: Bool = true, isFull: Bool = false) {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationController?.navigationBar.backItem?.title = ""
        //NSLocalizedString("navigation.button.back.title", comment: "")
        
        //Remove all view in rootView
        if removeView {
            self.clearSubView(self.rootView())
        }
        if !isFull {
            //Add constraint Safe Area
            if #available(iOS 9.0, *), checkIphoneX() {
                self.addConstraintIphoneX(self.rootScrollView(), self.view)
                self.addConstraintIphoneX(self.rootView(), self.view)
            }
        }
        
        //Get frame default
        self.frameDefaultIphone = self.getFrameIphone(frameEdit: self.rootView().frame, showStatusBar: false)
        
        self.rootScrollView().delegate = self
        self.rootScrollView().sizeToFit()
        self.rootScrollView().showsVerticalScrollIndicator = false
        self.rootScrollView().showsHorizontalScrollIndicator = false
        self.Responder(scrollView: self.rootScrollView())
    }
    
    func clearSubView(_ view: UIView) {
        for item in view.subviews{
            item.removeFromSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private var loadingView: LoadingView = LoadingView(frame: CGRect(origin: .zero, size: CGSize.init(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)))
    private var loadingProgressView: LoadingProgressView = LoadingProgressView.init(frame: UIScreen.main.bounds)
    private var loadingProgressCancelView: LoadingProgressCancelView = LoadingProgressCancelView.init(frame: UIScreen.main.bounds)
    
    func Responder(scrollView: UIScrollView) {
        self.scrollView = scrollView
        if #available(iOS 11.0, *) {
            self.scrollView?.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let tabHeight : CGFloat = 60.0
            let keyboardRectangle = keyboardFrame.cgRectValue
            var keyboardHeight = keyboardRectangle.height
            
            if let currentPositionY = frameCurrentFocus?.frame.origin.y, let currentPositionHeight = frameCurrentFocus?.frame.height, let height = self.scrollView?.contentSize.height{
                let checkPosition = height - currentPositionY - currentPositionHeight
                print("Current Position : frameCurrentFocus_Y = \(currentPositionY) -- ScrollView_Height : \(height) -- Check Position : \(checkPosition) -- Height KeyBoard: \(keyboardHeight)")
                if checkPosition <= keyboardHeight {
                    
                    if keyboardHeight > tabHeight {
                        keyboardHeight = keyboardHeight - tabHeight
                    }
                    self.scrollView?.contentSize = CGSize(width: (self.scrollView?.contentSize.width)!, height: (self.scrollView?.frame.size.height)! + keyboardHeight)
                    var offset = self.scrollView?.contentOffset
                    offset?.y = keyboardHeight //+ 40.0
                    self.scrollView?.setContentOffset(offset!, animated: true)
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        self.scrollView?.contentSize = CGSize(width: (self.scrollView?.contentSize.width)!, height: (self.scrollView?.frame.size.height)!)
        var offset = self.scrollView?.contentOffset
        offset?.y = 0
        self.scrollView?.setContentOffset(offset!, animated: true)
    }
    
    func autoScrollCellTableView(_ view: UIView,_ cell: UITableViewCell) {
        let viewFocus = UIView.init(frame: CGRect.init(
            x: view.frame.origin.x,
            y: view.frame.origin.y + (cell.parentTableView?.frame.origin.y ?? 0.0),
            width: view.frame.width,
            height: view.frame.height))
        self.frameCurrentFocus = viewFocus
    }
    
    func autoScrollCellCollectionView(_ view: UIView,_ cell: UICollectionViewCell) {
        let viewFocus = UIView.init(frame: CGRect.init(
            x: view.frame.origin.x,
            y: view.frame.origin.y + (cell.parentCollectionView?.frame.origin.y ?? 0.0),
            width: view.frame.width,
            height: view.frame.height))
        self.frameCurrentFocus = viewFocus
    }
    
    //Progress
    public func showProgress(){
        loadingView.tag = 999999
        self.view.addSubview(loadingView)
    }
    
    public func hideProgress(){
        if let viewWithTag = self.view.viewWithTag(999999) {
            viewWithTag.removeFromSuperview()
        }
    }
    //Progress ./
    
    //Progress Dialog
    public func showProgressDialog(value: String){
        loadingProgressView.tag = 888888
        loadingProgressView.updateStatus(value: value)
        self.view.addSubview(loadingProgressView)
    }
    
    public func updateProgressDialog(value: String){
        if let viewWithTag = self.view.viewWithTag(888888) {
            (viewWithTag as? LoadingProgressView)?.updateStatus(value: value)
        }
    }
    
    public func checkProgressDialog() -> Bool{
        if self.view.viewWithTag(888888) != nil {
            return true
        }
        return false
    }
    
    public func hideProgressDialog(){
        if let viewWithTag = self.view.viewWithTag(888888) {
            viewWithTag.removeFromSuperview()
        }
    }
    //Progress Dialog ./
    
    //Progress Cancel
    public func showProgressCancel(value: String){
        loadingProgressCancelView.tag = 777777
        loadingProgressCancelView.updateStatus(value: value)
        self.view.addSubview(loadingProgressCancelView)
    }
    
    public func updateProgressCancel(value: String){
        if let viewWithTag = self.view.viewWithTag(777777) {
            (viewWithTag as? LoadingProgressCancelView)?.updateStatus(value: value)
        }
    }
    
    public func checkProgressCancel() -> Bool{
        if self.view.viewWithTag(777777) != nil {
            return true
        }
        return false
    }
    
    public func hideProgressCancel(){
        if let viewWithTag = self.view.viewWithTag(777777) {
            viewWithTag.removeFromSuperview()
        }
    }
    //Progress Cancel ./
    
    
    func showDialog(_ title: String,_ message: String) {
        let attributedTitle = NSAttributedString(string: title, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        let attributedMessage = NSAttributedString(string: message, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let backView = alert.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        alert.setValue(attributedTitle, forKey: "attributedTitle") //Title
        alert.setValue(attributedMessage, forKey: "attributedMessage") //Message
        alert.view.tintColor = UIColor.black // Button
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        //        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        //           print("Hủy")
        //            self.navigationController?.popToRootViewController(animated: true)
        //        }
        //        alert.addAction(cancelAction)
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDialog(_ title: String,_ message: String, rootVC: UIViewController?) {
        let attributedTitle = NSAttributedString(string: title, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        let attributedMessage = NSAttributedString(string: message, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let backView = alert.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        alert.setValue(attributedTitle, forKey: "attributedTitle") //Title
        alert.setValue(attributedMessage, forKey: "attributedMessage") //Message
        alert.view.tintColor = UIColor.black // Button
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        //        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        //           print("Hủy")
        //            self.navigationController?.popToRootViewController(animated: true)
        //        }
        //        alert.addAction(cancelAction)
        alert.addAction(OKAction)
        rootVC?.present(alert, animated: true, completion: nil)
    }
    
    func showDialogOK(_ title: String,_ message: String, completionOK: (@escaping () -> (Void))) {
        let attributedTitle = NSAttributedString(string: title, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        let attributedMessage = NSAttributedString(string: message, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let backView = alert.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        alert.setValue(attributedTitle, forKey: "attributedTitle") //Title
        alert.setValue(attributedMessage, forKey: "attributedMessage") //Message
        alert.view.tintColor = UIColor.black // Button
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            completionOK()
        }
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDialogQuestion(_ title: String,_ message: String, completionOK: (@escaping () -> (Void)), completionCancel: (@escaping () -> (Void))) {
        let attributedTitle = NSAttributedString(string: title, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        let attributedMessage = NSAttributedString(string: message, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let backView = alert.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        alert.setValue(attributedTitle, forKey: "attributedTitle") //Title
        alert.setValue(attributedMessage, forKey: "attributedMessage") //Message
        alert.view.tintColor = UIColor.black // Button
        
        let cancelAction = UIAlertAction(title: "No", style: .default) { (action) in
            completionCancel()
        }
        alert.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            completionOK()
        }
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func RemoveViewWithID(_ view: UIView ,_ id: Int) {
        view.viewWithTag(id)?.removeFromSuperview()
    }
    
    @available(iOS 9.0, *)
    func addConstraintIphoneX(_ newView: UIView, _ view: UIView) {
        newView.translatesAutoresizingMaskIntoConstraints = false
        
        let margins = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            newView.leadingAnchor.constraint(equalTo: margins.leadingAnchor), //Trong trái
            newView.trailingAnchor.constraint(equalTo: margins.trailingAnchor) //Trong phải
            ])
        
        if #available(iOS 11.0, *) {
            let guide = view.safeAreaLayoutGuide //safeAreaLayoutGuide
            newView.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true //Trong phải
            newView.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true //Trong trái
            newView.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true //Dưới pos trên
            newView.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true //Dưới pos dưới
            
            newView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        } else {
            NSLayoutConstraint(item: newView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: newView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: newView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: newView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
            
            newView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        }
    }
    
    func checkIphoneX() -> Bool {
        if Device.current.isOneOf(UIDevice.groupOfLargeX){
            return true
        }
        return false
    }
    
    func getFrameIphone(frameEdit: CGRect, showStatusBar: Bool) -> CGRect {
        let heightStatusBar = UIApplication.shared.statusBarFrame.size.height
        if checkIphoneX() {
            if #available(iOS 11.0, *) {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                if let window = appDelegate.window {
                    let safeAreaBottom = window.safeAreaInsets.bottom
                    let safeAreaLeft = window.safeAreaInsets.left
                    let safeAreaRight = window.safeAreaInsets.right
                    let safeAreaTop = window.safeAreaInsets.top
                    print("SafeArea --> Bottom:\(safeAreaBottom) - Left:\(safeAreaLeft) - Right:\(safeAreaRight) - Top:\(safeAreaTop)")
                    if self.SIZE_TABBAR_HEIGHT != 0.0 {
                        return CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height + heightStatusBar - safeAreaBottom - safeAreaTop - self.SIZE_TABBAR_HEIGHT)
                    }else {
                        return CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - safeAreaBottom - safeAreaTop - self.SIZE_TABBAR_HEIGHT)
                    }
                }
            }
            if self.SIZE_TABBAR_HEIGHT != 0.0 {
                return CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height + heightStatusBar - 34 - 44 - self.SIZE_TABBAR_HEIGHT)
            }else {
                return CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 34 - 44 - self.SIZE_TABBAR_HEIGHT)
            }
        }else{
            var newFrame: CGRect = frameEdit
            let deviceCurrent = Device.current
            if showStatusBar {
                if deviceCurrent.isOneOf(UIDevice.groupOfSmall) {
                    newFrame = CGRect.init(x: 0, y: heightStatusBar/2, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - heightStatusBar - self.SIZE_TABBAR_HEIGHT)
                }else if deviceCurrent.isOneOf(UIDevice.groupOfMedium) {
                    newFrame = CGRect.init(x: 0, y: heightStatusBar/2, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - heightStatusBar - self.SIZE_TABBAR_HEIGHT)
                }else if deviceCurrent.isOneOf(UIDevice.groupOfLarge) {
                    newFrame = CGRect.init(x: 0, y: heightStatusBar/2, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - heightStatusBar - self.SIZE_TABBAR_HEIGHT)
                }
            }else {
                if deviceCurrent.isOneOf(UIDevice.groupOfSmall) {
                    newFrame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height  - self.SIZE_TABBAR_HEIGHT)
                }else if deviceCurrent.isOneOf(UIDevice.groupOfMedium) {
                    newFrame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height  - self.SIZE_TABBAR_HEIGHT)
                }else if deviceCurrent.isOneOf(UIDevice.groupOfLarge) {
                    newFrame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height  - self.SIZE_TABBAR_HEIGHT)
                }
            }
            return newFrame
        }
    }
    
    //Get Frame Constraints are set
    func getFrameContent(frameEdit: CGRect) -> CGRect {
        return CGRect.init(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: frameEdit.height)
    }
    
    func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Would you like to open settings and grant permission to contacts?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
            completionHandler(false)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
            }
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
    
//    func sessionTimeout() {
//        Share.isLogin = false
//        showDialogOK(String.kErrorMessageTitle, String.kErrorMessageTimeout) { () -> (Void) in
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.switchLoginNC()
//        }
//    }
    
    //Language
    var currentLanguage = LangVI //LangEN-LangVI
    var language = ["English", "Việt Nam"]
    typealias Action = (String) -> (Void)
    var completionLanguage: Action?
    
    func showPopupChangeLanguage(_ completion: (@escaping (String) -> (Void))) {
        self.completionLanguage = completion
        let actionSheet = UIAlertController(title: Language.getTextWithKey("PopupLanguage.1"), message: Language.getTextWithKey("PopupLanguage.2"), preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: Language.getTextWithKey("PopupLanguage.5"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            self.dismiss(animated: true, completion: {() -> Void in })
        }))
        self.language.removeAll()
        self.language.append(Language.getTextWithKey("PopupLanguage.3"))
        self.language.append(Language.getTextWithKey("PopupLanguage.4"))
        for item in self.language {
            actionSheet.addAction(UIAlertAction(title: item, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                let langCode = Language.getLanguageCode(item)
                Language.setLanguage(langCode)
                Preferences.shared.setCurrentLocale(langCode)
                
                // Send ObserverLanguage
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "MobiAppsLanguages"),object: nil))
                
                String.kErrorMessageTitle = Language.getTextWithKey("popup.title.error")
                
                self.completionLanguage?(Language.getLanguageName(langCode))
                self.dismiss(animated: true, completion: {() -> Void in})
            }))
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //Language case 2
    func updateLanguageOnView(_ locale: String) { //LangEN-LangVI
        Preferences.shared.setCurrentLocale(locale)
        self.rootView().onUpdateLanguage()
    }
    //Language case 2./
    
    func setPlaceHolder(_ txt:UITextField, _ title:String,_ color: UIColor,_ font: UIFont) {
        let placeHolderAttributes = [
            NSAttributedStringKey.foregroundColor: color,
            NSAttributedStringKey.font : font]
        txt.attributedPlaceholder = NSAttributedString(string: title, attributes:placeHolderAttributes)
    }
    
    //CoreDataEx
    func setupCoreData() {
        Run.sync(coreDataQueue) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            self.managedContext = appDelegate.managedObjectContext
            //self.deleteImageAll {
            //}
        }
    }
    
    let imageProcessingQueue = DispatchQueue(label: "imageProcessingQueue", attributes: DispatchQueue.Attributes.concurrent)
    let coreDataQueue = DispatchQueue(label: "coreDataQueue")
    var managedContext : NSManagedObjectContext?
    //CoreDataEx ./
    
    
    func pushToViewController(viewController: BaseViewController) {
        var tmp: BaseViewController?
        if let viewControllers = self.navigationController?.viewControllers {
            for item in viewControllers {
                if let vc = item as? BaseViewController, type(of: viewController) == type(of: vc) {
                    tmp = vc
                    break
                }
            }
            if tmp != nil {
                self.navigationController?.popToViewController(viewController, animated: false)
            } else {
                self.navigationController?.pushViewController(viewController, animated: false)
            }
        }
    }
}


@available(iOS 9.0, *)
extension BaseViewController: CNContactViewControllerDelegate {
    
    func showNewContactViewController(_ contact: CNMutableContact) {
        let contactViewController: CNContactViewController = CNContactViewController(forNewContact: contact)
        contactViewController.contactStore = CNContactStore()
        contactViewController.delegate = self
        self.present(UINavigationController(rootViewController: contactViewController), animated:true)
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool {
        return true
    }
}
