//  Created by boys vip on 5/29/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit

class BaseInputView: BaseView {
    var _currentView: UIImageView? = nil;
    var _dictMaxLength:[UITextField:Int] = [UITextField:Int]()
    var _dictHighlight:[UITextField:UIImageView] = [UITextField:UIImageView]()
    var checkBackSpace:Bool = false
    var checkBackSpaceTag:Int = 8888
    
    func setFocus(_ view: UIImageView!) {
        if _currentView != nil {
            _currentView!.isHighlighted = false
        }
        
        view.isHighlighted = true;
        _currentView = view
    }
    
    func lostFocus(_ view: UIImageView!) {
        view.isHighlighted = false;
    }
    
    func setMaxLength(_ textField: UITextField, _ maxLength: Int) {
        _dictMaxLength[textField] = maxLength
    }
    
    func setHighlight(_ textField: UITextField, _ hightLight: UIImageView) {
        _dictHighlight[textField] = hightLight
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        super.textFieldDidBeginEditing(textField)
        if _dictHighlight.keys.contains(textField) {
            setFocus(_dictHighlight[textField])
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if _dictHighlight.keys.contains(textField) {
            lostFocus(_dictHighlight[textField])
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.textColor = UIColor.black
        //set format phone
        if textField.tag == checkBackSpaceTag {
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                checkBackSpace = true
            }
        }
        // set format phone./
        if _dictMaxLength.keys.contains(textField) {
            let limit = _dictMaxLength[textField]!
            let nsString = textField.text as NSString?
            let newString = nsString?.replacingCharacters(in: range, with: string)
            if newString!.count <= limit {
                return true;
            }
            
            return false
        }
        
        return true
    }
    
    func validateNotEmpty(_ textField: UITextField) -> Bool {
        if textField.text!.isEmpty {
            if _dictHighlight.keys.contains(textField) {
                setFocus(_dictHighlight[textField])
            }
            
            textField.becomeFirstResponder();
            return false
        }
        
        return true
    }
    
    @objc func textFieldDidChangeFormatPhone(_ textField: UITextField) {
        if let temp = textField.text?.count {
            if temp > 0 {
                //check format phone
                if !checkBackSpace {
                    if temp == 5 {
                        if let temp = textField.text{
                            let end = temp.substring(location: temp.count - 1 , length: 1) ?? ""
                            let begin = temp.substring(location: 0, length: temp.count - 1) ?? ""
                            textField.text = begin + " " + end
                        }
                    }
                }else{
                    if let temp = textField.text {
                        let value = temp.substring(location: temp.count - 1 , length: 1)
                        if value == " " || temp.count == 4 {
                            if let name = textField.text {
                                textField.text = name.substring(location: 0, length: name.count - 1)
                            }
                        }
                    }
                    checkBackSpace = false
                }
                //check format phone ./
            }
        }
    }
    
    @objc func textFieldDidChangeUpperCase(_ textField: UITextField) {
        textField.text = textField.text?.uppercased() //firstUppercased - capitalized
    }
    
    @objc func textFieldDidChangeFirstUpperCase(_ textField: UITextField) {
        textField.text = textField.text?.firstUppercased
    }
}
