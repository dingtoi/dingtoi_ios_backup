//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

class SpinnerCellWidget: UITableViewCell {

    @IBOutlet weak var _title: UILabel!
    @IBOutlet weak var _imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commonInit(_ value: SelectListItem?, isCheck: Bool) {
        _title.text = value?.Text
        _imgCheck.isHidden = !isCheck
    }
}
