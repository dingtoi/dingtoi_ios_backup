//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class SpinnerSearchWidget: UITableView, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    public static let TAGTABLEVIEW:Int = 22222222
    private var styleIndex: Int = 1
    private var _viewHeader: UIView?
    var viewHeader: UIView? {
        get{
            return _viewHeader
        }
        set(newValue){
            self._viewHeader = newValue
        }
    }
    
    private var _heightViewHeader: CGFloat = 50
    var heightViewHeader: CGFloat {
        get{
            return self._heightViewHeader
        }
        set(newValue){
            self._heightViewHeader = newValue
        }
    }
    
    private var _listArray: Array<SelectListItem>?
    var listArray: Array<SelectListItem>? {
        get{
            return self._listArray
        }
        set(newValue){
            self._listArray = newValue
        }
    }
    
    private var _listArrayDefault: Array<SelectListItem>?
    var listArrayDefault: Array<SelectListItem>? {
        get{
            return self._listArrayDefault
        }
        set(newValue){
            self._listArrayDefault = newValue
        }
    }
    
    private var _title: String?
    var title: String? {
        get{
            return self._title
        }
        set(newValue){
            self._title = newValue
        }
    }
    
    private var _fontTitle: UIFont = UIFont(name: "Roboto-Regular", size: SizePickerView.FONT_TEXT_IN_BUTTON_BEFORE_REGISTER)!
    var fontTitle: UIFont {
        get{
            return self._fontTitle
        }
        set(newValue){
            self._fontTitle = newValue
        }
    }
    
    private var _colorTitle: UIColor = UIColor.rgb(fromHexString: "#1984bc")
    var colorTitle: UIColor {
        get{
            return self._colorTitle
        }
        set(newValue){
            self._colorTitle = newValue
        }
    }
    
    typealias Action = (String, String) -> (Void)
    var completion: Action?
    var selectDisplayValue: String = ""
    
    init(_ title: String,_ valueDisplaySelect: String,_ list: Array<SelectListItem>?,_ completion: (@escaping (String, String) -> (Void)) ) {
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        super.init(frame: CGRect.init(x: 0, y: (height - (height/2)), width: width, height: (height/2)), style: UITableViewStyle.plain)
        self.backgroundColor = UIColor.white
        self.selectDisplayValue = valueDisplaySelect
        self._listArrayDefault = list
        self._listArray = list
        self.completion = completion
        self.styleIndex = 1
        initTable(styleIndex: self.styleIndex)
        
        let positionY = (height - (height/2)) - self._heightViewHeader
        _viewHeader = UIView.init(frame: CGRect.init(x: 0, y: positionY, width: width, height: self._heightViewHeader))
        _viewHeader?.backgroundColor = UIColor.white
        
        let textField = UITextField.init(frame: CGRect.init(x: 15.0, y: 0.0, width: width - 30.0, height: self._heightViewHeader))
        textField.backgroundColor = UIColor.clear
        textField.textColor = _colorTitle
        textField.textAlignment = NSTextAlignment.left
        textField.attributedPlaceholder = NSAttributedString(string: title.uppercased(), attributes: [NSAttributedStringKey.foregroundColor : _colorTitle])
        textField.placeholder = title.uppercased()
        textField.font = _fontTitle
        textField.leftView = UIImageView.init(image: UIImage.init(named: "search2.png"))
        textField.leftView?.frame = CGRect(x: 0, y: 5, width: 30 , height:30)
        textField.leftViewMode = .always
        textField.layer.borderWidth = 1.0
        textField.layer.masksToBounds = true
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.delegate = self
        
        _viewHeader?.addSubview(textField)
    }
    
    init(_ title: String,_ valueDisplaySelect: String,_ list: Array<SelectListItem>?, styleIndex: Int?,_ completion: (@escaping (String, String) -> (Void)) ) {
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        super.init(frame: CGRect.init(x: 0, y: (height - (height/2)), width: width, height: (height/2)), style: UITableViewStyle.plain)
        self.backgroundColor = UIColor.white
        self.selectDisplayValue = valueDisplaySelect
        self._listArrayDefault = list
        self._listArray = list
        self.completion = completion
        self.styleIndex = 2 //styleIndex
        initTable(styleIndex: self.styleIndex)
        
        let positionY = (height - (height/2)) - self._heightViewHeader
        _viewHeader = UIView.init(frame: CGRect.init(x: 0, y: positionY, width: width, height: self._heightViewHeader))
        _viewHeader?.backgroundColor = UIColor.white
        
        let label = UILabel.init(frame: CGRect.init(x: 15.0, y: 0.0, width: width - 30.0, height: self._heightViewHeader))
        label.backgroundColor = UIColor.clear
        label.textColor = _colorTitle
        label.textAlignment = NSTextAlignment.left
        label.text = title.uppercased()
        label.font = _fontTitle
        
        _viewHeader?.addSubview(label)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func hide() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromBottom
        
        if appDelegate.window!.viewWithTag(SpinnerSearchWidget.TAGTABLEVIEW + 1) != nil {
            appDelegate.window!.viewWithTag(SpinnerSearchWidget.TAGTABLEVIEW + 1)?.removeFromSuperview()
        }
        if appDelegate.window!.viewWithTag(SpinnerSearchWidget.TAGTABLEVIEW) != nil {
            let view = appDelegate.window!.viewWithTag(SpinnerSearchWidget.TAGTABLEVIEW)
            view?.layer.add(transition, forKey: nil)
            view?.removeFromSuperview()
        }
    }
    
    func show() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromTop
        
        let viewOutSide = UIView.init(frame: UIScreen.main.bounds)
        viewOutSide.backgroundColor = UIColor.init(red: 0.41, green: 0.41, blue: 0.41, alpha: 0.5)
        viewOutSide.tag = SpinnerSearchWidget.TAGTABLEVIEW + 1
        appDelegate.window?.addSubview(viewOutSide)
        
        let view:UIView = UIView.init(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.clear
        view.addSubview(self)
        view.addSubview(self._viewHeader!)
        
        let btnOutSide = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: view.frame.height - self._viewHeader!.frame.height - self.frame.height))
        btnOutSide.backgroundColor = UIColor.clear
        let tapOutSide = UITapGestureRecognizer(target: self, action: #selector(viewOutSideClick))
        btnOutSide.addGestureRecognizer(tapOutSide)
        btnOutSide.isUserInteractionEnabled = true
        view.addSubview(btnOutSide)
        
        view.tag = SpinnerSearchWidget.TAGTABLEVIEW
        view.layer.add(transition, forKey: nil)
        appDelegate.window!.endEditing(true)
        appDelegate.window!.addSubview(view)
    }
    
    @objc func viewOutSideClick(sender: UITapGestureRecognizer) {
        self.hide()
    }
    
    //TableView
    func initTable(styleIndex: Int) {
        self.delegate = self
        self.dataSource = self
        separatorInset = UIEdgeInsets.init(top: 0.0, left: 15.0, bottom: 0.0, right: 15.0)
        
        if styleIndex == 1 {
            let nibName1Cell = UINib(nibName: "SpinnerCellWidget", bundle: nil)
            register(nibName1Cell, forCellReuseIdentifier: "spinnerCellWidget")
        }else if styleIndex == 2{
            let nibName1Cell = UINib(nibName: "SpinnerCell2Widget", bundle: nil)
            register(nibName1Cell, forCellReuseIdentifier: "spinnerCellWidget")
        }
        
        setContentOffset(CGPoint.zero, animated: true)
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listArray?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.styleIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "spinnerCellWidget", for: indexPath) as! SpinnerCellWidget
            if let temp = self.listArray?[indexPath.row].Text, temp == self.selectDisplayValue {
                cell.commonInit(self.listArray?[indexPath.row], isCheck: true)
            }else{
                cell.commonInit(self.listArray?[indexPath.row], isCheck: false)
            }
            cell.selectionStyle = .none
            return cell
        }else if self.styleIndex == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "spinnerCell2Widget", for: indexPath) as! SpinnerCell2Widget
            if let temp = self.listArray?[indexPath.row].Text, temp == self.selectDisplayValue {
                cell.commonInit(self.listArray?[indexPath.row], isCheck: true)
            }else{
                cell.commonInit(self.listArray?[indexPath.row], isCheck: false)
            }
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Table Widget - Session: \(indexPath.section) --> Select Row: \(indexPath.row))")
        completion!(_listArray?[indexPath.row].Text ?? ""   ,  _listArray?[indexPath.row].Value ?? "")
        self.hide()
    }
    //TableView ./
    
    func ReloadDataWidthTextSearch(_ inputString: String?) {
        print("Search: \(String(describing: inputString))")
        if let search = inputString, search == "" {
            self.listArray?.removeAll()
            self.listArray = self.listArrayDefault
            reloadData()
        }else if let search = inputString, search != "" {
            if let list = self.listArrayDefault {
                self.listArray?.removeAll()
                for item in list {
                    if let txt = item.Text, let search = inputString, let compare = txt.uppercased().index(of: search.uppercased()), compare >= 0{
                        self.listArray?.append(item)
                    }
                }
                reloadData()
            }
        }
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        showKeyBoard()
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyBoard()
        textField.resignFirstResponder()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            print("Backspace was pressed")
            if let value = textField.text, value != "" {
                ReloadDataWidthTextSearch(value.substring(location: 0, length: value.count - 1))
            }
        }else{
            ReloadDataWidthTextSearch("\(textField.text ?? "")\(string)")
        }
        return true
    }
    
    var checkMoveTop = false
    func showKeyBoard() {
        if !self.checkMoveTop {
            self.checkMoveTop = true
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if appDelegate.window!.viewWithTag(SpinnerSearchWidget.TAGTABLEVIEW) != nil {
                moveTop(appDelegate.window!.viewWithTag(SpinnerSearchWidget.TAGTABLEVIEW)!)
            }
        }
    }
    
    func hideKeyBoard() {
        if self.checkMoveTop {
            self.checkMoveTop = false
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if appDelegate.window!.viewWithTag(SpinnerSearchWidget.TAGTABLEVIEW) != nil {
                moveBottom(appDelegate.window!.viewWithTag(SpinnerSearchWidget.TAGTABLEVIEW)!)
            }
        }
    }
    
    private func moveTop(_ view: UIView) {
        UIView.animate(withDuration: 0.5, animations: {
            view.frame = CGRect.init(x: view.frame.origin.x, y: view.frame.origin.y - 216.0, width: view.frame.width, height: view.frame.height)
        })
    }
    
    private func moveBottom(_ view: UIView) {
        view.frame = CGRect.init(x: view.frame.origin.x, y: view.frame.origin.y + 216.0, width: view.frame.width, height: view.frame.height)
    }
    
}
