//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class SpinnerWidget: UITableView, UITableViewDelegate, UITableViewDataSource {
    public static let TAGTABLEVIEW:Int = 222222222
    private var styleIndex: Int = 1
    private var _viewHeader: UIView?
    var viewHeader: UIView? {
        get{
            return _viewHeader
        }
        set(newValue){
            self._viewHeader = newValue
        }
    }
    
    private var _heightViewHeader: CGFloat = 50
    var heightViewHeader: CGFloat {
        get{
            return self._heightViewHeader
        }
        set(newValue){
            self._heightViewHeader = newValue
        }
    }
    
    private var _listArray: Array<SelectListItem>?
    var listArray: Array<SelectListItem>? {
        get{
            return self._listArray
        }
        set(newValue){
            self._listArray = newValue
        }
    }
    
    private var _title: String?
    var title: String? {
        get{
            return self._title
        }
        set(newValue){
            self._title = newValue
        }
    }
    
    private var _fontTitle: UIFont = UIFont(name: "Roboto-Regular", size: SizePickerView.FONT_TEXT_IN_BUTTON_BEFORE_REGISTER)!
    var fontTitle: UIFont {
        get{
            return self._fontTitle
        }
        set(newValue){
            self._fontTitle = newValue
        }
    }
    
    private var _colorTitle: UIColor = UIColor.rgb(fromHexString: "#1984bc")
    var colorTitle: UIColor {
        get{
            return self._colorTitle
        }
        set(newValue){
            self._colorTitle = newValue
        }
    }
    
    typealias Action = (String, String) -> (Void)
    var completion: Action?
    typealias Action2 = (Array<String>) -> (Void)
    var completion2: Action2?
    var selectDisplayValue: String = ""
    var selectDisplayListValue: Array<String> = []
    var checkShowMulti = false
    
    init(_ title: String,_ valueDisplaySelect: String,_ list: Array<SelectListItem>?,_ completion: (@escaping (String, String) -> (Void)) ) {
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        super.init(frame: CGRect.init(x: 0, y: (height - (height/2)), width: width, height: (height/2)), style: UITableViewStyle.plain)
        self.backgroundColor = UIColor.white
        self.selectDisplayValue = valueDisplaySelect
        self._listArray = list
        self.completion = completion
        self.styleIndex = 1
        initTable(styleIndex: self.styleIndex)
        
        let positionY = (height - (height/2)) - self._heightViewHeader
        _viewHeader = UIView.init(frame: CGRect.init(x: 0, y: positionY, width: width, height: self._heightViewHeader))
        _viewHeader?.backgroundColor = UIColor.white

        let label = UILabel.init(frame: CGRect.init(x: 15.0, y: 0.0, width: width, height: self._heightViewHeader))
        label.backgroundColor = UIColor.clear
        label.textColor = _colorTitle
        label.textAlignment = NSTextAlignment.left
        label.text = title.uppercased()
        label.font = _fontTitle

        _viewHeader?.addSubview(label)
    }
    
    init(_ title: String,_ list: Array<SelectListItem>?, styleIndex: Int?,_ completion: (@escaping (Array<String>) -> (Void)) ) {
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        super.init(frame: CGRect.init(x: 0, y: (height - (height/2)), width: width, height: (height/2)), style: UITableViewStyle.plain)
        self.backgroundColor = UIColor.white
        self._listArray = list
        self.completion2 = completion
        self.styleIndex = 2
        initTable(styleIndex: self.styleIndex)
        
        let positionY = (height - (height/2)) - self._heightViewHeader
        _viewHeader = UIView.init(frame: CGRect.init(x: 0, y: positionY, width: width, height: self._heightViewHeader))
        _viewHeader?.backgroundColor = UIColor.white
        
        let label = UILabel.init(frame: CGRect.init(x: 15.0, y: 0.0, width: width, height: self._heightViewHeader))
        label.backgroundColor = UIColor.clear
        label.textColor = _colorTitle
        label.textAlignment = NSTextAlignment.left
        label.text = title.uppercased()
        label.font = _fontTitle
        
        _viewHeader?.addSubview(label)
    }
    
    init(_ title: String,_ selectDisplayListValue: Array<String>, _ list: Array<SelectListItem>?,_ completion: (@escaping (Array<String>) -> (Void)) ) {
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        super.init(frame: CGRect.init(x: 0, y: (height - (height/2)), width: width, height: (height/2)), style: UITableViewStyle.plain)
        self.backgroundColor = UIColor.white
        self.selectDisplayListValue = selectDisplayListValue
        self.checkShowMulti = true
        self._listArray = list
        self.completion2 = completion
        self.styleIndex = 1
        initTable(styleIndex: self.styleIndex)
        
        let positionY = (height - (height/2)) - self._heightViewHeader
        _viewHeader = UIView.init(frame: CGRect.init(x: 0, y: positionY, width: width, height: self._heightViewHeader))
        _viewHeader?.backgroundColor = UIColor.white
        
        let label = UILabel.init(frame: CGRect.init(x: 15.0, y: 0.0, width: width, height: self._heightViewHeader))
        label.backgroundColor = UIColor.clear
        label.textColor = _colorTitle
        label.textAlignment = NSTextAlignment.left
        label.text = title.uppercased()
        label.font = _fontTitle
        
        _viewHeader?.addSubview(label)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func hide() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromBottom
        
        if appDelegate.window!.viewWithTag(SpinnerWidget.TAGTABLEVIEW + 1) != nil {
            appDelegate.window!.viewWithTag(SpinnerWidget.TAGTABLEVIEW + 1)?.removeFromSuperview()
        }
        if appDelegate.window!.viewWithTag(SpinnerWidget.TAGTABLEVIEW) != nil {
            let view = appDelegate.window!.viewWithTag(SpinnerWidget.TAGTABLEVIEW)
            view?.layer.add(transition, forKey: nil)
            view?.removeFromSuperview()
        }
    }
    
    func show() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromTop
        
        let viewOutSide = UIView.init(frame: UIScreen.main.bounds)
        viewOutSide.backgroundColor = UIColor.init(red: 0.41, green: 0.41, blue: 0.41, alpha: 0.5)
        viewOutSide.tag = SpinnerWidget.TAGTABLEVIEW + 1
        appDelegate.window?.addSubview(viewOutSide)
        
        let view:UIView = UIView.init(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.clear
        view.addSubview(self)
        view.addSubview(self._viewHeader!)
        
        let btnOutSide = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: view.frame.height - self._viewHeader!.frame.height - self.frame.height))
        btnOutSide.backgroundColor = UIColor.clear
        let tapOutSide = UITapGestureRecognizer(target: self, action: #selector(viewOutSideClick))
        btnOutSide.addGestureRecognizer(tapOutSide)
        btnOutSide.isUserInteractionEnabled = true
        view.addSubview(btnOutSide)
        
        view.tag = SpinnerWidget.TAGTABLEVIEW
        view.layer.add(transition, forKey: nil)
        appDelegate.window!.endEditing(true)
        appDelegate.window!.addSubview(view)
    }
    
    @objc func viewOutSideClick(sender: UITapGestureRecognizer) {
        if self.styleIndex == 1 {
            if !self.checkShowMulti {
                self.hide()
            }else{
                self.selectDisplayListValue = self.selectDisplayListValue.sorted { $0 < $1 }
                completion2!(self.selectDisplayListValue)
                self.hide()
            }
        }else if self.styleIndex == 2{
            var listIdTag = Array<String>.init()
            if let list = self.listArray {
                for item in list {
                    if let ck = item.Selected, ck {
                        listIdTag.append(item.Value!)
                    }
                }
            }
            completion2!(listIdTag)
            self.hide()
        }
    }
    
    //TableView
    func initTable(styleIndex: Int) {
        self.delegate = self
        self.dataSource = self
        separatorInset = UIEdgeInsets.init(top: 0.0, left: 15.0, bottom: 0.0, right: 15.0)
        
        if styleIndex == 1 {
            let nibName1Cell = UINib(nibName: "SpinnerCellWidget", bundle: nil)
            register(nibName1Cell, forCellReuseIdentifier: "spinnerCellWidget")
        }else if styleIndex == 2{
            let nibName1Cell = UINib(nibName: "SpinnerCell2Widget", bundle: nil)
            register(nibName1Cell, forCellReuseIdentifier: "spinnerCell2Widget")
        }
        
        setContentOffset(CGPoint.zero, animated: true)
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listArray?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.styleIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "spinnerCellWidget", for: indexPath) as! SpinnerCellWidget
            if !self.checkShowMulti {
                if let temp = self.listArray?[indexPath.row].Text, temp == self.selectDisplayValue {
                    cell.commonInit(self.listArray?[indexPath.row], isCheck: true)
                }else{
                    cell.commonInit(self.listArray?[indexPath.row], isCheck: false)
                }
            }else{
                var checkExits = false
                for item in self.selectDisplayListValue {
                    if let temp = self.listArray?[indexPath.row].Value, temp == item {
                        checkExits = true
                        break
                    }
                }
                if !checkExits {
                    cell.commonInit(self.listArray?[indexPath.row], isCheck: false)
                }else{
                    cell.commonInit(self.listArray?[indexPath.row], isCheck: true)
                }
            }
            cell.selectionStyle = .none
            return cell
        }else if self.styleIndex == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "spinnerCell2Widget", for: indexPath) as! SpinnerCell2Widget
            cell.commonInit(self.listArray?[indexPath.row], isCheck: self.listArray?[indexPath.row].Selected ?? false)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Table Widget - Session: \(indexPath.section) --> Select Row: \(indexPath.row))")
        if self.styleIndex == 1 {
            if !self.checkShowMulti {
                completion!(self._listArray?[indexPath.row].Text ?? ""   ,  _listArray?[indexPath.row].Value ?? "")
                self.hide()
            }else{
                var checkExits = false
                var indexExits: Int?
                var index = 0
                for item in self.selectDisplayListValue {
                    if let val = _listArray?[indexPath.row].Value, val == item {
                        checkExits = true
                        indexExits = index
                    }
                    index = index + 1
                }
                if !checkExits {
                    self.selectDisplayListValue.append(_listArray?[indexPath.row].Value ?? "")
                    reloadData()
                }else{
                    self.selectDisplayListValue.remove(at: indexExits!)
                    reloadData()
                }
            }
        }else if self.styleIndex == 2{
            if let ck = self._listArray?[indexPath.row].Selected, ck {
                self._listArray?[indexPath.row].Selected = false
            }else{
                self._listArray?[indexPath.row].Selected = true
            }
            reloadData()
        }
    }
    //TableView ./
    
}
