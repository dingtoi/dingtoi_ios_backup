//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

enum StyleSwipe {
    case LEFT
    case LEFT_TOP
    case LEFT_BOTTOM
    case LEFT_1
    case LEFT_2
    case LEFT_3
    case RIGHT
    case RIGHT_TOP
    case RIGHT_BOTTOM
    case RIGHT_1
    case RIGHT_2
    case RIGHT_3
}

protocol TableViewDelegateWidget {
    func numberOfSections(in tableView: UITableView) -> Int? //require
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat?
    func tableView(_ tableView: UITableView, nibForHeaderInSection section: Int) -> String?
    func tableView(_ tableView: UITableView,_ header: UITableViewCell?, viewForHeaderInSection section: Int) -> UIView?
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat?
    func tableView(_ tableView: UITableView, nibForFooterInSection section: Int) -> String?
    func tableView(_ tableView: UITableView,_ footer: UITableViewCell?, viewForFooterInSection section: Int) -> UIView?
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat?
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int?
    func tableView(_ tableView: UITableView, nibForRowAt indexPath: IndexPath) -> String? //require
    func tableView(_ tableView: UITableView,_ row: UITableViewCell, cellForRowAt indexPath: IndexPath) -> UITableViewCell? //require
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) //require
    func tableView(_ cell: UITableViewCell, didTapSwipe styleSwipe: StyleSwipe)
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool?
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
}

extension TableViewDelegateWidget {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat? {return nil}
    func tableView(_ tableView: UITableView, nibForHeaderInSection section: Int) -> String? {return nil}
    func tableView(_ tableView: UITableView,_ header: UITableViewCell?, viewForHeaderInSection section: Int) -> UIView? {return nil}
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat? {return nil}
    func tableView(_ tableView: UITableView, nibForFooterInSection section: Int) -> String? {return nil}
    func tableView(_ tableView: UITableView,_ footer: UITableViewCell?, viewForFooterInSection section: Int) -> UIView? {return nil}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat? {return nil}
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat? {return nil}
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int? {return nil}
    
    func tableView(_ cell: UITableViewCell, didTapSwipe styleSwipe: StyleSwipe) {}
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {}
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool? {return nil}
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {return nil}
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {}
}

class TableViewWidget: UITableView, UITableViewDelegate, UITableViewDataSource, SwipeStyle1CellDelegate, SwipeStyle2CellDelegate {
    private var tableViewDelegateWidget: TableViewDelegateWidget?
    private var listData: [Any?]?
    private var heighRow: CGFloat?
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)//UITableViewStyle.grouped - UITableViewStyle.plain
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //If listData == nil && heighRow == nil <--> require implement TableViewDelegateWidget
    //init(frame: CGRect, listNib: [String], listData: nil, heighRow: nil, tableViewDelegateWidget: self, style: nil)
    //init(frame: CGRect, listNib: [String], listData: [AnyObject?], heighRow: 60, tableViewDelegateWidget: nil, style: nil)
    func initRegistry(_ listNib: [String],_ listData: [Any?]?,_ heighRow: CGFloat?,_ tableViewDelegateWidget: TableViewDelegateWidget?) {
        for nib in listNib {
            register(UINib(nibName: nib.firstUppercased, bundle: nil), forCellReuseIdentifier: nib)
        }
        if let list = listData{
            self.listData = list
        }
        if let heighRow = heighRow{
            self.heighRow = heighRow
        }
        if let delegate = tableViewDelegateWidget {
            self.tableViewDelegateWidget = delegate
        }
        self.delegate = self
        self.dataSource = self
        self.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func updateListData(_ listData: [AnyObject?]?) {
        if let list = listData{
            self.listData = list
            reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewDelegateWidget?.numberOfSections(in: tableView) ?? 0
    }
    
    //Header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.tableViewDelegateWidget?.tableView(tableView, heightForHeaderInSection: section) ?? 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let nibHeader = self.tableViewDelegateWidget?.tableView(tableView, nibForHeaderInSection: section) {
            if let headerView = tableView.dequeueReusableCell(withIdentifier: nibHeader) {//UITableViewCell
                headerView.selectionStyle = .none
                headerView.layoutIfNeeded()
                return self.tableViewDelegateWidget?.tableView(tableView, headerView, viewForHeaderInSection: section)
            }
        }
        return self.tableViewDelegateWidget?.tableView(tableView, nil, viewForHeaderInSection: section)
    }
    
    //Header ./
    
    //Footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return self.tableViewDelegateWidget?.tableView(tableView, heightForFooterInSection: section) ?? 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let nibFooter = self.tableViewDelegateWidget?.tableView(tableView, nibForFooterInSection: section) {
            if let footerView = tableView.dequeueReusableCell(withIdentifier: nibFooter) {//UITableViewCell
                footerView.selectionStyle = .none
                footerView.layoutIfNeeded()
                return self.tableViewDelegateWidget?.tableView(tableView, footerView, viewForFooterInSection: section)
            }
        }
        return self.tableViewDelegateWidget?.tableView(tableView, nil, viewForFooterInSection: section)
    }
    //Footer ./
    
    //Row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.tableViewDelegateWidget?.tableView(tableView, heightForRowAt: indexPath) {
            return height
        }else if let height = self.heighRow {
            return height
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.tableViewDelegateWidget?.tableView(tableView, estimatedHeightForRowAt: indexPath) {
            return height
        }else if let height = self.heighRow {
            return height
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let size = self.tableViewDelegateWidget?.tableView(tableView, numberOfRowsInSection: section) {
            return size
        }else if let size = self.listData?.count {
            return size
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let nibRow = self.tableViewDelegateWidget?.tableView(tableView, nibForRowAt: indexPath) {
            if let rowView = tableView.dequeueReusableCell(withIdentifier: nibRow) {//UITableViewCell
                if let cellRow = self.tableViewDelegateWidget?.tableView(tableView, rowView, cellForRowAt: indexPath) as? SwipeStyle1Cell {
                    cellRow.delegate = self
                    return cellRow
                }
                if let cellRow = self.tableViewDelegateWidget?.tableView(tableView, rowView, cellForRowAt: indexPath) as? SwipeStyle2Cell {
                    cellRow.delegate = self
                    return cellRow
                }
                if let row = self.tableViewDelegateWidget?.tableView(tableView, rowView, cellForRowAt: indexPath) {
                    row.selectionStyle = .none
                    row.layoutIfNeeded()
                    return row
                }
            }
        }
        return UITableViewCell()
    }
    //Row ./
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("TableView Section: \(indexPath.section) - RowCell: \(indexPath.row)")
        self.tableViewDelegateWidget?.tableView(tableView, didSelectRowAt: indexPath)
    }
    
    //Swipe Style1
    func DidTapSwipeBegin_Style1(cell: SwipeStyle1Cell, indexPath: IndexPath) {
        print("Did Tap Swipe Begin Style 1")
    }
    
    func DidTapLeftButtonDetail_Style1(cell: SwipeStyle1Cell, indexPath: IndexPath) {
        print("Did Tap Left Button Detail")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.LEFT)
    }
    
    func DidTapRightButtonTop_Style1(cell: SwipeStyle1Cell, indexPath: IndexPath) {
        print("Did Tap Right Button Top")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_TOP)
    }
    
    func DidTapRightButtonBottom_Style1(cell: SwipeStyle1Cell, indexPath: IndexPath) {
        print("Did Tap Right Button Bottom")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_BOTTOM)
    }
    //Swipe Style1 ./
    
    //Swipe Style2
    func DidTapSwipeBegin_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        print("Did Tap Swipe Begin Style 2")
    }
    
    func DidTapRightButton_1_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        print("Did Tap Right Button 1")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_1)
    }
    
    func DidTapRightButton_2_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        print("Did Tap Right Button 2")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_2)
    }
    
    func DidTapRightButton_3_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        print("Did Tap Right Button 3")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_3)
    }
    
    func DidTapLeftButton_1_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        print("Did Tap Left Button 1")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.LEFT_1)
    }
    //Swipe Style2 ./
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableViewDelegateWidget?.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return self.tableViewDelegateWidget?.tableView(tableView, canEditRowAt: indexPath) ?? false
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return self.tableViewDelegateWidget?.tableView(tableView, editActionsForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        self.tableViewDelegateWidget?.tableView(tableView, commit: editingStyle, forRowAt: indexPath)
    }
}


extension UITableView {
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.numberOfSections > 0 {
                if self.numberOfRows(inSection:  self.numberOfSections - 1) > 0 {
                    let indexPath = IndexPath(
                        row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                        section: self.numberOfSections - 1)
                    self.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
    func scrollToTop() {
        DispatchQueue.main.async {
            if self.numberOfSections > 0 {
                if self.numberOfRows(inSection:  self.numberOfSections - 1) > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }
        }
    }
}
