//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

protocol SwipeStyle2CellDelegate : NSObjectProtocol {
    func DidTapSwipeBegin_Style2(cell : SwipeStyle2Cell, indexPath: IndexPath)
    func DidTapRightButton_1_Style2(cell : SwipeStyle2Cell, indexPath: IndexPath)
    func DidTapRightButton_2_Style2(cell : SwipeStyle2Cell, indexPath: IndexPath)
    func DidTapRightButton_3_Style2(cell : SwipeStyle2Cell, indexPath: IndexPath)
    func DidTapLeftButton_1_Style2(cell : SwipeStyle2Cell, indexPath: IndexPath)
}

class SwipeStyle2Cell: UITableViewCell  {
    weak var delegate : SwipeStyle2CellDelegate?
    var indexPath: IndexPath?
    var animationOptions : UIViewAnimationOptions = [.allowUserInteraction, .beginFromCurrentState]
    var animationDuration : TimeInterval = 0.5
    var animationDelay : TimeInterval = 0.0
    var animationSpingDamping : CGFloat = 0.5
    var animationInitialVelocity : CGFloat = 1
    private weak var leftWidthConstraint : NSLayoutConstraint!
    private weak var rightWidthConstraint : NSLayoutConstraint!
    var buttonWidth :CGFloat = 60 {
        didSet(val) {
            if let r = self.rightWidthConstraint {
                r.constant = self.buttonWidth
            }
            if let l = self.leftWidthConstraint {
                l.constant = self.buttonWidth
            }
        }
    }
    private weak var panRecognizer : UIPanGestureRecognizer!
    private weak var btnTabDefaulCell : UITapGestureRecognizer!
    
    private var beginPointContentViewX : CGPoint = CGPoint.zero
    
    private let colorView = UIColor.rgb(fromHexString: "#EDEDED")
    private let colorButton = UIColor.rgb(fromHexString: "#F9F9F9")
    private let sizeIconButton = CGSize.init(width: 16.0, height: 16.0)
    private let paddingLeftRightCell: CGFloat = 10.0
    private let paddingTopCell: CGFloat = 0.0
    private let fontTitle = UIFont(name: "Roboto-Regular", size: 12.0)
    private let countBtnRight: CGFloat = 3.0
    private let countBtnLeft: CGFloat = 1.0
    
    private var _isSwipeLeft:Bool = true
    public var isSwipeLeft: Bool {
        get{
            return self._isSwipeLeft
        }
        set(newValue){
            self._isSwipeLeft = newValue
        }
    }
    
    private var _isSwipeRight: Bool = true
    public var isSwipeRight: Bool {
        get{
            return self._isSwipeRight
        }
        set(newValue){
            self._isSwipeRight = newValue
        }
    }
    
    //LEFT
    private var _nameImageLeft = "edit_32_px.png"
    public var nameImageLeft: String {
        get{
            return self._nameImageLeft
        }
        set(newValue){
            self._nameImageLeft = newValue
        }
    }
    
    private var _titleLeft = "Button Left"
    public var titleLeft: String {
        get{
            return self._titleLeft
        }
        set(newValue){
            self._titleLeft = newValue
        }
    }
    //LEFT ./
    
    //RIGHT
    private var _nameImageRight_1 = "call_red.png"
    public var nameImageRight_1: String {
        get{
            return self._nameImageRight_1
        }
        set(newValue){
            self._nameImageRight_1 = newValue
        }
    }
    
    private var _titleRight_1 = "Button Right 1"
    public var titleRight_1: String {
        get{
            return self._titleRight_1
        }
        set(newValue){
            self._titleRight_1 = newValue
        }
    }
    
    private var _nameImageRight_2 = "call_green.png"
    public var nameImageRight_2: String {
        get{
            return self._nameImageRight_2
        }
        set(newValue){
            self._nameImageRight_2 = newValue
        }
    }
    
    private var _titleRight_2 = "Button Right 2"
    public var titleRight_2: String {
        get{
            return self._titleRight_2
        }
        set(newValue){
            self._titleRight_2 = newValue
        }
    }
    
    private var _nameImageRight_3 = "btnclose.png"
    public var nameImageRight_3: String {
        get{
            return self._nameImageRight_3
        }
        set(newValue){
            self._nameImageRight_3 = newValue
        }
    }
    
    private var _titleRight_3 = "Button Right 3"
    public var titleRight_3: String {
        get{
            return self._titleRight_3
        }
        set(newValue){
            self._titleRight_3 = newValue
        }
    }
    //RIGHT ./
    
    weak var rightView : UIView! {
        willSet(val) {
            if let r = self.rightView {
                r.removeFromSuperview()
            }
            if let v = val {
                self.addSubview(v)
                v.translatesAutoresizingMaskIntoConstraints = false
                self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[v]-(0)-|", options: [], metrics: nil, views: ["v":v]))
                self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "[v]-(0)-|", options: [], metrics: nil, views: ["v":v]))
                
                let wc = NSLayoutConstraint(item: v, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.buttonWidth*self.countBtnRight)
                v.addConstraint(wc)
                v.backgroundColor = self.colorView
                self.rightWidthConstraint = wc
                self.sendSubview(toBack: v)
                
                let btnRight_1 = UIButton.init(frame: CGRect.init(x: self.buttonWidth*0 - self.paddingLeftRightCell, y: self.paddingTopCell, width: self.buttonWidth, height: self.frame.height - self.paddingTopCell*2))
                btnRight_1.setTitle(self._titleRight_1, for: .normal)
                btnRight_1.setTitleColor(UIColor.lightGray, for: .normal)
                btnRight_1.titleLabel?.font = self.fontTitle
                btnRight_1.setImage(UIImage.init(named: self._nameImageRight_1)?.resizeImage(targetSize: self.sizeIconButton), for: .normal)
                btnRight_1.backgroundColor = self.colorButton
                btnRight_1.centerImageAndButton(8.0, imageOnTop: true)
                btnRight_1.tag = 2
                btnRight_1.isEnabled = true
                btnRight_1.isUserInteractionEnabled = true
                btnRight_1.addTarget(self, action:#selector(clickButton), for: .touchUpInside)
                v.addSubview(btnRight_1)
                
                let btnRight_2 = UIButton.init(frame: CGRect.init(x: self.buttonWidth*1 - self.paddingLeftRightCell, y: self.paddingTopCell, width: self.buttonWidth, height: self.frame.height - self.paddingTopCell*2))
                btnRight_2.setTitle(self._titleRight_2, for: .normal)
                btnRight_2.setTitleColor(UIColor.lightGray, for: .normal)
                btnRight_2.titleLabel?.font = self.fontTitle
                btnRight_2.setImage(UIImage.init(named: self._nameImageRight_2)?.resizeImage(targetSize: self.sizeIconButton), for: .normal)
                btnRight_2.backgroundColor = self.colorButton
                btnRight_2.centerImageAndButton(8.0, imageOnTop: true)
                btnRight_2.tag = 3
                btnRight_2.isEnabled = true
                btnRight_2.isUserInteractionEnabled = true
                btnRight_2.addTarget(self, action:#selector(clickButton), for: .touchUpInside)
                v.addSubview(btnRight_2)
                
                let btnRight_3 = UIButton.init(frame: CGRect.init(x: self.buttonWidth*2 - self.paddingLeftRightCell, y: self.paddingTopCell, width: self.buttonWidth, height: self.frame.height - self.paddingTopCell*2))
                btnRight_3.setTitle(self._titleRight_3, for: .normal)
                btnRight_3.setTitleColor(UIColor.lightGray, for: .normal)
                btnRight_3.titleLabel?.font = self.fontTitle
                btnRight_3.setImage(UIImage.init(named: self._nameImageRight_3)?.resizeImage(targetSize: self.sizeIconButton), for: .normal)
                btnRight_3.backgroundColor = self.colorButton
                btnRight_3.centerImageAndButton(8.0, imageOnTop: true)
                btnRight_3.tag = 4
                btnRight_3.isEnabled = true
                btnRight_3.isUserInteractionEnabled = true
                btnRight_3.addTarget(self, action:#selector(clickButton), for: .touchUpInside)
                v.addSubview(btnRight_3)
                
            }
        }
    }
    
    weak var leftView : UIView! {
        willSet(val) {
            if let l = self.leftView {
                l.removeFromSuperview()
            }
            if let v = val {
                self.addSubview(v)
                v.translatesAutoresizingMaskIntoConstraints = false
                self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[v]-(0)-|", options: [], metrics: nil, views: ["v":v]))
                self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(0)-[v]", options: [], metrics: nil, views: ["v":v]))
                let wc = NSLayoutConstraint(item: v, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.buttonWidth*self.countBtnLeft)
                v.addConstraint(wc)
                v.backgroundColor = self.colorView
                self.leftWidthConstraint = wc
                self.sendSubview(toBack: v)
                
                let btnDetail = UIButton.init(frame: CGRect.init(x: self.paddingLeftRightCell, y: self.paddingTopCell, width: self.buttonWidth, height: self.frame.height - self.paddingTopCell*2))
                btnDetail.setTitle(self._titleLeft, for: .normal)
                btnDetail.setTitleColor(UIColor.lightGray, for: .normal)
                btnDetail.titleLabel?.font = self.fontTitle
                btnDetail.setImage(UIImage.init(named: self._nameImageLeft)?.resizeImage(targetSize: self.sizeIconButton), for: .normal)
                btnDetail.backgroundColor = self.colorButton
                btnDetail.centerImageAndButton(8.0, imageOnTop: true)
                btnDetail.tag = 1
                btnDetail.isEnabled = true
                btnDetail.isUserInteractionEnabled = true
                btnDetail.addTarget(self, action:#selector(clickButton), for: .touchUpInside)
                v.addSubview(btnDetail)
            }
        }
    }
    
    @objc func clickButton(sender:UIButton!) {
        if let d = delegate {
            if (self.leftView) != nil {
                if sender.tag == 1{
                    print("Click Left Button")
                    if let index = self.indexPath {
                        d.DidTapLeftButton_1_Style2(cell: self, indexPath: index)
                    }
                }
            }
            if (self.rightView) != nil {
                if sender.tag == 2 {
                    print("Click Right Button 1")
                    if let index = self.indexPath {
                        d.DidTapRightButton_1_Style2(cell: self, indexPath: index)
                    }
                }else if sender.tag == 3 {
                    print("Click Right Button 2")
                    if let index = self.indexPath {
                        d.DidTapRightButton_2_Style2(cell: self, indexPath: index)
                    }
                }else if sender.tag == 4 {
                    print("Click Right Button 3")
                    if let index = self.indexPath {
                        d.DidTapRightButton_3_Style2(cell: self, indexPath: index)
                    }
                }
            }
        }
        
        //closeButtonsIfShown
        if self.contentView.frame.origin.x != 0 {
            //Yes Animation
            UIView.animate(withDuration: self.animationDuration, delay: self.animationDelay, usingSpringWithDamping: self.animationSpingDamping, initialSpringVelocity: self.animationInitialVelocity, options: self.animationOptions, animations: { () -> Void in
                self.contentView.frame.origin.x = 0
                self.panRecognizer.isEnabled = false
                self.panRecognizer.isEnabled = true
            }, completion: nil)
            
            //No Animation
            //self.contentView.frame.origin.x = 0
            //self.panRecognizer.isEnabled = false
            //self.panRecognizer.isEnabled = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addViewSwipe()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addViewSwipe()
    }
    
    private func addViewSwipe() {
        if !isSwipeLeft && !isSwipeRight {
            return
        }else {
            let pan = UIPanGestureRecognizer(target: self, action:#selector(didPan) )
            pan.delegate = self
            self.addGestureRecognizer(pan)
            self.panRecognizer = pan
            
            let tap = UITapGestureRecognizer(target: self, action:#selector(didTap) )
            tap.delegate = self
            self.addGestureRecognizer(tap)
            self.btnTabDefaulCell = tap
        }
    }
    
    //Sự kiên click vào cell --> dịch chuyển frame.x content view về default
    @objc func didTap(sender : UITapGestureRecognizer) {
        UIView.animate(withDuration: self.animationDuration, delay: self.animationDelay, usingSpringWithDamping: self.animationSpingDamping, initialSpringVelocity: self.animationInitialVelocity, options: self.animationOptions, animations: { () -> Void in
            print("Click Tab Defaul Cell")
            self.contentView.frame.origin.x = 0
        }, completion: nil)
    }
    
    //Sự kiện Pan chạm và thay đổi vị trí chạm --> swipe Left To Right || swipe Right To Left --> dịch chuyển frame.x contentView theo chiều dài của button left và right
    @objc func didPan(sender: UIPanGestureRecognizer) {
        if !isSwipeLeft && !isSwipeRight {
            return
        }
        switch sender.state {
        case .began:
            print("Swipe Begin")
            if let index = self.indexPath {
                self.delegate?.DidTapSwipeBegin_Style2(cell: self, indexPath: index)
            }
            self.beginPointContentViewX = sender.location(in: self) //Lấy vị trí chạm-kéo-đầu
            self.beginPointContentViewX.x -= self.contentView.frame.origin.x //Lưu lại vị trí frame.x contentView khi bắt đầu chạm-kéo-đầu
            
            if self._isSwipeLeft {
                self.leftView = UIView.init(frame: CGRect())
                self.leftView.frame.origin.x = self.leftView.frame.origin.x - self.buttonWidth*self.countBtnLeft
            }
            if self._isSwipeRight {
                self.rightView = UIView.init(frame: CGRect())
                self.rightView.frame.origin.x = self.rightView.frame.origin.x + self.buttonWidth*self.countBtnRight
            }
            break
        case .changed:
            let now = sender.location(in: self)
            let distX = now.x - self.beginPointContentViewX.x //Khoảng cách distX = tính theo X giữa vị trí chạm-kéo-sau và chạm-kéo-đầu
            if distX <= 0 && self._isSwipeRight {
                print("Swipe Right To Left")
                let d = max(distX,-(self.contentView.frame.size.width-self.buttonWidth)) //Giá trị lớn nhất giữa distX và self.buttonWidth
                if d > -self.buttonWidth*2 || self.rightView != nil || self.contentView.frame.origin.x > 0 {
                    
                    self.contentView.frame.origin.x = d//-self.buttonWidth // || d Set frame.x
                    self.rightView.frame.origin.x = self.contentView.frame.origin.x + self.contentView.frame.width
                }else {
                    sender.isEnabled = false
                    sender.isEnabled = true
                }
            }else if self._isSwipeLeft {
                print("Swipe Left To Right")
                let d = min(distX,self.contentView.frame.size.width-self.buttonWidth)//Giá trị lớn nhất giữa distX và self.buttonWidth
                if d < self.buttonWidth*2 || self.leftView != nil || self.contentView.frame.origin.x < 0 {
                    
                    self.contentView.frame.origin.x = d//self.buttonWidth  // || d Set frame.x
                    self.leftView.frame.origin.x = self.contentView.frame.origin.x - self.buttonWidth
                }else {
                    sender.isEnabled = false
                    sender.isEnabled = true
                }
            }
            break
        default:
            if let index = self.indexPath {
                delegate?.DidTapSwipeBegin_Style2(cell: self, indexPath: index)
            }
            let offset = self.contentView.frame.origin.x
            if offset > self.buttonWidth && self.leftView != nil {
                UIView.animate(withDuration: self.animationDuration, delay: self.animationDelay, usingSpringWithDamping: self.animationSpingDamping, initialSpringVelocity: self.animationInitialVelocity, options: self.animationOptions, animations: { () -> Void in
                    
                    self.contentView.frame.origin.x = self.buttonWidth*self.countBtnLeft
                    self.leftView.frame.origin.x = self.contentView.frame.origin.x - self.buttonWidth
                    
                }, completion: nil)
            }else if -offset > self.buttonWidth && self.rightView != nil {
                UIView.animate(withDuration: self.animationDuration, delay: self.animationDelay, usingSpringWithDamping: self.animationSpingDamping, initialSpringVelocity: self.animationInitialVelocity, options: self.animationOptions, animations: { () -> Void in
                    
                    self.contentView.frame.origin.x = -self.buttonWidth*self.countBtnRight
                    self.rightView.frame.origin.x = self.contentView.frame.origin.x + self.contentView.frame.width
                    
                }, completion: nil)
            }
            break
        }
    }
    
    //Kiếm tra và lựa chọn xử lý theo UITapGestureRecognizer hay UIPanGestureRecognizer
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let tap = gestureRecognizer as? UITapGestureRecognizer {
            if tap == self.btnTabDefaulCell {
                return self.contentView.frame.origin.x != 0 //True --> xử lý sự kiện UITapGestureRecognizer
            }
            else {
                return super.gestureRecognizerShouldBegin(gestureRecognizer)
            }
        }else if let pan = gestureRecognizer as? UIPanGestureRecognizer {
            let trans = pan.translation(in: self)
            if abs(trans.x) > abs(trans.y) { //Xử lý khi swipe left to right or right to left
                if self.contentView.frame.origin.x != 0 {
                    self.contentView.frame.origin.x = 0
                    return false // Tắt GestureRecognizer --> Không xử lý khi frame.x ViewContent đang được swipe != 0 --> Chuyển sang vị trí mặc định khi đang được swipe
                }else{
                    return true // Mở GestureRecognizer --> Xử lý theo vị trí swipe
                }
            }else if self.contentView.frame.origin.x != 0 { //Xử lý khi swipe top to bottom OR bottom to top
                return true // Mở GestureRecognizer --> Không xử lý khi swipe --> swipe không phải left to right OR right to left
            }else {
                return false // Tắt GestureRecognizer --> Cuộn ngoài cell TableView
            }
        }else {
            return super.gestureRecognizerShouldBegin(gestureRecognizer)
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let showing = self.contentView.frame.origin.x != 0
        if !showing {
            super.setHighlighted(highlighted, animated: animated)
            self.rightView?.alpha = showing || !highlighted ? 1 : 0
            self.leftView?.alpha = showing || !highlighted ? 1 : 0
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        let showing = self.contentView.frame.origin.x != 0
        if !showing {
            super.setSelected(selected, animated: animated)
            self.rightView?.alpha = showing || !selected ? 1 : 0
            self.leftView?.alpha = showing || !selected ? 1 : 0
        }
    }
    
}
