//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import DeviceKit

public class CustomTextField: UITextField {
    private static let groupOfSmall: [Device] = [.iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE, .simulator(.iPhone5), .simulator(.iPhone5c), .simulator(.iPhone5s), .simulator(.iPhoneSE)]
    private static let groupOfMedium: [Device] = [.iPhone6, .iPhone6s, .iPhone7, .iPhone8, .simulator(.iPhone6), .simulator(.iPhone6s), .simulator(.iPhone7), .simulator(.iPhone8)]
    private static let groupOfLarge: [Device] = [.iPhone6Plus,.iPhone6sPlus, .iPhone7Plus, .iPhone8Plus, .iPhoneX, .simulator(.iPhone6Plus), .simulator(.iPhone6sPlus), .simulator(.iPhone7Plus), .simulator(.iPhone8Plus), .simulator(.iPhoneX)]
    
    public static var FONT_TEXT_TITLE: CGFloat {
        if Device.current.isOneOf(groupOfSmall) {
            return 11
        } else if Device.current.isOneOf(groupOfLarge) {
            return 14
        }
        return 13
    }
    
    var viewValidate = UIView()
    let viewHeight = CGFloat(30.0)
    let viewColorUnselect = UIColor.clear
    let viewColorSelect = UIColor.clear
    
    var viewLine = UIView()
    let lineHeight = CGFloat(2.0)
    var _lineColorUnselect = UIColor.rgb(fromHexString: "#007BB5")
    public var lineColorUnselect :UIColor {
        get{
            return self._lineColorUnselect
        }
        set(newValue){
            self._lineColorUnselect = newValue
        }
    }
    var _lineColorSelect = UIColor.rgb(fromHexString: "#007BB5")
    public var lineColorSelect :UIColor {
        get{
            return self._lineColorSelect
        }
        set(newValue){
            self._lineColorSelect = newValue
        }
    }
    var labelTitle = UILabel()
    public let fontTitle = UIFont.init(name: "Roboto-Regular", size: CustomTextField.FONT_TEXT_TITLE)
    private var _err: String? = "Giá trị nhập không hợp lệ !" {
        didSet{
            self.labelTitle.text = self._err
        }
    }
    public var textValidate :String? {
        get{
            return self._err
        }
        set(newValue){
            self._err = newValue
        }
    }
    
    var iconDanger = UIImageView()
    let iconHeight = CGFloat(20.0) // iconHeight < viewHeight
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        if newWindow == nil {
            // UIView disappear
        } else {
            // UIView appear
            addLineFocous()
        }
    }
    
    public func addLineFocous() {
        for item in subviews{
            item.removeFromSuperview()
        }
        self.layer.masksToBounds = false
        viewLine =  UIView.init(frame: CGRect(x: 0, y: frame.size.height - lineHeight, width: frame.size.width, height: lineHeight))
        viewLine.backgroundColor = _lineColorUnselect
        addSubview(viewLine)
        
        viewValidate = UIView.init(frame: CGRect(x: 0, y: frame.size.height, width: frame.size.width, height: viewHeight))
        viewValidate.backgroundColor = viewColorUnselect
        
        labelTitle = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: viewValidate.frame.width - iconHeight, height: viewValidate.frame.height))
        labelTitle.font = self.fontTitle
        labelTitle.text = self._err
        labelTitle.textColor = UIColor.rgb(fromHexString: "#ff6e6e")
        labelTitle.adjustsFontSizeToFitWidth = true
        labelTitle.minimumScaleFactor = 0.5
        
        let positionIconY = (viewValidate.frame.height - iconHeight)/2
        iconDanger = UIImageView.init(frame: CGRect.init(x: viewValidate.frame.width - iconHeight, y: positionIconY, width: iconHeight, height: iconHeight))
        iconDanger.image = UIImage.init(named: "icondanger.png")?.resizeImage(targetSize: CGSize.init(width: iconHeight, height: iconHeight))
        viewValidate.addSubview(labelTitle)
        viewValidate.addSubview(iconDanger)
        
        addSubview(viewValidate)
        
        viewLine.isHidden = false
        viewValidate.isHidden = true
        
        tintColor = _lineColorSelect
    }
    
    //Out Focus
    public override func resignFirstResponder() -> Bool {
        viewLine.backgroundColor = _lineColorUnselect
        return super.resignFirstResponder()
    }
    
    //In Focus
    public override func becomeFirstResponder() -> Bool {
        viewLine.backgroundColor = _lineColorSelect
        return super.becomeFirstResponder()
    }
    
    public func setIsValidate(_ isValue: Bool){
        if isValue {
            viewLine.backgroundColor = _lineColorUnselect
            viewLine.isHidden = false
            
            viewValidate.backgroundColor = viewColorUnselect
            viewValidate.isHidden = true
        }else{
            viewLine.backgroundColor = _lineColorSelect
            viewLine.isHidden = false
            
            viewValidate.backgroundColor = viewColorSelect
            viewValidate.isHidden = false
        }
    }
}
