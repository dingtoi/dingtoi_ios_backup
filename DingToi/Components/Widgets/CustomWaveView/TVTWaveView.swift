//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class TVTWaveView: UIView {
    public enum STYLEWAVE {
        case SINGLE
        case MUTIL
    }
    private var displayLink: CADisplayLink?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    private func initView() {
    }
    
    public func stopAnimating() {
        displayLink?.invalidate()
        displayLink = nil
    }
    
    public func startAnimating(_ style: STYLEWAVE = STYLEWAVE.MUTIL) {
        if style == .SINGLE {
            self.addSingle()
        }else if style == .MUTIL {
            self.addMutil()
        }
    }
    
    // STYLE SIGNLE -------------------
    private var startTime: CFAbsoluteTime?
    private let singleLayer: CAShapeLayer = {
        let _layer = CAShapeLayer()
        _layer.backgroundColor = UIColor.clear.cgColor //Màu nền
        _layer.lineWidth = 3
        _layer.strokeColor = UIColor.red.cgColor //Màu đường vẽ
        _layer.fillColor = UIColor.clear.cgColor //Màu nền trong khối vẽ
        _layer.strokeStart = 0
        _layer.strokeEnd = 1
        return _layer
    }()
    
    private func addSingle() {
        for layer in self.layer.sublayers ?? [] {
            layer.removeFromSuperlayer()
        }
        self.layer.addSublayer(singleLayer)
        
        startTime = CFAbsoluteTimeGetCurrent()
        displayLink?.invalidate()
        displayLink = CADisplayLink(target: self, selector:#selector(handleWave_Single(_:)))
        displayLink?.add(to: RunLoop.current, forMode: .commonModes)
    }
    
    @objc func handleWave_Single(_ displayLink: CADisplayLink) {
        let elapsed = CFAbsoluteTimeGetCurrent() - startTime! //Tổng thời gian hoạt động
        singleLayer.path = self.wave_Single(at: elapsed).cgPath
    }
    
    private func wave_Single(at elapsed: Double) -> UIBezierPath {
        let centerY: CGFloat = self.bounds.height / 2
        let bienDo: CGFloat = 10.0 //CGFloat(50) - fabs(fmod(CGFloat(elapsed), 3) - 1.5) * 40
        
        //Hàm tính tọa điểm liên tiếp của đường SIN theo biên độ + thời gian đã trôi qua elapsed
        func tinhY(_ x: Int) -> CGFloat {
            return sin( ( (CGFloat(x)/self.bounds.width) + CGFloat(elapsed)) * 4 * .pi) * bienDo + centerY
        }
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: tinhY(0))) //tinhY(0) == centerY
        
        // Vẽ đường cong nối tiếp từ CGPoint(x: 0, y: f(0)) --> CGPoint(x: CGFloat(X), y: tinhY(X))
        // Với X tăng dần từ 0 --> self.bounds.width
        // 10 --> mức độ di chuyển tọa độ X --> để tăng tốc độ --> nhưng giảm độ mịn của đường vẽ
        // 9 --> không vẽ vượt quá biên --> nếu là 10 thì bị vẽ vượt qua biên 1 lần với khoảng cách 10
        for x in stride(from: 0, to: Int(self.bounds.width + 9), by: 10) {
            path.addLine(to: CGPoint(x: CGFloat(x), y: tinhY(x)))
        }
        //Đóng khối
        //path.addLine(to: CGPoint(x: Int(self.bounds.width + 3), y: 0)) // 3 --> cộng thêm độ dày của đường
        //path.addLine(to: CGPoint(x: 0, y: 0))
        //path.addLine(to: CGPoint(x: 0, y: tinhY(0))) //tinhY(0) == centerY
        
        //path.close() //Tự động đóng khối nối với điểm bắt đầu
        return path
    }
    // STYLE SIGNLE ------------------- ./
    
    
    // STYLE MUTIL -------------------
    private var waveWidthRange: CGFloat = 0
    private var waveHeightRange: CGFloat = 0
    private var isIncrement = false // Chiều lướt hiện tại
    private var rate: CGFloat = 0.12
    public var isAutoHeight: Bool = false // Tự động tăng giảm chiều cao
    public var padBottom: CGFloat = 0.0 // Tỉ lệ padding bottom
    public var waveSpeed: CGFloat = 4 //Tốc độ lướt default = 7
    public var waveHeight: CGFloat = 15.0 {
        didSet {
            self.layoutIfNeeded()
            self.rate = waveHeight / bounds.height
        }
    }
    private let waveLayerBelow: CAShapeLayer = {
        let _layer = CAShapeLayer()
        _layer.backgroundColor = UIColor.clear.cgColor //Màu nền
        _layer.lineWidth = 1
        _layer.strokeColor = UIColor.clear.cgColor //Màu đường vẽ
        _layer.fillColor = UIColor.white.cgColor //Màu nền trong khối vẽ
        _layer.strokeStart = 0
        _layer.strokeEnd = 1
        return _layer
    }()
    private let waveLayerMid: CAShapeLayer = {
        let _layer = CAShapeLayer()
        _layer.backgroundColor = UIColor.clear.cgColor //Màu nền
        _layer.lineWidth = 1
        _layer.strokeColor = UIColor.rgb(fromHexString: "#3D3C85").cgColor //Màu đường vẽ
        _layer.fillColor = UIColor.clear.cgColor //Màu nền trong khối vẽ
        _layer.strokeStart = 0
        _layer.strokeEnd = 1
        return _layer
    }()
    private let waveLayerAbove: CAShapeLayer = {
        let _layer = CAShapeLayer()
        _layer.backgroundColor = UIColor.clear.cgColor //Màu nền
        _layer.lineWidth = 1
        _layer.strokeColor = UIColor.clear.cgColor //Màu đường vẽ
        _layer.fillColor = UIColor.clear.cgColor //Màu nền trong khối vẽ
        _layer.strokeStart = 0
        _layer.strokeEnd = 1
        return _layer
    }()
    
    private func addMutil() {
        for layer in self.layer.sublayers ?? [] {
            layer.removeFromSuperlayer()
        }
        self.layer.addSublayer(waveLayerBelow)
        self.layer.addSublayer(waveLayerMid)
        self.layer.addSublayer(waveLayerAbove)
        
        displayLink?.invalidate()
        displayLink = CADisplayLink(target: self, selector:#selector(handleWave_Mutil(_:)))
        displayLink?.add(to: RunLoop.current, forMode: .commonModes)
    }
    
    @objc func handleWave_Mutil(_ displayLink: CADisplayLink) {
        self.wave_Mutil()
    }
    
    private func wave_Mutil() {
        if self.isAutoHeight {
            //Tính toán tự động tăng - giảm chiều cao
            if isIncrement {
                if rate >= 0.65 {
                    isIncrement = false
                }else {
                    waveWidthRange += 7
                    rate += 0.0005
                }
            }else {
                if rate <= 0.35 {
                    isIncrement = true
                }else {
                    rate -= 0.0005
                    waveWidthRange -= 7
                }
            }
        }
        
        // Tính toán tự động đảo chiều
        if isIncrement {
            if waveWidthRange >= CGFloat.greatestFiniteMagnitude {
                isIncrement = false
            }else {
                waveWidthRange += waveSpeed
            }
        }else {
            if waveWidthRange <= 0 {
                isIncrement = true
            }else {
                waveWidthRange -= waveSpeed
            }
        }
        
        waveHeightRange = self.bounds.height * self.rate * (1.0 - self.padBottom)
        let wavePathBelow = UIBezierPath()
        let wavePathAbove = UIBezierPath()
        let wavePathMid = UIBezierPath()
        var y1 = (1 - rate) * bounds.height
        var y2 = y1
        wavePathBelow.move(to: CGPoint(x: 0, y: y1))
        wavePathAbove.move(to: CGPoint(x: 0, y: y2))
        let a = 2.1 / bounds.width * CGFloat(CGFloat.pi) //Độ lệch pha
        for x in 0 ... Int(bounds.width) {
            let b1 = 2 * waveWidthRange / bounds.width * CGFloat.pi
            let b2 = (waveWidthRange / bounds.width) * CGFloat.pi
            let c = (1 - rate) * bounds.height
            y1 = waveHeightRange * CGFloat(sin(Double(a * CGFloat(x) + b1))) + c
            y2 = waveHeightRange * CGFloat(sin(Double(a * CGFloat(x) + b2))) + c
            wavePathBelow.addLine(to: CGPoint(x: CGFloat(x), y: y1))
            wavePathAbove.addLine(to: CGPoint(x: CGFloat(x), y: y2))
            if y2 > y1 {
                wavePathMid.move(to: CGPoint(x: CGFloat(x), y: y1))
                wavePathMid.addLine(to: CGPoint(x: CGFloat(x), y: y2))
            }
        }
        //Đóng khối
        wavePathBelow.addLine(to: CGPoint(x: bounds.width, y: bounds.height))
        wavePathBelow.addLine(to: CGPoint(x: 0, y: bounds.height))
        //wavePathBelow.close() //Tự động đóng khối nối với điểm bắt đầu
        waveLayerBelow.path = wavePathBelow.cgPath
        
        waveLayerMid.path = wavePathMid.cgPath
        
        //Đóng khối
        wavePathAbove.addLine(to: CGPoint(x: bounds.width, y: bounds.height))
        wavePathAbove.addLine(to: CGPoint(x: 0, y: bounds.height))
        //wavePathAbove.close() //Tự động đóng khối nối với điểm bắt đầu
        waveLayerAbove.path = wavePathAbove.cgPath
    }
    // STYLE MUTIL ------------------- ./
}

