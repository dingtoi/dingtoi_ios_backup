//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class ExampleSVPinView: UIViewController {
    
    @IBOutlet var pinView:SVPinView!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SVPinView"
        
        configurePinView()
    }
    
    public func configurePinView() {
        pinView.pinLength = 5
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 5
        pinView.textColor = UIColor.black
        pinView.underlineColor = UIColor.black
        pinView.underLineThickness = 2
        pinView.shouldSecureText = true
        
        pinView.font = UIFont.systemFont(ofSize: 15)
        pinView.keyboardType = .phonePad
        pinView.pinIinputAccessoryView = UIView()
        
        pinView.didFinishCallback = didFinishEnteringPin(pin:)
    }
    
    @IBAction func printPin() {
        let pin = pinView.getPin()
        guard !pin.isEmpty else {
            showAlert(title: "Error", message: "Pin entry incomplete")
            return
        }
        showAlert(title: "Success", message: "The Pin entered is \(pin)")
    }
    
    @IBAction func clearPin() {
        pinView.clearPin()
    }
    
    public func didFinishEnteringPin(pin:String) {
        showAlert(title: "Success", message: "The Pin entered is \(pin)")
    }
    
    public func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
