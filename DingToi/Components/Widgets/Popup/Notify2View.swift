//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import SnapKit
import KGModal

@objc protocol Notify2ViewDelegate {
    func btnCloseDialogPressed()
}

public class Notify2View: UIView {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    public var message:String? {
        didSet {
            self.lblMessage.text = message
        }
    }
    
    public var titleButton:String? {
        didSet {
            self.btnClose.titleLabel?.text = titleButton
        }
    }
    
    weak var delegate:Notify2ViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadXib(xibName: String(describing: Notify2View.self))
        self.initSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadXib(xibName: String(describing: Notify2View.self))
        self.initSubviews()
    }
    
    private func initSubviews () {
        self.lblMessage.text = NSLocalizedString("popup.login.title", comment: "")
        self.btnClose.setTitle(NSLocalizedString("popup.login.button", comment: ""), for: .normal)
        self.btnClose.setupDefaultBorder()
    }
    
    @IBAction func btnClosePressed() {
        KGModal.sharedInstance().hide(animated: true)
        self.delegate?.btnCloseDialogPressed()
    }
}
