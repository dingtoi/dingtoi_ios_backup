//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

class ExampleFaveButton: UIViewController {
    
    @IBOutlet weak var _btnOK2: FaveButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addEventFaveButton()
    }
    
    func addEventFaveButton() {
        _btnOK2.delegate = self
        _btnOK2.setSelected(selected: true, animated: false)
        _btnOK2.setSelected(selected: true, animated: false)
        _btnOK2.setSelected(selected: false, animated: false)
    }
    
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        
    }
    
    func faveButtonDotColors(_ faveButton: FaveButton) -> [DotColors]? {
        if faveButton == _btnOK2 {
            return [
                DotColors(first: color(0x7DC2F4), second: color(0xE2264D)),
                DotColors(first: color(0xF8CC61), second: color(0x9BDFBA)),
                DotColors(first: color(0xAF90F4), second: color(0x90D1F9)),
                DotColors(first: color(0xE9A966), second: color(0xF8C852)),
                DotColors(first: color(0xF68FA7), second: color(0xF6A2B8))
            ]
        }
        return nil
    }
    
    func color(_ rgbColor: Int) -> UIColor{
        return UIColor(
            red:   CGFloat((rgbColor & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbColor & 0x00FF00) >> 8 ) / 255.0,
            blue:  CGFloat((rgbColor & 0x0000FF) >> 0 ) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
