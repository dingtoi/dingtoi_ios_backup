//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

@objc public enum LTMorphingEffect: Int, CustomStringConvertible {
    
    case scale = 0
    case evaporate
    case fall
    case pixelate
    case sparkle
    case burn
    case anvil
    
    public static let allValues = [
        "Scale", "Evaporate", "Fall", "Pixelate", "Sparkle", "Burn", "Anvil"
    ]
    
    public var description: String {
        switch self {
        case .evaporate:
            return "Evaporate"
        case .fall:
            return "Fall"
        case .pixelate:
            return "Pixelate"
        case .sparkle:
            return "Sparkle"
        case .burn:
            return "Burn"
        case .anvil:
            return "Anvil"
        default:
            return "Scale"
        }
    }
    
}

