//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

class LTMorphingLabelView: UIView, LTMorphingLabelDelegate {
    
    @IBOutlet weak var _lbHello: LTMorphingLabel!
    
    func addMorphingLabel() {
        _lbHello.delegate = self
        _lbHello.tag = 0
        _lbHello.isUserInteractionEnabled = true
        _lbHello.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(_btnMorphingLabelClick)))
    }
    
    @objc func _btnMorphingLabelClick(sender: UITapGestureRecognizer) {
        print("Click")
        switch _lbHello.tag%7 {
        case 0:
            _lbHello.morphingEffect = .sparkle
            break
        case 1:
            _lbHello.morphingEffect = .burn
            break
        case 2:
            _lbHello.morphingEffect = .anvil
            break
        case 3:
            _lbHello.morphingEffect = .pixelate
            break
        case 4:
            _lbHello.morphingEffect = .scale
            break
        case 5:
            _lbHello.morphingEffect = .evaporate
            break
        case 6:
            _lbHello.morphingEffect = .fall
            break
        default:
            _lbHello.morphingEffect = .burn
            break
        }
        _lbHello.tag = _lbHello.tag + 1
        _lbHello.text = ""
        _lbHello.text = "Click Me"
    }
    
    func morphingDidStart(_ label: LTMorphingLabel) {
        
    }
    
    func morphingDidComplete(_ label: LTMorphingLabel) {
        
    }
    
    func morphingOnProgress(_ label: LTMorphingLabel, progress: Float) {
        
    }
    
}
