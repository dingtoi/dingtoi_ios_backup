//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import CoreData

public struct Const {
    public struct CoreData {
        public static let FullRes = "FullRes"
        public static let Thumbnail = "Thumbnail"
    }
}

//CoreDataEx - BaseViewController
//func setupCoreData() {
//    Run.sync(coreDataQueue) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        self.managedContext = appDelegate.managedObjectContext
//        self.deleteImageAll {
//        }
//    }
//}
//
//let imageProcessingQueue = DispatchQueue(label: "imageProcessingQueue", attributes: DispatchQueue.Attributes.concurrent)
//let coreDataQueue = DispatchQueue(label: "coreDataQueue")
//var managedContext : NSManagedObjectContext?
//CoreDataEx - BaseViewController ./

extension BaseViewController {
    //Note: Thumbnail
    //thumbnail.imageData = thumbnailData --> image thumb
    //thumbnail.id = id_date as NSNumber --> id
    //thumbnail.fullRes = fullRes --> fullRes.imageData = imageData --> image full resolution
    
    //Save image in the App - save FullRes && save Thumbnail by Data
    public func prepareImageForSaving(image: UIImage, done: @escaping (Double, UIImage?) -> Void) {
        // use date as unique id
        let id_date : Double = Date().timeIntervalSince1970
        
        self.showProgress()
        
        //Run.async(imageProcessingQueue) {
            // create NSData from UIImage
            guard let imageData = UIImageJPEGRepresentation(image, 0.6) else {
                // handle failed conversion
                print("==> jpg error")
                return
            }
            
            // scale image
            let thumbnail = image.scale(toSize: self.view.frame.size)
            
            guard let thumbnailData  = UIImageJPEGRepresentation(thumbnail, 0.1) else {
                // handle failed conversion
                print("==> jpg error")
                return
            }
            
            self.hideProgress()
            
            // send to save function
            self.saveImage(imageData, thumbnailData: thumbnailData, id_date: id_date, done: done)
        //}
    }
    
    public func saveImage(_ imageData: Data, thumbnailData: Data, id_date: Double, done: @escaping (Double, UIImage?) -> Void) {
        self.showProgress()
        
        // create new objects in moc
        //Run.async(coreDataQueue) {
            guard let moc = self.managedContext else {
                return
            }
            
            let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateMOC.parent = moc
            
            privateMOC.perform {//performAndWait
                guard let fullRes = NSEntityDescription.insertNewObject(forEntityName: Const.CoreData.FullRes, into: privateMOC) as? FullRes,
                    let thumbnail = NSEntityDescription.insertNewObject(forEntityName: Const.CoreData.Thumbnail, into: privateMOC) as? Thumbnail else {
                    // handle failed new object in moc
                    print("==> moc error")
                    return
                }
                
                //set image data of fullres
                fullRes.imageData = imageData
                
                //set image data of thumbnail
                thumbnail.imageData = thumbnailData
                thumbnail.id = id_date as NSNumber
                thumbnail.fullRes = fullRes
                
                // save the new objects
                do {
                    try privateMOC.save()
                    moc.performAndWait {//performAndWait
                        do {
                            try moc.save()
                            moc.refreshAllObjects()
                            privateMOC.refreshAllObjects()
                            self.hideProgress()
                            print("==> Write: \(id_date)")
                            done(id_date, thumbnail.imageThumb)
                        } catch {
                            fatalError("==> Failure to save context: \(error)")
                        }
                    }
                } catch {
                    fatalError("==> Failure to save context: \(error)")
                }
            }
        //}
    }
    
    //Load images saved with Id by the App --> Thumbnail
    //thumbnail.imageData - thumbnail.id - thumbnail.fullRes.imageData
    public func loadImageWithId(_ id_date: Double,_ fetched:@escaping (_ image: Thumbnail) -> Void) {
        self.showProgress()
        
        //Run.async(coreDataQueue) {
            guard let moc = self.managedContext else {
                return
            }
            
            let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateMOC.parent = moc
            
            privateMOC.perform { //performAndWait
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Const.CoreData.Thumbnail)
                do {
                    let results = try privateMOC.fetch(fetchRequest)
                    let maybeImageData = results as? [Thumbnail]
                    
                    guard let imageData = maybeImageData else {
                        //Run.main {
                            self.noImagesFound()
                        //}
                        return
                    }
                    
                    self.hideProgress()
                    //Run.main {
                    print("==> Size CoreData: \(imageData.count)")
                        for thumbnail in imageData {
                            if thumbnail.imageData != nil && thumbnail.id != nil && thumbnail.id == (id_date as NSNumber) {
                                print("==> Read: \(id_date)")
                                fetched(thumbnail)
                                return
                            }
                        }
                    //}
                } catch {
                    self.hideProgress()
                    //Run.main {
                        self.noImagesFound()
                    //}
                    return
                }
            }
        //}
    }
    
    //Load all images thumb saved by the App --> [Thumbnail]
    //thumbnail.imageData - thumbnail.id - thumbnail.fullRes.imageData
    public func loadImageAll(_ fetched:@escaping (_ imagesThumb: [Thumbnail]) -> Void) {
        self.showProgress()
        
        //Run.async(coreDataQueue) {
            guard let moc = self.managedContext else {
                return
            }
            
            let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateMOC.parent = moc
            
            privateMOC.perform { //performAndWait
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Const.CoreData.Thumbnail)
                
                do {
                    let results = try privateMOC.fetch(fetchRequest)
                    let maybeImageData = results as? [Thumbnail]
                    
                    guard let imageData = maybeImageData else {
                        //Run.main {
                            self.noImagesFound()
                        //}
                        return
                    }
                    
                    self.hideProgress()
                    //Run.main {
                        fetched(imageData.filter { thumbnail in
                            return thumbnail.imageData != nil && thumbnail.id != nil
                        })
                    //}
                } catch {
                    self.hideProgress()
                    //Run.main {
                        self.noImagesFound()
                    //}
                    return
                }
            }
        //}
    }
    
    //Delete all images saved by the App
    public func deleteImageAll(_ done: @escaping () -> Void) {
        self.showProgress()
        
        //Run.async(coreDataQueue) {
            guard let moc = self.managedContext else {
                return
            }
            
            let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateMOC.parent = moc
            
            privateMOC.perform { //performAndWait
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Const.CoreData.Thumbnail)
                do {
                    let results = try privateMOC.fetch(fetchRequest)
                    for item in results {
                        if let object = item as? NSManagedObject {
                            privateMOC.delete(object)
                        }
                    }
                } catch {
                    self.hideProgress()
                    //Run.main {
                        done()
                    //}
                    return
                }
                
                // save the delete op
                do {
                    try privateMOC.save()
                    moc.performAndWait { //performAndWait
                        do {
                            try moc.save()
                            moc.refreshAllObjects()
                            self.hideProgress()
                            //Run.main {
                                done()
                            //}
                        } catch {
                            fatalError("==> Failure to save context: \(error)")
                        }
                    }
                } catch {
                    fatalError("==> Failure to save context: \(error)")
                }
            }
        //}
    }
    
    //Delete images saved with Id by the App
    public func deleteImageWithId(_ id_date: Double,_ done: @escaping () -> Void) {
        self.showProgress()
        
        //Run.async(coreDataQueue) {
            guard let moc = self.managedContext else {
                return
            }
            
            let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateMOC.parent = moc
            
            privateMOC.perform { //performAndWait
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Const.CoreData.Thumbnail)
                do {
                    let results = try privateMOC.fetch(fetchRequest)
                    if let listResults = results as? [Thumbnail] {
                        for thumbnail in  listResults {
                            if thumbnail.id != nil && thumbnail.id == (id_date as NSNumber) {
                                print("Remove: \(thumbnail.id!)")
                                privateMOC.delete(thumbnail)
                            }
                        }
                    }
                } catch {
                    self.hideProgress()
                    //Run.main {
                        done()
                    //}
                    return
                }
                
                // save the delete op
                do {
                    try privateMOC.save()
                    moc.performAndWait { //performAndWait
                        do {
                            try moc.save()
                            moc.refreshAllObjects()
                            self.hideProgress()
                            //Run.main {
                                done()
                            //}
                        } catch {
                            fatalError("==> Failure to save context: \(error)")
                        }
                    }
                } catch {
                    fatalError("==> Failure to save context: \(error)")
                }
            }
        //}
    }
    
    public func cantOpenPicker(withSource source:UIImagePickerControllerSourceType) {
        let sourceName : String
        
        switch source {
            case .camera : sourceName = "Camera"
            case .photoLibrary : sourceName = "Photo Library"
            case .savedPhotosAlbum : sourceName = "Saved Photos Album"
        }
        
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        let alertVC = UIAlertController(title: "Sorry", message: "Can't access your \(sourceName)", preferredStyle: .alert)
        
        alertVC.addAction(alertAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    public func noImagesFound() {
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        let alertVC = UIAlertController(title: "No Images Found", message: "There were no images saved in Core Data", preferredStyle: .alert)
        
        alertVC.addAction(alertAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
}


extension UIImage {
    public func scale(toSize newSize:CGSize) -> UIImage {
        // make sure the new size has the correct aspect ratio
        let aspectFill = self.size.resizeFill(newSize)
        
        UIGraphicsBeginImageContextWithOptions(aspectFill, false, 0.0);
        self.draw(in: CGRect(x: 0, y: 0, width: aspectFill.width, height: aspectFill.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

extension CGSize {
    public func resizeFill(_ toSize: CGSize) -> CGSize {
        let scale : CGFloat = (self.height / self.width) < (toSize.height / toSize.width) ? (self.height / toSize.height) : (self.width / toSize.width)
        return CGSize(width: (self.width / scale), height: (self.height / scale))
    }
}
