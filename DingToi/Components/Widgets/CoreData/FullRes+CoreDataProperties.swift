//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import CoreData

extension FullRes {
    @NSManaged public var imageData: Data?
    @NSManaged public var thumbnail: Thumbnail?
}
