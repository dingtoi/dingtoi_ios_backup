//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation

public struct Run {
    
    /**
     Run Syncronous task
     
     - parameter queue: Queue for the task to run on
     - parameter task:  the Task to be executed
     */
    public static func sync(_ queue: DispatchQueue, task:() -> Void) {
        queue.sync {
            task()
        }
    }
    
    /**
     Run Asyncronous task
     
     - parameter queue: Queue for the task to run on
     - parameter task:  the Task to be executed
     */
    public static func async(_ queue: DispatchQueue, task:@escaping () -> Void) {
        queue.async {
            task()
        }
    }
    
    /**
     Run Syncronous task with a Barrier
     
     - parameter queue: Queue for the task to run on
     - parameter task:  the Task to be executed
     */
    public static func barrierSync(_ queue: DispatchQueue,task:() -> Void) {
        queue.sync(flags: .barrier)  {
            task()
        }
    }
    
    /**
     Run a task on the Main Thread
     
     - parameter task:  the Task to be executed
     */
    public static func main(_ task:@escaping () -> Void) {
        DispatchQueue.main.async {
            task()
        }
    }
}
