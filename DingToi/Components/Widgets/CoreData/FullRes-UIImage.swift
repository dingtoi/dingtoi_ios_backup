//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import UIKit

extension FullRes {
    /// Convenience Property to get set the imageDate with a UIImage
    public var imageFull : UIImage? {
        get {
            if let imageData = imageData {
                return UIImage(data: imageData as Data)
            }
            return nil
        }
        set(value) {
            if let value = value {
                imageData = UIImageJPEGRepresentation(value, 0.6)
            }
        }
    }
}
