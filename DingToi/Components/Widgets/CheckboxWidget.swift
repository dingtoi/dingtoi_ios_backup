//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

protocol CheckboxWidgetDelegate : NSObjectProtocol {
    func check(isChecked: Bool)
}

public class CheckboxWidget: UIImageView {
    weak var delegateCheckboxWidget : CheckboxWidgetDelegate?
    let checkedImage = UIImage(named: "checkbox_on.png")! as UIImage
    let uncheckedImage = UIImage(named: "checkbox_off.png")! as UIImage
    
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.image = checkedImage
            } else {
                self.image = uncheckedImage
            }
        }
    }
    
    override public func awakeFromNib() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
        self.isChecked = false
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        isChecked = !isChecked
        delegateCheckboxWidget?.check(isChecked: isChecked)
    }
    
}

