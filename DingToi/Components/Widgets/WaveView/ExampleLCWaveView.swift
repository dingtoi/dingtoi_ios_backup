//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

// width of the screen
private let screenW = UIScreen.main.bounds.width

// Width of the avatar view
private let iconImageWidth: CGFloat = 100

// Height of the wave view
private let waveViewHeight: CGFloat = 200

// Cell reuse identifier
private let identifier = "cellID"


public class LCWaveViewViewController: UIViewController {
    
    // form
    @IBOutlet weak var table: UITableView!
    
    /** LCWaveView **/
    private lazy var waveView: LCWaveView = {
        let waveView = LCWaveView(frame: CGRect(x: 0, y: 0, width: screenW, height: waveViewHeight), color: .white)
        waveView.waveRate = 2
        waveView.waveSpeed = 1
        waveView.waveHeight = 7
        return waveView
    }()
    
    // avatar view
    private lazy var iconImageView: UIImageView = {
        let image = UIImageView(frame: CGRect(x: screenW / 2 - iconImageWidth / 2, y: 0, width: iconImageWidth, height: iconImageWidth))
        image.layer.borderColor  = UIColor.white.cgColor
        image.layer.cornerRadius = image.bounds.width / 2
        image.layer.masksToBounds = true
        image.layer.borderWidth = 3
        image.layer.contents = UIImage(named: "UserAvatar.png")?.cgImage
        return image
    }()
    
    
    // MARK: - View Life Cycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
}

// MARK: - Custom Method
extension LCWaveViewViewController {
    
    // Configure the UI interface
    private func configUI() {
        configTableView()
        configWaveView()
    }
    
    // Configure the wave view
    private func configWaveView() {
        waveView.completion = { centerY in  // Wave animation callback
            // Synchronize the y coordinate of the avatar view
            self.iconImageView.frame.origin.y = waveViewHeight + centerY - iconImageWidth
        }
        waveView.addSubview(iconImageView)
        waveView.startWave()
    }
    
    // Configuration form
    private func configTableView() {
        table.backgroundColor = .red
        table.dataSource = self
        table.register(UITableViewCell.self, forCellReuseIdentifier: identifier)
        table.contentInset = UIEdgeInsets(top: 100, left: 0, bottom: 0, right: 0)
        table.tableHeaderView = waveView
    }
}

// MARK: - UITableViewDataSource
extension LCWaveViewViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        cell.textLabel?.text = "\(indexPath.row)"
        return cell
    }
}

