//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

// Wave curve animation view
// Wave curve formula: y = h * sin(a * x + b);
// h: wave height, a: wave width factor, b: wave offset

// Wave view
public class LCWaveView: UIView {

    // MARK: - public property
    
    /// Wave width factor -> a
    public var waveRate: CGFloat = 1.5
    
    /// Wave speed (default: 0.5 value 0 to 1)
    public var waveSpeed: CGFloat = 0.5
    
    /// The height of the wave -> h (default: 5)
    public var waveHeight: CGFloat = 5
    
    /// True wave layer color
    public var realWaveColor: UIColor = UIColor.white {
        didSet {
            realWaveLayer.fillColor = realWaveColor.cgColor
        }
    }
    
    /// Mask wave layer color
    public var maskWaveColor: UIColor = UIColor.white {
        didSet {
            maskWaveLayer.fillColor = maskWaveColor.cgColor
        }
    }
    
    /// Fluctuation completion callback
    public var completion: ((_ centerY: CGFloat)->())?
    
    
    // MARK: - private property
    
    /// true wave layer
    private lazy var realWaveLayer: CAShapeLayer = CAShapeLayer()
   
    /// Mask layer
    private lazy var maskWaveLayer: CAShapeLayer = CAShapeLayer()
    
    /// Screen refresh rate timer
    private var waveDisplayLink: CADisplayLink?
    
    /// Wave offset -> b
    private var offset: CGFloat = 0
    
    /// frequency
    private var priFrequency: CGFloat = 0

    /// speed
    private var priWaveSpeed: CGFloat = 0

    /// height
    private var priWaveHeight: CGFloat = 0
    
    /// Define the state of the variable record fluctuation view - start (default: false)
    private var isStarting: Bool = false

    /// Define the state of the variable record fluctuation view - stop (default: false)
    private var isStopping: Bool = false
    
   
    // MARK: - Initialize
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        var tempf = bounds
        tempf.origin.y = frame.size.height
        tempf.size.height = 0
        
        maskWaveLayer.frame = tempf
        realWaveLayer.frame = tempf
        
        backgroundColor = .clear
        layer.addSublayer(realWaveLayer)
        layer.addSublayer(maskWaveLayer)
    }
    
    // MARK: - Convenience constructor
    
    /// Initialize the size position of the wave view, as well as the color
    /// - Parameters:
    /// - frame: size position
    /// - color: color
    public convenience init(frame: CGRect, color: UIColor) {
        self.init(frame: frame)
        
        realWaveColor = color
        maskWaveColor = color.withAlphaComponent(0.7)
        realWaveLayer.fillColor = realWaveColor.cgColor     // The fill color of the layer
        maskWaveLayer.fillColor = maskWaveColor.cgColor
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func updateColor(color: UIColor) {
        realWaveColor = color
        maskWaveColor = color.withAlphaComponent(0.7)
        realWaveLayer.fillColor = realWaveColor.cgColor     // The fill color of the layer
        maskWaveLayer.fillColor = maskWaveColor.cgColor
    }
}

// MARK: - Method
extension LCWaveView {
    
    /// Start to fluctuate
    public func startWave() {
        
        if !isStarting {
            
            removeTimer()   // First remove the screen refresh rate timer
            isStarting = true
            isStopping = false
            
            /* CADisplayLink: A timer with the same screen refresh rate needs to be registered to the runloop in a specific mode. Each time the screen is refreshed, the selector method on the bound target is called.
                                 Duration: time between frames
                                 Pause: pause, set true to pause, false to continue
                                 At the end, you need to call the invalidate method, and remove the previously bound target and selector from the runloop.
                                 Cannot be inherited
            */
            // turn on the timer
            waveDisplayLink = CADisplayLink(target: self, selector: #selector(waveEvent))
            waveDisplayLink?.add(to: .current, forMode: RunLoop.Mode.commonModes)
        }
    }
    
    /// Stop the wave
    public func stopWave() {
        if !isStopping {
            isStarting = false
            isStopping = true
        }
    }
    
    /// Remove the timer
    private func removeTimer() {
        // Remove the timer from the run loop
        waveDisplayLink?.invalidate()
        waveDisplayLink = nil
    }
    
    /// Floating event
    @objc func waveEvent() {
        if isStarting {
            BeganToWave()
        }
        if isStopping {
            endToWave()
        }
        other()
    }
    
}


// MARK: - Three states of float (start, end, other)
extension LCWaveView {
    
    /// Start to fluctuate
    private func BeganToWave() {
        guard priWaveHeight < waveHeight else {
            isStarting = false
            return
        }
        priWaveHeight = priWaveHeight + waveHeight / 100
        
        // 1. Use a temporary variable to save the size of the current view
        var f = self.bounds
        
        // 2. Assign a value to this variable
        f.origin.y = f.size.height - priWaveHeight
        f.size.height = priWaveHeight
        
        // 3. Modify the value of the frame
        maskWaveLayer.frame = f
        realWaveLayer.frame = f
        priFrequency = priFrequency + waveRate / 100
        priWaveSpeed = priWaveSpeed + waveSpeed / 100
    }
    
    /// End the swing
    private func endToWave() {
        guard priWaveHeight > 0 else {  // 停止
            isStopping = false
            stopWave()
            return
        }
        priWaveHeight = priWaveHeight - waveHeight / 50.0
        
        // 1. Use a temporary variable to save the size of the current view
        var f = self.bounds
        
        // 2. Assign a value to this variable
        f.origin.y = f.size.height
        f.size.height = priWaveHeight
        
        // 3. Modify the value of the frame
        maskWaveLayer.frame = f
        realWaveLayer.frame = f
        priFrequency = priFrequency - waveRate / 50.0
        priWaveSpeed = priWaveSpeed - waveSpeed / 50.0
    }
    
    /// Other cases
    private func other() {
        
        // The key to wave movement: offset by the specified speed
        offset += priWaveSpeed
        
        var y: CGFloat = 0.0
        let width: CGFloat = frame.width
        let height: CGFloat = priWaveHeight
        
        // Create a variable graphics path 1, 2
        let realPath = CGMutablePath()
        let maskPath = CGMutablePath()
        
        // Start specifying a new subpath.
        realPath.move(to: CGPoint(x: 0, y: height))
        maskPath.move(to: CGPoint(x: 0, y: height))
        
        let offset_f = Float(offset * 0.045)
        
        let waveFrequency_f = Float(0.01 * priFrequency)
        
        for x in 0...Int(width) {
            
            // wave curve
            y = height * CGFloat(sin(waveFrequency_f * Float(x) + offset_f))
            
            // draw these points in the first form
            realPath.addLine(to: CGPoint(x: CGFloat(x), y: y))
            maskPath.addLine(to: CGPoint(x: CGFloat(x), y: -y))
        }
        
        let midX = bounds.size.width * 0.5
        let midY = height * sin(midX * CGFloat(waveFrequency_f) + CGFloat(offset_f))
        
        if let callback = completion {
            callback(midY)
        }
        
        // 1. Draw the path in the form of a line from the current point to the specified point
        realPath.addLine(to: CGPoint(x: width, y: height))
        realPath.addLine(to: CGPoint(x: 0, y: height))
        maskPath.addLine(to: CGPoint(x: width, y: height))
        maskPath.addLine(to: CGPoint(x: 0, y: height))
        // 2. Close the path
        maskPath.closeSubpath()
        realPath.closeSubpath()
        // 3. Assignment path
        realWaveLayer.path = realPath
        maskWaveLayer.path = maskPath
    }
}

