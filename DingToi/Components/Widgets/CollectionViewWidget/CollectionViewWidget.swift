//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

extension UICollectionView {
    func deselectAllItems(animated: Bool) {
        guard let selectedItems = indexPathsForSelectedItems else { return }
        for indexPath in selectedItems { deselectItem(at: indexPath, animated: animated) }
    }
}

protocol CollectionViewDelegateWidget {
    func numberOfSections(in collectionView: UICollectionView) -> Int? //require
    func customViewLayout(_ colletionViewLayout: BaseCollectionViewLayout, insetForSection section: Int) -> UIEdgeInsets?
    
    func customViewLayout(_ collectionViewLayout: BaseCollectionViewLayout, sizeForHeaderSection section: Int) -> CGSize?
    func customViewLayout(_ collectionView: UICollectionView, nibForHeaderInSection section: Int) -> String?
    func customViewLayout(_ collectionView: UICollectionView,_ header: UICollectionReusableView?, viewForHeaderInSection section: Int) -> UICollectionReusableView?
    
    func customViewLayout(_ collectionViewLayout: BaseCollectionViewLayout, sizeForFooterSection section: Int) -> CGSize?
    func customViewLayout(_ collectionView: UICollectionView, nibForFooterInSection section: Int) -> String?
    func customViewLayout(_ collectionView: UICollectionView,_ footer: UICollectionReusableView?, viewForFooterInSection section: Int) -> UICollectionReusableView?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int?
    func customViewLayout(_ collecitionViewLayout: BaseCollectionViewLayout, numberColsInSection section: Int) -> Int?
    func customViewLayout(_ collecitionViewLayout: BaseCollectionViewLayout, sizeForItem indexPath: IndexPath) -> CGSize?
    func collectionView(_ collectionView: UICollectionView, nibForRowAt indexPath: IndexPath) -> String? //require
    func collectionView(_ collectionView: UICollectionView,_ item: UICollectionViewCell, cellForRowAt indexPath: IndexPath) -> UICollectionViewCell? //require
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) //require
}

extension CollectionViewDelegateWidget {
    func customViewLayout(_ colletionViewLayout: BaseCollectionViewLayout, insetForSection section: Int) -> UIEdgeInsets? {return nil}
    
    func customViewLayout(_ collectionViewLayout: BaseCollectionViewLayout, sizeForHeaderSection section: Int) -> CGSize? {return nil}
    func customViewLayout(_ collectionView: UICollectionView, nibForHeaderInSection section: Int) -> String? {return nil}
    func customViewLayout(_ collectionView: UICollectionView,_ header: UICollectionReusableView?, viewForHeaderInSection section: Int) -> UICollectionReusableView? {return nil}
    
    func customViewLayout(_ collectionViewLayout: BaseCollectionViewLayout, sizeForFooterSection section: Int) -> CGSize? {return nil}
    func customViewLayout(_ collectionView: UICollectionView, nibForFooterInSection section: Int) -> String? {return nil}
    func customViewLayout(_ collectionView: UICollectionView,_ footer: UICollectionReusableView?, viewForFooterInSection section: Int) -> UICollectionReusableView? {return nil}
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int? {return nil}
    func customViewLayout(_ collecitionViewLayout: BaseCollectionViewLayout, numberColsInSection section: Int) -> Int? {return nil}
    func customViewLayout(_ collecitionViewLayout: BaseCollectionViewLayout, sizeForItem indexPath: IndexPath) -> CGSize? {return nil}
}

class CollectionViewWidget: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, BaseCollectionViewLayoutDelegate {
    private var collectionViewDelegateWidget: CollectionViewDelegateWidget?
    private var listData: [Any?]?
    private var sizeCell: CGSize = CGSize(width: 120.0, height: 120.0)
    private var numberColsInSection: Int = 2
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //let collectionView = CollectionViewWidget.init(self.frameDefaultIphone!, [], [], [], ["A","B","C","A","B","C","A","B","C"], CGSize(width: 120.0, height: 120.0), 5, nil, nil)
    init(_ frame: CGRect,_ listNibCell: [String],_ listNibHeader: [String], _ listNibFooter: [String],_ listData: [Any?]?,_ sizeCell: CGSize?,_ numberColsInSection: Int?,_ layout: UICollectionViewLayout?,_  collectionViewDelegateWidget: CollectionViewDelegateWidget?) {
        if layout == nil {
            let viewLayout = BaseCollectionViewLayout()
            viewLayout.scrollDirection = .vertical
            super.init(frame: frame, collectionViewLayout: viewLayout)
            viewLayout.delegate = self
        }else {
            super.init(frame: frame, collectionViewLayout: layout!)
        }
        
        for nib in listNibCell {
            self.register(UINib(nibName: nib.firstUppercased, bundle: nil), forCellWithReuseIdentifier: nib)
        }
        self.register(UINib(nibName: "CellWidget", bundle: nil), forCellWithReuseIdentifier: "cellWidget")
        
        for nib in listNibHeader {
            self.register(UINib(nibName: nib.firstUppercased, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: nib)
        }
        self.register(UINib(nibName: "HeaderWidget", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerWidget")
        
        for nib in listNibFooter {
            self.register(UINib(nibName: nib.firstUppercased, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: nib)
        }
        self.register(UINib(nibName: "FooterWidget", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerWidget")
        
        if let list = listData{
            self.listData = list
        }
        if let sizeCell = sizeCell{
            self.sizeCell = sizeCell
        }
        if let numberColsInSection = numberColsInSection{
            self.numberColsInSection = numberColsInSection
        }
        if let delegate = collectionViewDelegateWidget {
            self.collectionViewDelegateWidget = delegate
        }
        self.delegate = self
        self.dataSource = self
        self.layoutIfNeeded()
        self.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func initRegistry(_ listNibCell: [String],_ listNibHeader: [String], _ listNibFooter: [String],_ listData: [Any?]?,_ sizeCell: CGSize?,_ numberColsInSection: Int?,_ layout: UICollectionViewLayout?,_  collectionViewDelegateWidget: CollectionViewDelegateWidget?) {
        if layout == nil {
            let viewLayout = BaseCollectionViewLayout()
            viewLayout.scrollDirection = .vertical
            viewLayout.delegate = self
        }else{
            self.collectionViewLayout = layout!
        }
        
        for nib in listNibCell {
            self.register(UINib(nibName: nib.firstUppercased, bundle: nil), forCellWithReuseIdentifier: nib)
        }
        self.register(UINib(nibName: "CellWidget", bundle: nil), forCellWithReuseIdentifier: "cellWidget")
        
        for nib in listNibHeader {
            self.register(UINib(nibName: nib.firstUppercased, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: nib)
        }
        self.register(UINib(nibName: "HeaderWidget", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerWidget")
        
        for nib in listNibFooter {
            self.register(UINib(nibName: nib.firstUppercased, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: nib)
        }
        self.register(UINib(nibName: "FooterWidget", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerWidget")
        
        if let list = listData{
            self.listData = list
        }
        if let sizeCell = sizeCell{
            self.sizeCell = sizeCell
        }
        if let numberColsInSection = numberColsInSection{
            self.numberColsInSection = numberColsInSection
        }
        if let delegate = collectionViewDelegateWidget {
            self.collectionViewDelegateWidget = delegate
        }
        self.delegate = self
        self.dataSource = self
        self.layoutIfNeeded()
        self.setContentOffset(CGPoint.zero, animated: true)
    }
    
    //Section
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.collectionViewDelegateWidget?.numberOfSections(in: collectionView) ?? (self.listData?.count) == nil ? 0:1
    }
    
    func customViewLayout(colletionViewLayout: BaseCollectionViewLayout, insetForSection section: Int) -> UIEdgeInsets {
        if let inset = self.collectionViewDelegateWidget?.customViewLayout(colletionViewLayout, insetForSection: section) {
            return inset
        }else if colletionViewLayout.scrollDirection == .horizontal {
            if UIApplication.shared.statusBarOrientation == .landscapeLeft {//Turn right device
                return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
            }else {
                return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            }
        }else {
            return UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        }
    }
    //Section ./
    
    //Header + Footer
    func customViewLayout(collectionViewLayout: BaseCollectionViewLayout, sizeForHeaderSection section: Int) -> CGSize {
        return self.collectionViewDelegateWidget?.customViewLayout(collectionViewLayout, sizeForHeaderSection: section) ?? CGSize(width: self.frame.size.width, height: 0.0)
    }
    
    func customViewLayout(collectionViewLayout: BaseCollectionViewLayout, sizeForFooterSection section: Int) -> CGSize {
        return self.collectionViewDelegateWidget?.customViewLayout(collectionViewLayout, sizeForFooterSection: section) ?? CGSize(width: self.frame.size.width, height: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            case UICollectionElementKindSectionHeader:
                if let nibHeader = self.collectionViewDelegateWidget?.customViewLayout(collectionView, nibForHeaderInSection: indexPath.section) {
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: nibHeader, for: indexPath)
                    headerView.layoutIfNeeded()
                    if let headerView =  self.collectionViewDelegateWidget?.customViewLayout(collectionView, headerView, viewForHeaderInSection: indexPath.section) {
                        return headerView
                    }
                }
                
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerWidget", for: indexPath) as! HeaderWidget
                return headerView
            case UICollectionElementKindSectionFooter:
                if let nibFooter = self.collectionViewDelegateWidget?.customViewLayout(collectionView, nibForFooterInSection: indexPath.section) {
                    let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: nibFooter, for: indexPath)
                    footerView.layoutIfNeeded()
                    if let footerView =  self.collectionViewDelegateWidget?.customViewLayout(collectionView, footerView, viewForFooterInSection: indexPath.section) {
                        return footerView
                    }
                }
                
                let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footerWidget", for: indexPath) as! FooterWidget
                return footerView
            default:
                return UICollectionReusableView()
        }
    }
    //Header + Footer ./
    
    //Cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let size = self.collectionViewDelegateWidget?.collectionView(collectionView, numberOfItemsInSection: section) {
            return size
        }else if let size = self.listData?.count {
            return size
        }
        return 0
    }

    func customViewLayout(collecitionViewLayout: BaseCollectionViewLayout, numberColsInSection section: Int) -> Int {
        if let cols = self.collectionViewDelegateWidget?.customViewLayout(collecitionViewLayout, numberColsInSection: section) {
            return cols
        }
        return self.numberColsInSection
    }

    func customViewLayout(collecitionViewLayout: BaseCollectionViewLayout, sizeForItem indexPath: IndexPath) -> CGSize {
        if let sizeCell = self.collectionViewDelegateWidget?.customViewLayout(collecitionViewLayout, sizeForItem: indexPath) {
            return sizeCell
        }
        return self.sizeCell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let nibCell = self.collectionViewDelegateWidget?.collectionView(collectionView, nibForRowAt: indexPath) {
            let cellView = collectionView.dequeueReusableCell(withReuseIdentifier: nibCell, for: indexPath)//UICollectionViewCell
            if let cellView = self.collectionViewDelegateWidget?.collectionView(collectionView, cellView, cellForRowAt: indexPath) {
                cellView.layoutIfNeeded()
                return cellView
            }
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellWidget", for: indexPath) as! CellWidget
        return cell
    }
    //Cell ./
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("CollectionView Section: \(indexPath.section) - RowCell: \(indexPath.row)")
        self.collectionViewDelegateWidget?.collectionView(collectionView, didSelectItemAt: indexPath)
    }
}
