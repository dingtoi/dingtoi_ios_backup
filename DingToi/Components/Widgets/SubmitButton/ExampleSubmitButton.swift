//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class ExampleSubmitButton: UIViewController {
    
    @IBOutlet weak var _btnSubmitButton: SubmitButton!
    @IBAction func _btnSubmitButtonClick(_ sender: SubmitButton) {
        _btnSubmitButton.changeState(toState: SubmitButtonState.Loading)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        //SubmitButton
        _btnSubmitButton.layer.cornerRadius = _btnSubmitButton.frame.height/2
        _btnSubmitButton.checkLineWidth = 4.0
        _btnSubmitButton.progressBorderWidth = 2.0
        _btnSubmitButton.layer.borderWidth = 2.0
        _btnSubmitButton.layer.borderColor = UIColor.blue.cgColor
        _btnSubmitButton.backgroundColor = UIColor.white
        
        _btnSubmitButton.progressColor = UIColor.blue
        _btnSubmitButton.setTitleColor(UIColor.black, for: .normal)
        _btnSubmitButton.stateChanged = { (toState:SubmitButtonState) in
            if toState == SubmitButtonState.Loading {
                self._btnSubmitButton.progress  = 1.0
            }else if toState == SubmitButtonState.Finished {
                //DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                //    self._btnSubmitButton.changeState(toState: SubmitButtonState.Normal)
                //})
            }else if toState == SubmitButtonState.Error {
                //self._btnSubmitButton.changeState(toState: SubmitButtonState.Normal)
            }
        }
        //SubmitButton ./
        
    }
    
    func OnSuccess() {
        //Success
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            self._btnSubmitButton.changeState(toState: SubmitButtonState.Normal)
        })
    }
    
    func OnError() {
        //Error
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            //self._btnSubmitButton.changeState(toState: SubmitButtonState.Error)
            self._btnSubmitButton.changeState(toState: SubmitButtonState.Normal)
        })
    }
}

