//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class SliderWidget: UISlider {
    let nameIcon = "icontemp1.png"
    let sizeIcon = CGSize.init(width: 25.0, height: 25.0)
    let heightLine: CGFloat = 5.0
    let widthViewLabel: CGFloat = 100.0
    let heightViewLabel: CGFloat = 30.0
    let nameIconLabel = "" //place.png
    var _viewLabel: UIImageView? = nil
    var _label: UILabel? = nil
    var _unit:Int = 1
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        if self._label == nil || self._viewLabel == nil {
            self.addViewLabel()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func addViewLabel() {
        let img = UIImage.init(named: self.nameIcon)?.resizeImage(targetSize: self.sizeIcon)
        if let thumbImage = img {
            self.setThumbImage(thumbImage, for: .normal)
        }
        self.addTarget(self, action: #selector(self.updateKmsLabel(sender:)), for: .allEvents)
        
        self._viewLabel = UIImageView.init(frame: CGRect.init(x: 0.0, y: -(self.frame.height/2 + self.heightLine/2 + self.sizeIcon.height/2), width: self.widthViewLabel, height: self.heightViewLabel))
        self._viewLabel?.backgroundColor = .clear
        self._viewLabel?.image = UIImage.init(named: self.nameIconLabel)
        self._viewLabel?.contentMode = UIViewContentMode.scaleAspectFit
        
        self._label = UILabel.init(frame: CGRect.init(x: 0.0, y: 0.0, width: (self._viewLabel?.frame.width)!, height: (self._viewLabel?.frame.height)!))
        self._label?.baselineAdjustment = .alignCenters
        self._label?.textAlignment = NSTextAlignment.center
        self._label?.adjustsFontSizeToFitWidth = true
        self._label?.minimumScaleFactor = 0.2
        self._label?.numberOfLines = 1
        self._viewLabel?.addSubview(self._label!)
        
        addSubview(self._viewLabel!)
    }
    
    public override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let customBounds = CGRect(origin: bounds.origin, size: CGSize.init(width: bounds.size.width, height: self.heightLine))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
    
    public override func awakeFromNib() {
        self.addViewLabel()
        super.awakeFromNib()
    }
    
    @objc func updateKmsLabel(sender: UISlider!) {
        let value = Int(sender.value)
        DispatchQueue.main.async {
            let _thumbRect: CGRect = sender.thumbRect(forBounds: sender.bounds, trackRect: sender.trackRect(forBounds: sender.bounds), value: sender.value)
            let thumbRect: CGRect = self.convert(_thumbRect, from: sender)
            //print("Slider value = \(value) - ThumbRect: \(thumbRect)")
            self._label?.text = self.convertNumber(value: Int64(value*self._unit))
            if let _view = self._viewLabel {
                if (thumbRect.origin.x - (self.widthViewLabel - self.sizeIcon.width)/2) >= 0 {
                    self._label?.textAlignment = NSTextAlignment.center
                    _view.frame = CGRect.init(x: thumbRect.origin.x - (self.widthViewLabel - self.sizeIcon.width)/2, y: _view.frame.origin.y, width: _view.frame.width, height: _view.frame.height)
                }else{
                    self._label?.textAlignment = NSTextAlignment.left
                    _view.frame = CGRect.init(x: 0, y: _view.frame.origin.y, width: _view.frame.width, height: _view.frame.height)
                }
            }
        }
    }
    
    public override func setValue(_ value: Float, animated: Bool) {
        super.setValue(value, animated: animated)
        DispatchQueue.main.async {
            let _thumbRect: CGRect = self.thumbRect(forBounds: self.bounds, trackRect: self.trackRect(forBounds: self.bounds), value: self.value)
            let thumbRect: CGRect = self.convert(_thumbRect, from: self)
            self._label?.text = self.convertNumber(value: Int64(value)*Int64(self._unit))
            if let _view = self._viewLabel {
                if (thumbRect.origin.x - (self.widthViewLabel - self.sizeIcon.width)/2) >= 0 {
                    self._label?.textAlignment = NSTextAlignment.center
                    _view.frame = CGRect.init(x: thumbRect.origin.x - (self.widthViewLabel - self.sizeIcon.width)/2, y: _view.frame.origin.y, width: _view.frame.width, height: _view.frame.height)
                }else{
                    self._label?.textAlignment = NSTextAlignment.left
                    _view.frame = CGRect.init(x: 0, y: _view.frame.origin.y, width: _view.frame.width, height: _view.frame.height)
                }
            }
        }
    }
    
    public func convertNumber(value: Int64) -> String?{
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.groupingSeparator = ","
        if let formattedNumber: String =  numberFormatter.string(from: NSNumber(value:value)){
            return formattedNumber
        }else{
            return nil
        }
    }
}
