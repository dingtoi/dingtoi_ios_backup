//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class DatePickerWidget: UIDatePicker {
    public static let TAGDATEPICKER:Int = 1111122222
    typealias Action = (String) -> (Void)
    var completion: Action?
    var toolBar = UIToolbar()
    
    private var _heightViewHeader: CGFloat = 40
    var heightViewHeader: CGFloat {
        get{
            return self._heightViewHeader
        }
        set(newValue){
            self._heightViewHeader = newValue
        }
    }
    
    public init(_ valueSelect: String, _ completion: (@escaping (String) -> (Void)) ) {
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        super.init(frame:CGRect.init(x: 0, y: (height - (height/3)), width: width, height: (height/3)))
        self.completion = completion
        
        // ToolBar
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.rgb(fromHexString: "#0054FF")
        toolBar.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
        let positionY = (height - (height/3)) - self._heightViewHeader
        toolBar.frame = CGRect.init(x: 0, y: positionY, width: width, height: self._heightViewHeader)
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem.init(title: String.kDONE, style: .plain, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem.init(title: String.kCANCEL, style: .plain, target: self, action: #selector(self.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        self.backgroundColor = UIColor.white
        if Language.getCurrentLanguageCode() == LangVI {
            self.locale = Locale(identifier: "vi-VN")
        }else {
            self.locale = Locale(identifier: "en_US")
        }
        
        self.setValue(UIColor.black, forKey:"textColor")
        
        self.datePickerMode = .date
        //let formatter = DateFormatter()
        //formatter.dateFormat = "yyyy/MM/dd"
        //let minYear = Date().getYear().toInt()! - 65
        //self.minimumDate = formatter.date(from: "\(minYear)/01/01")
        //let maxYear = Date().getYear().toInt()! - 21
        //self.maximumDate = formatter.date(from: "\(maxYear)/01/01")
        //self.maximumDate = Calendar.current.date(byAdding: .day, value: 0, to: Date())
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd-MM-yyyy"
        let dateSelect = formatter2.date(from: valueSelect)
        if let unwrappedDate = dateSelect {
            self.setDate(unwrappedDate, animated: false)
        }else{
            #if DEBUG
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                selectDate(formatter.date(from: "01/01/1955") ?? Date())
            #else
                //selectDate(Date())
                selectDate(self.minimumDate ?? Date())
            #endif
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func donePicker(sender: UIButton) {
        completion!(self.getDate() ?? "")
        self.hide()
    }
    
    @objc func cancelPicker(sender: UIButton) {
        self.hide()
    }
    
    public func hide() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromBottom
        
        if appDelegate.window!.viewWithTag(DatePickerWidget.TAGDATEPICKER + 1) != nil {
            appDelegate.window!.viewWithTag(DatePickerWidget.TAGDATEPICKER + 1)?.removeFromSuperview()
        }
        if appDelegate.window!.viewWithTag(DatePickerWidget.TAGDATEPICKER) != nil {
            let view = appDelegate.window!.viewWithTag(DatePickerWidget.TAGDATEPICKER)
            view?.layer.add(transition, forKey: nil)
            view?.removeFromSuperview()
        }
    }
    
    public func show() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromTop
        
        let viewOutSide = UIView.init(frame: UIScreen.main.bounds)
        viewOutSide.backgroundColor = UIColor.init(red: 0.41, green: 0.41, blue: 0.41, alpha: 0.5)
        viewOutSide.tag = DatePickerWidget.TAGDATEPICKER + 1
        appDelegate.window?.addSubview(viewOutSide)
        
        
        let view:UIView = UIView.init(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.clear
        view.addSubview(self)
        view.addSubview(self.toolBar)
        view.tag = DatePickerWidget.TAGDATEPICKER
        view.layer.add(transition, forKey: nil)
        
        let btnOutSide = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: view.frame.height - self._heightViewHeader - self.frame.height))
        btnOutSide.backgroundColor = UIColor.clear
        let tapOutSide = UITapGestureRecognizer(target: self, action: #selector(viewOutSideClick))
        btnOutSide.addGestureRecognizer(tapOutSide)
        btnOutSide.isUserInteractionEnabled = true
        view.addSubview(btnOutSide)
        
        appDelegate.window!.endEditing(true)
        appDelegate.window!.addSubview(view)
    }
    
    @objc func viewOutSideClick(sender: UITapGestureRecognizer) {
        completion!(self.getDate() ?? "")
        self.hide()
    }
    
    public func getDate() -> String? {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "dd-MM-yyyy"
        return timeFormatter.string(from: self.date)
    }
    
    public func getTime() -> String? {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm:ss"
        return timeFormatter.string(from: self.date)
    }
    
    public func selectDate(_ date: Date) {
        if let unwrappedDate = Calendar.current.date(byAdding: .day, value: 0, to: date) {
            self.setDate(unwrappedDate, animated: false)  //set your own date
        }
    }
}
