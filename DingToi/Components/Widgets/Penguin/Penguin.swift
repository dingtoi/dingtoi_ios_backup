//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import DeviceKit

public class Penguin: EasyTipViewDelegate {
    public static let TAG_PENGUIN = 233
    public var _window: UIWindow!
    public var sizeIconQuestion = CGSize(width: UIDevice().autoSize(15, 18, 22, 22), height: UIDevice().autoSize(15, 18, 22, 22))
    public var sizeIconPenguin = CGSize(width: UIDevice().autoSize(20, 28, 32, 32) * 2.0, height: UIDevice().autoSize(20, 28, 32, 32) * 2.0)
    public var _nameImagePenguin = "icon_penguin.png"
    public var _nameImageQuestion = "icon_question.png"
    public var _showQuestion = true
    public var preferences = EasyTipView.Preferences()
    public var _easyTipView: EasyTipView!
    public var _message: String = ""
    
    public init(window: UIWindow) {
        self._window = window
        preferences.drawing.font = UIFont(name: "Arial", size: 12)!
        preferences.drawing.foregroundColor = UIColor.rgb(fromHexString: "#2C6196")
        preferences.drawing.backgroundColor = UIColor.white
        preferences.drawing.textAlignment = NSTextAlignment.center
        preferences.drawing.arrowPosition = .bottom
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 15, y: 15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 15, y: 15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1
        preferences.animating.dismissDuration = 0.5
        preferences.animating.dismissOnTap = true
        preferences.positioning.maxWidth = 200
        preferences.positioning.bubbleHInset = 10
        preferences.positioning.bubbleVInset = 1
        EasyTipView.globalPreferences = preferences
    }
    
    public func show(_ addHeight: CGFloat = 0.0) {
        self.hide()
        let x1 = self._window.frame.width - sizeIconPenguin.width - sizeIconPenguin.width/4
        let y1 = self._window.frame.height - sizeIconPenguin.height - sizeIconPenguin.height - addHeight
        let cGRectPenguin = CGRect.init(x: x1, y: y1, width: sizeIconPenguin.width, height: sizeIconPenguin.height)
        
        let x2 = x1 + (sizeIconPenguin.width - sizeIconQuestion.width)
        let y2 = y1 - sizeIconQuestion.width/4
        let cGRectQuesttion = CGRect.init(x: x2, y: y2, width: sizeIconQuestion.width, height: sizeIconQuestion.height)
        
        if let _imgPenguin = UIImage.init(named: self._nameImagePenguin),
           let _imgQuesttion = UIImage.init(named: self._nameImageQuestion) {
            
            let imgViewPenguin = UIImageView.init(frame: cGRectPenguin)
            imgViewPenguin.image = _imgPenguin
            imgViewPenguin.tag = Penguin.TAG_PENGUIN
            imgViewPenguin.addTapGesture(self, #selector(_tapPenguin))
            self._window.addSubview(imgViewPenguin)
            
            let imgViewQuesttion = UIImageView.init(frame: cGRectQuesttion)
            imgViewQuesttion.image = _imgQuesttion
            imgViewQuesttion.tag = Penguin.TAG_PENGUIN + 1
            self._window.addSubview(imgViewQuesttion)
        }
    }
    
    public func hide() {
        self._window.viewWithTag(Penguin.TAG_PENGUIN)?.removeFromSuperview()
        self._window.viewWithTag(Penguin.TAG_PENGUIN + 1)?.removeFromSuperview()
        self._window.viewWithTag(Penguin.TAG_PENGUIN + 2)?.removeFromSuperview()
    }
    
    public func updateContent(message: String) {
        self._message = message
        self._showQuestion = true
        self._window.viewWithTag(Penguin.TAG_PENGUIN + 1)?.isHidden = false
    }
    
    @objc func _tapPenguin(sender: UITapGestureRecognizer) {
        if self._showQuestion {
            self._showQuestion = false
            self._window.viewWithTag(Penguin.TAG_PENGUIN + 1)?.isHidden = true
        }
        
        if let buttonF = self._window.viewWithTag(Penguin.TAG_PENGUIN) {
            if self._window.viewWithTag(Penguin.TAG_PENGUIN + 2) != nil {
                self._easyTipView.dismiss()
            }else {
                self._easyTipView = EasyTipView.init(text: self._message, preferences: self.preferences, delegate: self)
                self._easyTipView.tag = Penguin.TAG_PENGUIN + 2
                self._easyTipView.show(animated: true, forView: buttonF, withinSuperview: self._window)
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                    self._easyTipView.dismiss()
                })
            }
        }
    }
    
    public func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        print("===> \(tipView) did dismiss!")
    }
    
    public func hideText() {
        self._easyTipView.dismiss()
    }
    
    public func moveTop() {
        let keyboardHeight = KeyboardService.keyboardHeight()
        print("===>KeyboardService moveTop: \(keyboardHeight)")
        if let imgViewPenguin = self._window.viewWithTag(Penguin.TAG_PENGUIN) {
            if (UIScreen.main.bounds.height - imgViewPenguin.frame.origin.y) < keyboardHeight {
                imgViewPenguin.frame = CGRect.init(x: imgViewPenguin.frame.origin.x, y: imgViewPenguin.frame.origin.y - keyboardHeight, width: imgViewPenguin.frame.width, height: imgViewPenguin.frame.height)
            }
        }
        
        if let imgViewQuesttion = self._window.viewWithTag(Penguin.TAG_PENGUIN + 1) {
            if (UIScreen.main.bounds.height - imgViewQuesttion.frame.origin.y) < keyboardHeight {
                imgViewQuesttion.frame = CGRect.init(x: imgViewQuesttion.frame.origin.x, y: imgViewQuesttion.frame.origin.y - keyboardHeight, width: imgViewQuesttion.frame.width, height: imgViewQuesttion.frame.height)
            }
        }
    }
    
    public func moveBottom() {
        let keyboardHeight = KeyboardService.keyboardHeight()
        print("===>KeyboardService moveBottom: \(keyboardHeight)")
        if let imgViewPenguin = self._window.viewWithTag(Penguin.TAG_PENGUIN) {
            if (UIScreen.main.bounds.height - imgViewPenguin.frame.origin.y) > keyboardHeight {
                imgViewPenguin.frame = CGRect.init(x: imgViewPenguin.frame.origin.x, y: imgViewPenguin.frame.origin.y + keyboardHeight, width: imgViewPenguin.frame.width, height: imgViewPenguin.frame.height)
            }
        }
        
        if let imgViewQuesttion = self._window.viewWithTag(Penguin.TAG_PENGUIN + 1) {
            if (UIScreen.main.bounds.height - imgViewQuesttion.frame.origin.y) > keyboardHeight {
                imgViewQuesttion.frame = CGRect.init(x: imgViewQuesttion.frame.origin.x, y: imgViewQuesttion.frame.origin.y + keyboardHeight, width: imgViewQuesttion.frame.width, height: imgViewQuesttion.frame.height)
            }
        }
    }
    
}
