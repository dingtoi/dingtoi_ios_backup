//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

public class Scheduler : AlarmSchedulerDelegate {
    public static let NotificationIdentifier = UUID().uuidString
    let categoryNotificationEventAction = "alarm.category"
    let checkIOS10 = false
    public var applicationStateString: String {
        if UIApplication.shared.applicationState == .active {
            return "active"
        } else if UIApplication.shared.applicationState == .background {
            return "background"
        }else {
            return "inactive"
        }
    }
    public var alarmModel: Alarms = Alarms()//lấy alarmModel.alarms: [Alarm] từ UserDefaults
    
    
    public func reSchedule() {
        //cancel all and register all is often more convenient
        removeAllAlarmTime()
        syncAlarmModel()
        for i in 0..<alarmModel.count{
            let alarm = alarmModel.alarms[i]
            if alarm.enabled && alarm.date > Date() {
                setNotificationWithDate(alarm.idAlarm, alarm.date as Date, alarm.label, alarm.location, alarm.description, onWeekdaysForNotify: alarm.repeatWeekdays, snoozeEnabled: alarm.snoozeEnabled, onSnooze: false, soundName: alarm.mediaLabel, index: i)
            }
        }
    }
    
    public func setNotificationWithDate(_ idAlarm: String,_ date: Date,_ label: String,_ location: String,_ description: String, onWeekdaysForNotify weekdays:[Int], snoozeEnabled:Bool,  onSnooze: Bool, soundName: String, index: Int) {
        //var dateString = date+" "+time
        //print("Set Alarm 1: \(dateString)")
        //let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        //let convertedDate = dateFormatter.date(from: dateString)!
        //let subtractTime = Calendar.current.date(byAdding: .minute, value: 1, to: convertedDate) // +/- phút
        //dateString = dateFormatter.string(from: subtractTime!)
        //print("Set Alarm 2: \(dateString)")
        //dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        //let dateAlarm:Date = dateFormatter.date(from: dateString)!
        //print("Alaram time : \(dateAlarm.toStringFormat(dateFormat: "dd-MM-yyyy HH:mm:ss")))")
        
        if #available(iOS 10.0, *), self.checkIOS10 {
            let content = UNMutableNotificationContent()
            content.title = "New Event"
            content.body = label
            content.subtitle = description
            content.categoryIdentifier = self.categoryNotificationEventAction //width button --> "Dismiss"
            content.sound = UNNotificationSound(named: "\(soundName).mp3")//UNNotificationSound.init(named: "ring.mp3") - UNNotificationSound.default()
            let repeating: Bool = !weekdays.isEmpty
            content.userInfo = [
                "idAlarm" : idAlarm,
                "NotificationIdentifier" : Scheduler.NotificationIdentifier,
                "label" : label,
                "location" : location,
                "description" : description,
                "snooze" : snoozeEnabled,
                "index": index,
                "soundName": soundName,
                "repeating" : repeating
            ] //NSDictionary
            if let path = Bundle.main.path(forResource: "logo_symbol", ofType: "png") {
                let url = URL(fileURLWithPath: path)
                do {
                    let attachment = try UNNotificationAttachment(identifier: "logo_symbol", url: url, options: nil)
                    content.attachments = [attachment]
                } catch {
                    print("Failed to load The attachment.")
                }
            }
            //--UNTimeIntervalNotificationTrigger, cho phép một thông báo được gửi đi một khoảng thời gian sau khi lên lịch nó
            //let timeTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 60.0, repeats: true)
            //--timeTrigger sẽ được kích hoạt một giờ sau khi thông báo được lên lịch. Tham số timeInterval được truyền vào - UNTimeIntervalNotificationTrigger được xác định trong vài giây.
            
            //--UNCalendarNotificationTrigger, cho phép một thông báo sẽ được gửi tại một thời điểm cụ thể, bất kể nó đã được lên lịch khi nào.
            var dateComponents = DateComponents()
            dateComponents.day = date.getDay()?.toInt()
            dateComponents.hour = date.getHouse().toInt()
            dateComponents.minute = date.getMinutes().toInt()
            dateComponents.timeZone = TimeZone.current
            //repeat weekly if repeat weekdays are selected
            //no repeat with snooze notification
            if !weekdays.isEmpty && !onSnooze{
                // Put your weekday indexes in an `IndexSet`
                let weekdaySet = IndexSet(weekdays)
                // Get the current calendar and the weekday from today
                let calendar = Calendar.current
                var weekday =  calendar.component(.weekday, from: date)
                // Calculate the next index
                if let nextWeekday = weekdaySet.integerGreaterThan(weekday) {
                    weekday = nextWeekday
                } else {
                    weekday = weekdaySet.first!
                }
                dateComponents.weekday = weekday
            }
            let calendarTrigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
            //-calendarTrigger sẽ liên tục được kích hoạt hàng ngày lúc 10:00. Thời điểm chính xác của việc kích hoạt dễ dàng có thể được thay đổi bằng cách sửa đổi các thuột tính khác của đối tượng DateComponents mà bạn truyền vào UNCalendarNotificationTrigger.
            
            //--UNLocationNotificationTrigger, cho phép một thông báo được gửi khi người dùng vào hoặc rời khỏi một khu vực địa lý đã định.
            //let center = CLLocationCoordinate2D(latitude: 40.0, longitude: 120.0)
            //let region = CLCircularRegion(center: center, radius: 500.0, identifier: "Location")
            //region.notifyOnEntry = true;
            //region.notifyOnExit = false;
            //let locationTrigger = UNLocationNotificationTrigger(region: region, repeats: true)
            //--locationTrigger sẽ được kích hoạt khi người dùng đến cách 500m tọa độ cho trước, trong trường hợp này 40°N 120°E. Như bạn có thể thấy từ code, kiểu kích hoạt này có thể được sử dụng cho bất kỳ kích thước toạ độ và/hoặc khu vực và cũng có thể kích hoạt các thông báo sau khi cả vào và ra khỏi khu vực.
            
            let request = UNNotificationRequest(identifier: idAlarm, content: content, trigger: calendarTrigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: {(error) in
                if let error = error {
                    print("Uh oh! i had an error: \(error)")
                } else {
                    print("Notification set up successfully --> \(date.toStringFormat(dateFormat: "yyyy-MM-dd HH:mm:ss"))")
                    self.showMinEventPendingList()
                }
            })
            // Register the notification settings.
            let dismissAction = UNNotificationAction(identifier: "Alarm-ios-swift-stop", title: "Dismiss", options: [])
            let alarmCategory = UNNotificationCategory(identifier: self.categoryNotificationEventAction,actions: [dismissAction],intentIdentifiers: [], options: [])
            UNUserNotificationCenter.current().setNotificationCategories([alarmCategory])
        }else{
            let notification: UILocalNotification = UILocalNotification()
            notification.alertAction = label
            notification.alertBody = description
            notification.category = self.categoryNotificationEventAction
            notification.soundName = soundName + ".mp3" //"ring.mp3" - UILocalNotificationDefaultSoundName
            notification.timeZone = TimeZone.current
            let repeating: Bool = !weekdays.isEmpty
            notification.userInfo = [
                "idAlarm" : idAlarm,
                "NotificationIdentifier" : Scheduler.NotificationIdentifier,
                "label" : label,
                "location" : location,
                "description" : description,
                "snooze" : snoozeEnabled,
                "index": index,
                "soundName": soundName,
                "repeating" : repeating
            ] //NSDictionary
            //repeat weekly if repeat weekdays are selected
            //no repeat with snooze notification
            if !weekdays.isEmpty && !onSnooze{
                notification.repeatInterval = NSCalendar.Unit.weekOfYear // 0 means don't repeat
            }
            let datesForNotification = correctDate(date, onWeekdaysForNotify:weekdays)
            syncAlarmModel()
            for d in datesForNotification {
                if onSnooze { //Notes check onSnooze -->
                    alarmModel.alarms[index].date = Scheduler.correctSecondComponent(date: alarmModel.alarms[index].date)
                }
                else {
                    alarmModel.alarms[index].date = d
                }
                notification.fireDate = d
                //notification.fireDate = NSDate(timeIntervalSinceNow: date.timeIntervalSinceNow) as Date
                print("Notification set up successfully --> \(notification.nextFireDate?.toStringFormat(dateFormat: "yyyy-MM-dd HH:mm:ss") ?? "")")
                UIApplication.shared.scheduleLocalNotification(notification)
            }
            // Register the notification settings.
            UIApplication.shared.registerUserNotificationSettings(setupNotificationSettings())
            showMinEventPendingList()
        }
    }
    
    public func setupNotificationSettings() -> UIUserNotificationSettings {
        var snoozeEnabled: Bool = false
        if let n = UIApplication.shared.scheduledLocalNotifications {
            if let result = minFireDateWithIndex(notifications: n) {
                let i = result.1
                snoozeEnabled = alarmModel.alarms[i].snoozeEnabled //notes
            }
        }
        // Specify the notification types.
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.sound]
        
        // Specify the notification actions.
        let stopAction = UIMutableUserNotificationAction()
        stopAction.identifier = "Alarm-ios-swift-stop"
        stopAction.title = "OK"
        stopAction.activationMode = UIUserNotificationActivationMode.background
        stopAction.isDestructive = false
        stopAction.isAuthenticationRequired = false
        
        let snoozeAction = UIMutableUserNotificationAction()
        snoozeAction.identifier = "Alarm-ios-swift-snooze"
        snoozeAction.title = "Snooze"
        snoozeAction.activationMode = UIUserNotificationActivationMode.background
        snoozeAction.isDestructive = false
        snoozeAction.isAuthenticationRequired = false
        
        let actionsArray = snoozeEnabled ? [UIUserNotificationAction](arrayLiteral: snoozeAction, stopAction) : [UIUserNotificationAction](arrayLiteral: stopAction)
        let actionsArrayMinimal = snoozeEnabled ? [UIUserNotificationAction](arrayLiteral: snoozeAction, stopAction) : [UIUserNotificationAction](arrayLiteral: stopAction)
        // Specify the category related to the above actions.
        let alarmCategory = UIMutableUserNotificationCategory()
        alarmCategory.identifier = self.categoryNotificationEventAction
        alarmCategory.setActions(actionsArray, for: .default)
        alarmCategory.setActions(actionsArrayMinimal, for: .minimal)
        
        let categoriesForSettings = Set(arrayLiteral: alarmCategory)
        let newNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: categoriesForSettings)
        
        return newNotificationSettings
    }
    
    private func correctDate(_ date: Date, onWeekdaysForNotify weekdays:[Int]) -> [Date] {
        var correctedDate: [Date] = [Date]()
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let now = Date()
        let flags: NSCalendar.Unit = [NSCalendar.Unit.weekday, NSCalendar.Unit.weekdayOrdinal, NSCalendar.Unit.day]
        let dateComponents = (calendar as NSCalendar).components(flags, from: date)
        let weekday:Int = dateComponents.weekday!
        
        //no repeat
        if weekdays.isEmpty{
            //scheduling date is eariler than current date
            if date < now {
                //plus one day, otherwise the notification will be fired righton
                correctedDate.append((calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: 1, to: date, options:.matchStrictly)!)
            }
            else { //later
                correctedDate.append(date)
            }
            return correctedDate
        }
        //repeat
        else {
            let daysInWeek = 7
            correctedDate.removeAll(keepingCapacity: true)
            for wd in weekdays {
                
                var wdDate: Date!
                //schedule on next week
                if compare(weekday: wd, with: weekday) == .before {
                    wdDate =  (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: wd+daysInWeek-weekday, to: date, options:.matchStrictly)!
                }
                //schedule on today or next week
                else if compare(weekday: wd, with: weekday) == .same {
                    //scheduling date is eariler than current date, then schedule on next week
                    if date.compare(now) == ComparisonResult.orderedAscending {
                        wdDate = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: daysInWeek, to: date, options:.matchStrictly)!
                    }
                    else { //later
                        wdDate = date
                    }
                }
                //schedule on next days of this week
                else { //after
                    wdDate =  (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.day, value: wd-weekday, to: date, options:.matchStrictly)!
                }
                
                //fix second component to 0
                wdDate = Scheduler.correctSecondComponent(date: wdDate, calendar: calendar)
                correctedDate.append(wdDate)
            }
            return correctedDate
        }
    }
    
    public static func correctSecondComponent(date: Date, calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian))->Date {
        let second = calendar.component(.second, from: date)
        let d = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.second, value: -second, to: date, options:.matchStrictly)!
        return d
    }
    
    public func setNotificationForSnooze(idAlarm: String, label: String, location: String, description: String, snoozeMinute: Int, soundName: String, index: Int) {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let now = Date()
        let snoozeTime = (calendar as NSCalendar).date(byAdding: NSCalendar.Unit.minute, value: snoozeMinute, to: now, options:.matchStrictly)!
        setNotificationWithDate(idAlarm, snoozeTime, label, location,description, onWeekdaysForNotify: [Int](), snoozeEnabled: true, onSnooze:true, soundName: soundName, index: index)
    }
    
    // workaround for some situation that alarm model is not setting properly (when app on background or not launched)
    public func checkNotification() {
        alarmModel = Alarms()
        let notifications = UIApplication.shared.scheduledLocalNotifications
        if notifications!.isEmpty {
            for i in 0..<alarmModel.count {
                alarmModel.alarms[i].enabled = false
            }
        }
        else {
            for (i, alarm) in alarmModel.alarms.enumerated() {
                var isOutDated = true
                if alarm.onSnooze {
                    isOutDated = false
                }
                for n in notifications! {
                    if let date = n.fireDate, alarm.date >= date {
                        isOutDated = false
                    }
                }
                if isOutDated {
                    alarmModel.alarms[i].enabled = false
                }
            }
        }
    }
    
    private func syncAlarmModel() {
        alarmModel = Alarms()
    }
    
    private enum weekdaysComparisonResult {
        case before
        case same
        case after
    }
    
    private func compare(weekday w1: Int, with w2: Int) -> weekdaysComparisonResult {
        if w1 != 1 && w2 == 1 {return .before}
        else if w1 == w2 {return .same}
        else {return .after}
    }
    
    private func minFireDateWithIndex(notifications: [UILocalNotification]) -> (Date, Int)? {
        if notifications.isEmpty {
            return nil
        }
        var minIndex = -1
        var minDate: Date = notifications.first!.fireDate!
        for n in notifications {
            let index = n.userInfo!["index"] as! Int
            if(n.fireDate! <= minDate) {
                minDate = n.fireDate!
                minIndex = index
            }
        }
        return (minDate, minIndex)
    }
    
    //-----
    public func removeAlarmTime(identifier: String)  {
        if #available(iOS 10.0, *), self.checkIOS10 {
            showMinEventPendingList()
            //UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
            UNUserNotificationCenter.current().getPendingNotificationRequests {
                (requests) in
                for request in requests{
                    if let idAlarm = request.content.userInfo["idAlarm"] as? String, idAlarm == identifier {
                        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [request.identifier])
                    }
                }
            }
            print("Remove Item with Identifier = \(identifier) Success")
        }else{
            showMinEventPendingList()
            if let listNotification = UIApplication.shared.scheduledLocalNotifications {
                for notification in listNotification {
                    if let idAlarm = notification.userInfo?["idAlarm"] as? String, idAlarm == identifier {
                        UIApplication.shared.cancelLocalNotification(notification)
                    }
                }
            }
            print("Remove Item with Identifier = \(identifier) Success")
        }
    }
    
    public func removeAllAlarmTime() {
        if #available(iOS 10.0, *), self.checkIOS10 {
            showMinEventPendingList()
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            print("Remove ALL Success")
        }else{
            showMinEventPendingList()
            UIApplication.shared.cancelAllLocalNotifications()
            print("Remove ALL Success")
        }
    }
    
    public func showMinEventPendingList() {
        if #available(iOS 10.0, *), self.checkIOS10 {
            UNUserNotificationCenter.current().getPendingNotificationRequests {
                (requests) in
                var nextTriggerDates: [Date] = []
                for request in requests{
                    if let trigger = request.trigger as? UNCalendarNotificationTrigger,
                        let triggerDate = trigger.nextTriggerDate(){
                        nextTriggerDates.append(triggerDate)
                    }
                }
                let nextTriggerDate = nextTriggerDates.min()
                print("Trigger NEXT MIN IOS 10.0: --> \(String(describing: nextTriggerDate?.toStringFormat(dateFormat: "dd-MM-yyyy HH:mm:ss")))")
            }
        }else {
            var nextTriggerDates: [Date] = []
            if let listNotification = UIApplication.shared.scheduledLocalNotifications {
                for notification in listNotification {
                    if let day = notification.nextFireDate {
                        nextTriggerDates.append(day)
                    }
                }
            }
            let nextTriggerDate = nextTriggerDates.min()
            print("Trigger NEXT MIN IOS 8.0: --> \(String(describing: nextTriggerDate?.toStringFormat(dateFormat: "dd-MM-yyyy HH:mm:ss")))")
        }
    }
    
    public func showAllEventPendingList() {
        if #available(iOS 10.0, *), self.checkIOS10 {
            UNUserNotificationCenter.current().getPendingNotificationRequests {
                (requests) in
                var index = 0
                for request in requests{
                    if let trigger = request.trigger as? UNCalendarNotificationTrigger,
                        let triggerDate = trigger.nextTriggerDate(){
                        print("\(index) - Trigger NEXT ALL IOS 10.0: --> \(String(describing: triggerDate.toStringFormat(dateFormat: "dd-MM-yyyy HH:mm:ss")))")
                    }
                    index = index + 1
                }
            }
        }else {
            if let listNotification = UIApplication.shared.scheduledLocalNotifications {
                var index = 0
                for notification in listNotification {
                    if let day = notification.nextFireDate {
                        print("\(index) - Trigger NEXT ALL IOS 8.0: --> \(String(describing: day.toStringFormat(dateFormat: "dd-MM-yyyy HH:mm:ss")))")
                    }
                    index = index + 1
                }
            }
        }
    }
    //---
}

extension UILocalNotification {
    
    public var nextFireDate: Date? {
        guard let fireDate = fireDate else { return nil }
        
        let today = Date()
        let cal = Calendar.current
        
        if fireDate.compare(today) == .orderedDescending {
            return fireDate
        }
        
        let s: Set<Calendar.Component>
        switch repeatInterval {
        case .year: s = [.month, .day, .hour, .minute, .second]
        case .month: s = [.day, .hour, .minute, .second]
        case .day: s = [.hour, .minute, .second]
        case .hour: s = [.minute, .second]
        case .minute: s = [.second]
        default: return nil // Not supporting other intervals
        }
        
        let components = cal.dateComponents(s, from: fireDate)
        return cal.nextDate(after: today, matching: components, matchingPolicy: .nextTimePreservingSmallerComponents)
    }
}
