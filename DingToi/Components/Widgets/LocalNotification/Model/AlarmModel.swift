//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import MediaPlayer

public struct Alarm: PropertyReflectable {
    var idAlarm: String = ""
    var date: Date = Date()
    var enabled: Bool = false
    var snoozeEnabled: Bool = false
    var repeatWeekdays: [Int] = []
    var uuid: String = ""
    var mediaID: String = ""
    var mediaLabel: String = "bell"
    var label: String = "Alarm"
    var location: String = "Location"
    var description: String = "Description"
    var onSnooze: Bool = false
    
    init(){}
    
    init(idAlarm:String, date:Date, enabled:Bool, snoozeEnabled:Bool, repeatWeekdays:[Int], uuid:String, mediaID:String, mediaLabel:String, label:String, location:String, description:String, onSnooze: Bool){
        self.idAlarm = idAlarm
        self.date = date
        self.enabled = enabled
        self.snoozeEnabled = snoozeEnabled
        self.repeatWeekdays = repeatWeekdays
        self.uuid = uuid
        self.mediaID = mediaID
        self.mediaLabel = mediaLabel
        self.label = label
        self.location = location
        self.description = description
        self.onSnooze = onSnooze
    }
    
    public init(_ dict: PropertyReflectable.RepresentationType){
        idAlarm = dict["idAlarm"] as? String ?? ""
        date = dict["date"] as! Date
        enabled = dict["enabled"] as? Bool ?? false
        snoozeEnabled = dict["snoozeEnabled"] as? Bool ?? false
        repeatWeekdays = dict["repeatWeekdays"] as? [Int] ?? []
        uuid = dict["uuid"] as? String ?? ""
        mediaID = dict["mediaID"] as? String ?? ""
        mediaLabel = dict["mediaLabel"] as? String ?? ""
        label = dict["label"] as? String ?? ""
        location = dict["location"] as? String ?? ""
        description = dict["description"] as? String ?? ""
        onSnooze = dict["onSnooze"] as? Bool ?? false
    }
    
    public static var propertyCount: Int = 12 //Số lượng property trong Alarm
}

extension Alarm {
    public var formattedTime: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: self.date)
    }
}

public class Alarms: Persistable {
    public let ud: UserDefaults = UserDefaults.standard
    public let persistKey: String = "myAlarmKey" //Key save list Alarm trong UserDefaults
    public var alarms: [Alarm] = [] {
        didSet{
            persist()
        }
    }
    
    //UserDefaults --> lấy [PropertyReflectable.RepresentationType] --> [Alarm] --> self.alarms: [Alarm]
    init() {
        alarms = getAlarms()
        //removeAllItemDisable()//Remover all item Alarm --> item.enabled == false
        removeAllItemOld() //Remover all item Alarm --> Old
        alarms = getAlarms()
    }
    
    //Lấy tấc cả Alarm trong UserDefaults --> lấy [PropertyReflectable.RepresentationType] --> [Alarm]
    public func getAlarms() -> [Alarm] {
        let array = UserDefaults.standard.array(forKey: persistKey)
        //Trường hợp [PropertyReflectable.RepresentationType] trong UserDefaults == nil
        guard let alarmArray = array else{
            return [Alarm]()
        }
        //Trường hợp [PropertyReflectable.RepresentationType] trong UserDefaults != nil --> trả về [Alarm]
        if let dicts = alarmArray as? [PropertyReflectable.RepresentationType]{
            if dicts.first?.count == Alarm.propertyCount {
                return dicts.map{Alarm($0)}
            }
        }
        //Remove All [PropertyReflectable.RepresentationType] trong UserDefaults
        unpersist()
        return [Alarm]()
    }
    
    //Save All Alarm get [Alarm] --> [PropertyReflectable.RepresentationType] --> save UserDefaults
    public func persist() {
        ud.set(getAlarmsDictRepresentation(), forKey: persistKey)
        ud.synchronize()
    }
    
    //Lấy [Alarm] --> [PropertyReflectable.RepresentationType]
    public  func getAlarmsDictRepresentation()->[PropertyReflectable.RepresentationType] {
        return alarms.map {$0.propertyDictRepresentation}
    }
    
    //Remove All Alarm [PropertyReflectable.RepresentationType] trong UserDefaults
    public func unpersist() {
        for key in ud.dictionaryRepresentation().keys {
            if key == persistKey {
                UserDefaults.standard.removeObject(forKey: key.description)
            }
        }
    }
    
    //Xóa toàn bộ item Alarm --> item.enabled == false
    public func removeAllItemDisable() {
        var index = 0
        var alarmsTemp: [Alarm] = []
        for item in self.alarms {
            if item.enabled {
                alarmsTemp.append(item)
            }
            index = index + 1
        }
        self.alarms = alarmsTemp
        unpersist()
        persist()
    }
    
    //Xóa toàn bộ item Alarm --> Old
    public func removeAllItemOld() {
        var index = 0
        var alarmsTemp: [Alarm] = []
        for item in self.alarms {
            if item.date >= Date().getBeginDateCurrentOfMonth() {
                alarmsTemp.append(item)
            }
            index = index + 1
        }
        self.alarms = alarmsTemp
        unpersist()
        persist()
    }
    
    //Xóa toàn bộ Item Alarm
    public func removeAll() {
        unpersist()
        self.alarms.removeAll()
        Scheduler().removeAllAlarmTime()
    }
    
    //Xóa Item width idAlarm
    public func removeItem(idAlarm: String, dateRemove: String?) {
        if let date = dateRemove, date != "" {
            var index = 0
            for item in self.alarms {
                if item.date.toStringFormat(dateFormat: "yyyy-MM-dd") == date && item.idAlarm == idAlarm {
                    self.alarms.remove(at: index)
                    unpersist()
                    persist()
                    Scheduler().removeAlarmTime(identifier: idAlarm)
                    break
                }
                index = index + 1
            }
        }
    }
    
    //Số lượng Alarm trong self.alarms
    public var count: Int {
        return alarms.count
    }
    
}
