//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import DeviceKit

public class TabbarView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var  _collectionView: UICollectionView?
    var  _baseView: BaseView?
    var delegate: TabbarView_Delegate?
    
    var listCatalogy: Array<TabbarViewModel>
    var _heightCollectionView: CGFloat = 60.0 {
        didSet{
            _collectionView?.reloadData()
        }
    }
    var _widthItemCollectionView: CGFloat = UIScreen.main.bounds.width/3 {
        didSet{
            _collectionView?.reloadData()
        }
    }
    var _checkAuto = false {
        didSet{
            _collectionView?.reloadData()
        }
    }
    var _paddingLeftRight: CGFloat = UIDevice().autoSize(10.0, 15.0, 20.0, 20.0) {
        didSet{
            _collectionView?.reloadData()
        }
    }
    var _colorBackgroudCatalory = UIColor.rgb(fromHexString: "#004FA5"){
        didSet{
            _collectionView?.backgroundColor = self._colorBackgroudCatalory
            _collectionView?.reloadData()
        }
    }
    var _fontText = UIFont.init(name: "Roboto-Medium", size: UIDevice().autoSize(10.0, 13.0, 14.0, 15.0)){
        didSet{
            _collectionView?.reloadData()
        }
    }
    var _colorTextSelect = UIColor.white{
        didSet{
            _collectionView?.reloadData()
        }
    }
    var _colorLineSelect = UIColor.rgb(fromHexString: "#f8e71c"){
        didSet{
            _collectionView?.reloadData()
        }
    }
    var _colorLineUnSelect = UIColor.clear{
        didSet{
            _collectionView?.reloadData()
        }
    }
    var _colorTextUnSelect = UIColor.rgb(fromHexString: "#8affffff") {
        didSet{
            _collectionView?.reloadData()
        }
    }
    var currentIndex = 0
    
    public init(frame: CGRect, listCatalogy: Array<TabbarViewModel>, delegate: TabbarView_Delegate?) {
        self.listCatalogy = listCatalogy
        self.delegate = delegate
        self._widthItemCollectionView = UIScreen.main.bounds.width/CGFloat.init(listCatalogy.count)
        super.init(frame: frame)
        backgroundColor = UIColor.white
        initCollectionView(frame: frame)
        showPage(self.currentIndex)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.listCatalogy = Array<TabbarViewModel>()
        super.init(coder: aDecoder)
    }
    
    //Collection View ./
    func initCollectionView(frame: CGRect) {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: _widthItemCollectionView, height: _heightCollectionView)
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flowLayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 0.0
        
        _collectionView = UICollectionView(frame: CGRect.init(x: 0.0, y: 0.0, width: frame.width, height: self._heightCollectionView), collectionViewLayout: flowLayout)
        _collectionView?.backgroundColor = self._colorBackgroudCatalory
        _collectionView?.showsHorizontalScrollIndicator = false
        _collectionView?.showsVerticalScrollIndicator = false
        _collectionView?.delegate = self
        _collectionView?.dataSource = self
        
        let nibName = UINib(nibName: "TabbarViewCell", bundle: nil)
        _collectionView?.register(nibName, forCellWithReuseIdentifier: "tabbarViewCell")
        
        _collectionView?.setContentOffset(CGPoint.zero, animated: true)
        addSubview(_collectionView!)
        
        //_baseView = BaseView.init(frame: CGRect.init(x: 0.0, y: self._heightCollectionView, width: frame.width, height: frame.height - self._heightCollectionView))
        //_baseView?.backgroundColor = UIColor.white
        //addSubview(_baseView!)
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self._checkAuto {
            let width = self.listCatalogy[indexPath.row].title?.widthOfString(usingFont: UIFont.init(name: (self._fontText?.fontName)!, size: (self._fontText?.pointSize)! + 0.0 )!) ?? 0.0
            return CGSize(width: width + self._paddingLeftRight, height: self._heightCollectionView)
        }
        return CGSize(width: _widthItemCollectionView, height: self._heightCollectionView)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listCatalogy.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tabbarViewCell", for: indexPath) as! TabbarViewCell
        if indexPath.row == self.currentIndex {
            cell.commonInit(self.listCatalogy[indexPath.row], isSelect: true, colorTextSelect: self._colorTextSelect, colorTextUnSelect: self._colorTextUnSelect, fontText: self._fontText!, colorLineSelect: self._colorLineSelect, colorLineUnSelect: self._colorLineUnSelect)
        }else{
            cell.commonInit(self.listCatalogy[indexPath.row], isSelect: false, colorTextSelect: self._colorTextSelect, colorTextUnSelect: self._colorTextUnSelect, fontText: self._fontText!, colorLineSelect: self._colorLineSelect, colorLineUnSelect: self._colorLineUnSelect)
        }
        cell.layoutIfNeeded()
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Tabbar Selected Index: #\(indexPath.item)!")
        self.currentIndex = indexPath.row
        self._collectionView?.reloadData()
        showPage(indexPath.row)
    }
    
    func showPage(_ index: Int) {
        delegate?.showPage(index, self._baseView)
    }
    //Collection View ./
}

public class TabbarViewModel {
    private var _id: Int?
    public var id: Int? {
        get{
            return self._id
        }
        set(newValue){
            self._id = newValue
        }
    }
    
    private var _title: String?
    public var title: String?{
        get{
            return self._title
        }
        set(newValue){
            self._title = newValue
        }
    }
    
    private var _size: String?
    public var size: String?{
        get{
            return self._size
        }
        set(newValue){
            self._size = newValue
        }
    }
    
    init(id: Int?, title: String?, size: String?) {
        self._id = id
        self._title = title
        self._size = size
    }
}

public protocol TabbarView_Delegate {
    func showPage(_ index: Int,_ baseView: UIView?)
}
