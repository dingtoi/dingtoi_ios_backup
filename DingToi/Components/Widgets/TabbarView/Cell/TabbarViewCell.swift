//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

class TabbarViewCell: UICollectionViewCell {

    @IBOutlet weak var _title: UILabel!
    @IBOutlet weak var _size: UILabel!
    @IBOutlet weak var _viewSelect: UIView!
    @IBOutlet weak var _viewUnSelect: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        _viewSelect.backgroundColor = UIColor.rgb(fromHexString: "#f8e71c")
    }
    
    func commonInit(_ value: TabbarViewModel?, isSelect: Bool, colorTextSelect: UIColor, colorTextUnSelect: UIColor, fontText: UIFont, colorLineSelect: UIColor, colorLineUnSelect: UIColor) {
        _title.text = value?.title?.uppercased()
        _title.font = fontText
        _size.text = value?.size
        _size.font = fontText
        DispatchQueue.main.async {
            self._viewSelect.backgroundColor = colorLineSelect
            self._viewSelect.setNeedsLayout()
            self._viewUnSelect.backgroundColor = colorLineUnSelect
            self._viewUnSelect.setNeedsLayout()
            if isSelect {
                self._title.textColor = colorTextSelect
                self._size.textColor = colorTextSelect
                self._viewSelect.isHidden = !isSelect
            }else{
                self._title.textColor = colorTextUnSelect
                self._size.textColor = colorTextUnSelect
                self._viewSelect.isHidden = !isSelect
            }
        }
    }
}
