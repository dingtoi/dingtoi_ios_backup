//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import DeviceKit

class SizePickerView {
    public static let groupOfSmall: [Device] = [.iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE, .simulator(.iPhone5), .simulator(.iPhone5c), .simulator(.iPhone5s), .simulator(.iPhoneSE)]
    public static let groupOfMedium: [Device] = [.iPhone6, .iPhone6s, .iPhone7, .iPhone8, .simulator(.iPhone6), .simulator(.iPhone6s), .simulator(.iPhone7), .simulator(.iPhone8)]
    public static let groupOfLarge: [Device] = [.iPhone6Plus,.iPhone6sPlus, .iPhone7Plus, .iPhone8Plus, .iPhoneX, .simulator(.iPhone6Plus), .simulator(.iPhone6sPlus), .simulator(.iPhone7Plus), .simulator(.iPhone8Plus), .simulator(.iPhoneX)]
    
    public static var FONT_SIZE_BUTTON_PICKER: CGFloat {
        if Device.current.isOneOf(groupOfSmall) {
            return 16
        } else if Device.current.isOneOf(groupOfLarge) {
            return 19
        }
        return 18
    }
    
    public static var SIZE_CELL_PICKERVIEW_WIDGETS: CGFloat {
        if Device.current.isOneOf(groupOfSmall) {
            return 30.0
        } else if Device.current.isOneOf(groupOfLarge) {
            return 40.0
        }
        return 35.0
    }
    
    public static var FONT_CELL_PICKER_VIEW: CGFloat {
        if Device.current.isOneOf(groupOfSmall) {
            return 16
        } else if Device.current.isOneOf(groupOfLarge) {
            return 22
        }
        return 18
    }
    
    public static var FONT_TEXT_IN_BUTTON_BEFORE_REGISTER: CGFloat {
        if Device.current.isOneOf(groupOfSmall) {
            return 16
        } else if Device.current.isOneOf(groupOfLarge) {
            return 19
        }
        return 18
    }
    
}

extension String {
    public static let kDONE:String = "DONE"
    public static let kCANCEL:String = "CANCEL"
}

extension UIColor {
    public class var ErrorRed : UIColor {
        return rgb(fromHexString: "#FC0D0D")
    }
    
    public class func rgb(fromHex: Int) -> UIColor {
        let red =   CGFloat((fromHex & 0xFF0000) >> 16) / 0xFF
        let green = CGFloat((fromHex & 0x00FF00) >> 8) / 0xFF
        let blue =  CGFloat(fromHex & 0x0000FF) / 0xFF
        let alpha = CGFloat(1.0)
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    public class func rgb(fromHexString: String) -> UIColor {
        var cString = fromHexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}


public class PickerViewWidget: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    public static let TAGPICKERVIEW:Int = 1111111111
    
    private var _viewHeader: UIView?
    public var viewHeader: UIView? {
        get{
            return _viewHeader
        }
        set(newValue){
            self._viewHeader = newValue
        }
    }
    
    private var _heightViewHeader: CGFloat = 50
    public var heightViewHeader: CGFloat {
        get{
            return self._heightViewHeader
        }
        set(newValue){
            self._heightViewHeader = newValue
        }
    }
    
    private var _widthButtonViewHeader: CGFloat = 70
    public var widthButtonViewHeader: CGFloat {
        get{
            return self._widthButtonViewHeader
        }
        set(newValue){
            self._widthButtonViewHeader = newValue
        }
    }
    
    private var _fontButton:UIFont = UIFont(name: "Roboto-Bold", size: SizePickerView.FONT_SIZE_BUTTON_PICKER)!
    public var fontButton:UIFont {
        get{
            return self._fontButton
        }
        set(newValue){
            self._fontButton = newValue
        }
    }
    
    private var _heightCell: CGFloat = SizePickerView.SIZE_CELL_PICKERVIEW_WIDGETS
    public var heightCell: CGFloat {
        get{
            return self._heightCell
        }
        set(newValue){
            self._heightCell = newValue
        }
    }
    
    private var _textColorCell: UIColor = UIColor.black
    public var textColorCell: UIColor {
        get{
            return self._textColorCell
        }
        set(newValue){
            self._textColorCell = newValue
        }
    }
    
    private var _fontCell: UIFont = UIFont(name: "Roboto-Bold", size: SizePickerView.FONT_CELL_PICKER_VIEW)!
    public var fontCell: UIFont {
        get{
            return self._fontCell
        }
        set(newValue){
            self._fontCell = newValue
        }
    }
    
    private var _listArray: Array<String>?
    public var listArray: Array<String>? {
        get{
            return self._listArray
        }
        set(newValue){
            self._listArray = newValue
        }
    }
    
    private var _title: String?
    public var title: String? {
        get{
            return self._title
        }
        set(newValue){
            self._title = newValue
        }
    }
    
    private var _fontTitle: UIFont = UIFont(name: "Roboto-Bold", size: SizePickerView.FONT_TEXT_IN_BUTTON_BEFORE_REGISTER)!
    public var fontTitle: UIFont {
        get{
            return self._fontTitle
        }
        set(newValue){
            self._fontTitle = newValue
        }
    }
    
    typealias Action = (String) -> (Void)
    var completion: Action?
    public var result: String?
    public var selectRowValue: String = ""
    
    public init(_ title: String,_ valueSelect: String,_ list: Array<String>,_ completion: (@escaping (String) -> (Void)) ) {
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        super.init(frame:CGRect.init(x: 0, y: (height - (height/3)), width: width, height: (height/3)))
        self.backgroundColor = UIColor.white
        self.showsSelectionIndicator = true
        self.delegate = self
        self.dataSource = self
        self.selectRowValue = valueSelect
        self._listArray = list
        self.completion = completion
        
        let positionY = (height - (height/3)) - self._heightViewHeader
        _viewHeader = UIView.init(frame: CGRect.init(x: 0, y: positionY, width: width, height: self._heightViewHeader))
        _viewHeader?.backgroundColor = UIColor.white
        
        let btnCancel = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: self._widthButtonViewHeader, height: self._heightViewHeader))
        btnCancel.setTitle(String.kCANCEL, for: UIControlState.normal)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.titleLabel?.adjustsFontSizeToFitWidth = true
        btnCancel.titleLabel?.font = self._fontButton
        btnCancel.setTitleColor(UIColor.rgb(fromHexString: "#0054FF"), for: UIControlState.normal)
        btnCancel.addTarget(self, action: #selector(self.cancelPicker), for:.touchUpInside)
        
        let btnDone = UIButton.init(frame: CGRect.init(x: (width - self._widthButtonViewHeader), y: 0, width: self._widthButtonViewHeader, height: self._heightViewHeader))
        btnDone.setTitle(String.kDONE, for: UIControlState.normal)
        btnDone.backgroundColor = UIColor.clear
        btnDone.titleLabel?.adjustsFontSizeToFitWidth = true
        btnDone.titleLabel?.font = self._fontButton
        btnDone.setTitleColor(UIColor.rgb(fromHexString: "#0054FF"), for: UIControlState.normal)
        btnDone.addTarget(self, action: #selector(self.donePicker), for:.touchUpInside)
        
        let label = UILabel.init(frame: CGRect.init(x: self._widthButtonViewHeader, y: 0, width: (width - self._widthButtonViewHeader*2), height: self._heightViewHeader))
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.red
        label.textAlignment = NSTextAlignment.center
        label.text = title
        label.font = _fontTitle
        
        _viewHeader?.addSubview(label)
        _viewHeader?.addSubview(btnCancel)
        _viewHeader?.addSubview(btnDone)
    }
    
    init(_ title: String,_ fontTitle: UIFont,_ valueSelect: String,_ list: Array<String>,_ completion: (@escaping (String) -> (Void)) ) {
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        super.init(frame:CGRect.init(x: 0, y: (height - (height/3)), width: width, height: (height/3)))
        self.backgroundColor = UIColor.white
        self.showsSelectionIndicator = true
        self.delegate = self
        self.dataSource = self
        self.selectRowValue = valueSelect
        self._listArray = list
        self.completion = completion
        self._fontTitle = fontTitle
        
        let positionY = (height - (height/3)) - self._heightViewHeader
        _viewHeader = UIView.init(frame: CGRect.init(x: 0, y: positionY, width: width, height: self._heightViewHeader))
        _viewHeader?.backgroundColor = UIColor.white
        
        let btnCancel = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: self._widthButtonViewHeader, height: self._heightViewHeader))
        btnCancel.setTitle(String.kCANCEL, for: UIControlState.normal)
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.titleLabel?.adjustsFontSizeToFitWidth = true
        btnCancel.titleLabel?.font = self._fontButton
        btnCancel.setTitleColor(UIColor.rgb(fromHexString: "#0054FF"), for: UIControlState.normal)
        btnCancel.addTarget(self, action: #selector(self.cancelPicker), for:.touchUpInside)
        
        let btnDone = UIButton.init(frame: CGRect.init(x: (width - self._widthButtonViewHeader), y: 0, width: self._widthButtonViewHeader, height: self._heightViewHeader))
        btnDone.setTitle(String.kDONE, for: UIControlState.normal)
        btnDone.backgroundColor = UIColor.clear
        btnDone.titleLabel?.adjustsFontSizeToFitWidth = true
        btnDone.titleLabel?.font = self._fontButton
        btnDone.setTitleColor(UIColor.rgb(fromHexString: "#0054FF"), for: UIControlState.normal)
        btnDone.addTarget(self, action: #selector(self.donePicker), for:.touchUpInside)
        
        let label = UILabel.init(frame: CGRect.init(x: self._widthButtonViewHeader, y: 0, width: (width - self._widthButtonViewHeader*2), height: self._heightViewHeader))
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.red
        label.textAlignment = NSTextAlignment.center
        label.text = title
        label.font = _fontTitle
        label.adjustsFontSizeToFitWidth = true
        
        _viewHeader?.addSubview(label)
        _viewHeader?.addSubview(btnCancel)
        _viewHeader?.addSubview(btnDone)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func donePicker(sender: UIButton) {
        completion!(result ?? "")
        self.hide()
    }
    
    @objc func cancelPicker(sender: UIButton) {
        self.hide()
    }
    
    public func hide() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromBottom
        
        if appDelegate.window!.viewWithTag(PickerViewWidget.TAGPICKERVIEW + 1) != nil {
            appDelegate.window!.viewWithTag(PickerViewWidget.TAGPICKERVIEW + 1)?.removeFromSuperview()
        }
        if appDelegate.window!.viewWithTag(PickerViewWidget.TAGPICKERVIEW) != nil {
            let view = appDelegate.window!.viewWithTag(PickerViewWidget.TAGPICKERVIEW)
            view?.layer.add(transition, forKey: nil)
            view?.removeFromSuperview()
        }
    }
    
    public func show() {
        self.reloadAllComponents()
        if let temp = self._listArray {
            if temp.count >= 2 {
                var index = 1
                var i = 0
                for x in temp {
                    if x == self.selectRowValue {
                        index = i
                        break
                    }
                    i += 1
                }
                selectRow(index, inComponent: 0, animated: true)
                result = _listArray?[index]
            }
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromTop
        
        let viewOutSide = UIView.init(frame: UIScreen.main.bounds)
        viewOutSide.backgroundColor = UIColor.init(red: 0.41, green: 0.41, blue: 0.41, alpha: 0.5)
        viewOutSide.tag = PickerViewWidget.TAGPICKERVIEW + 1
        appDelegate.window?.addSubview(viewOutSide)
        
        let view:UIView = UIView.init(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.clear
        view.addSubview(self)
        view.addSubview(self._viewHeader!)
        view.tag = PickerViewWidget.TAGPICKERVIEW
        view.layer.add(transition, forKey: nil)
        appDelegate.window!.endEditing(true)
        appDelegate.window!.addSubview(view)
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self._listArray!.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return self._heightCell
    }
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        pickerView.subviews[1].backgroundColor = UIColor.lightGray
        pickerView.subviews[2].backgroundColor = UIColor.lightGray
        
        //let height = self.frame.height
        let width = self.frame.width
        let m_View = UIView.init(frame: CGRect.init(x: 0, y: 0, width: width, height: self._heightCell))
        
        let label:UILabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: width, height: self._heightCell))
        label.font = self._fontCell
        label.text = self._listArray?[row]
        label.textColor = self._textColorCell
        label.textAlignment = NSTextAlignment.center
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        
        m_View.addSubview(label)
        return m_View
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        result = _listArray?[row]
    }
    
}
