//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import CoreLocation

 class ImagePickerWidget: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {
     static var sizeImageValidate = 5400
     static var isCheckSizeImageValidate = false
    var pickerController: UIImagePickerController
    var viewController:UIViewController
    
     var listImgResult = Array<UIImage>()
    var completion_Selected: ((Array<UIImage>) -> (Void))?
    
    
    init(_ viewController: UIViewController) {
        self.listImgResult.removeAll()
        self.pickerController = UIImagePickerController()
        self.viewController = viewController
    }
    
     func show(_ completion: (@escaping (Array<UIImage>) -> (Void))) {
        self.listImgResult.removeAll()
        self.completion_Selected = completion
        self.pickerController = UIImagePickerController()
        self.pickerController.delegate = self
        
        self.initLocationManager()
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction!) in
            return
        }))
        
        self.viewController.present(alert, animated: true, completion: nil)
    }
    
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            pickerController.delegate = self
            pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            pickerController.allowsEditing = false
            viewController.present(pickerController, animated: true, completion: nil)
        }else {
            let alertWarning = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
                print("You pressed OK")
            }
            alertWarning.addAction(okAction)
            alertWarning.show(self.viewController , sender: self)
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            pickerController.delegate = self
            pickerController.sourceType = UIImagePickerControllerSourceType.camera
            pickerController.allowsEditing = false
            viewController.present(pickerController, animated: true, completion: nil)
        }else {
            let alertWarning = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
                print("You pressed OK")
            }
            alertWarning.addAction(okAction)
            alertWarning.show(self.viewController , sender: self)
        }
    }
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
    }
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var checkFileSize = false
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            let imageData: NSData = UIImageJPEGRepresentation(image, 1.0)! as NSData
            print("Image Size \(imageData.length/1024) KB")
            if !ImagePickerWidget.isCheckSizeImageValidate || imageData.length/1024 < ImagePickerWidget.sizeImageValidate {
                //imageData.write(to: getPath(), atomically: true)
//                self.listImgResult.append(image.fixedOrientation())
                self.listImgResult.append(image)
                if let completion = self.completion_Selected {
                    completion(self.listImgResult)
                }
            }else{
                checkFileSize = true
            }
        }else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let imageData: NSData = UIImageJPEGRepresentation(image, 1.0)! as NSData
            print("Image Size \(imageData.length/1024) KB")
            if !ImagePickerWidget.isCheckSizeImageValidate || imageData.length/1024 < ImagePickerWidget.sizeImageValidate {
                //imageData.write(to: getPath(), atomically: true)
//                self.listImgResult.append(image.fixedOrientation())
                self.listImgResult.append(image)
                if let completion = self.completion_Selected {
                    completion(self.listImgResult)
                }
            }else{
                checkFileSize = true
            }
        }else{
            if let completion = self.completion_Selected {
                completion(self.listImgResult)
            }
            print("Something went wrong")
        }
        picker.dismiss(animated: true, completion: nil)
        
        if checkFileSize {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let rootVC = appDelegate.rootNavigationController.visibleViewController as? BaseViewController
            let rootPresent = appDelegate.rootNavigationController.presentedViewController as? BaseViewController
            rootVC?.showDialog(String.kNextStepTitle, String.kMaxFileSize )
            rootPresent?.showDialog(String.kNextStepTitle, String.kMaxFileSize )
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
     func getPath() -> URL {
        let documentsFolder = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let path = NSURL(fileURLWithPath: documentsFolder[0] as String)
        let fullPath = path.appendingPathComponent("images.png")
        //Reading
        //        let data = NSData(contentsOf: fullPath!)
        
        //Deleting
        //        let fileManager = FileManager.default
        //        fileManager.delegate = self
        //        let documentsFolder = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        //        let path = NSURL(fileURLWithPath: documentsFolder[0] as String)
        //        let fullPath = path.appendingPathComponent("choose.png")
        //        fileManager.removeItem(atPath: fullPath!.path) || fileManager.removeItem(at: fullPath!)
        
        return fullPath!
    }
    
     func getValueImg() -> UIImage? {
        if UIImage(contentsOfFile: getPath().path) != nil {
            return UIImage(contentsOfFile: getPath().path)
        }else{
            return nil
        }
    }
    
    //LocationManager
    let manager = CLLocationManager()
    let geocoder = CLGeocoder()
    private func initLocationManager(){
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            manager.requestWhenInUseAuthorization()
        } else {
            manager.startUpdatingLocation()
        }
        print("Request Location")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        print("Response Location: \(location.description)")
        manager.stopUpdatingLocation()
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Error in reverseGeocode")
            }
            
            let placemark = placemarks! as [CLPlacemark] // Place details
            if placemark.count > 0 {
                let placemark = placemarks![0]
                let locationName = placemark.name ?? "" //Location name
                let locality = placemark.locality ?? "" //Locality
                let street = placemark.thoroughfare ?? "" // Street address
                let country = placemark.country ?? ""// Country
                let administrativeArea = placemark.administrativeArea ?? "" //AdministrativeArea
                
                print(locationName)//422 Đường Nguyễn Thị Minh Khai
                print(locality)//Tp. Qui Nhơn
                print(street)//Đường Nguyễn Thị Minh Khai
                print(country)//Việt Nam
                print(administrativeArea)//Tỉnh Bình Định
                
                let userLocationString = "\(street), \(locality), \(administrativeArea), \(country)"
                print("Address --> "+userLocationString)
            }
        })
    }
    
}


