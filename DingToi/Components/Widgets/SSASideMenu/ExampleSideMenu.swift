//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class ExampleSideMenu: UIViewController {
    public var sideMenu: UIView!
    public var screenImageView: UIImageView!
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public func showMenu(sender: UIButton) {
        // Add the background image to the main view
        UIGraphicsBeginImageContext(self.view.frame.size);
        UIImage(named: "splashBG")!.draw(in: self.view.bounds)
        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        view.backgroundColor = UIColor(patternImage: backgroundImage!)
        
        // Capture the current screen
        screenImageView = UIImageView(image: captureScreen())
        screenImageView.tag = 2001
        view.addSubview(screenImageView)
        
        // Initiate Gesture recognizer
        view.addTapGesture(self, #selector(_closeMenu))
        
        // Build the menu from a Nib and add it as a sub view
        sideMenu = UIView.init(frame: UIScreen.main.bounds)
        sideMenu.backgroundColor = .green
        sideMenu.tag = 2002
        sideMenu.alpha = 0
        sideMenu.frame.origin.y = -10
        view.addSubview(sideMenu)
        
        // Hide all other subviews
        for subView in view.subviews as [UIView]{
            if subView.tag < 2000{
                subView.isHidden = true
            }
        }
        
        // Animate the captured image
        var id = CATransform3DIdentity
        id.m34 =  -1.0 / 1000
        
        let rotationTransform = CATransform3DRotate(id, 0.5 * CGFloat(-Double.pi/2), 0, 1.0, 0)
        let translationTransform = CATransform3DMakeTranslation(screenImageView.frame.width * 0.2, 0, 0)
        let transform = CATransform3DConcat(rotationTransform, translationTransform)
        
        UIView.animateKeyframes(withDuration: 1, delay: 0, options: [], animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/3, animations: {
                self.screenImageView.layer.transform = transform
                self.screenImageView.frame.size.height -= 200
                self.screenImageView.center.y += 100
            })
            
            UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 2/3, animations: {
                self.sideMenu.alpha = 1
                self.sideMenu.frame.origin.y = 0
            })
            
        },completion: {_ in
            
        })
    }
    
    @objc func _closeMenu(sender: UITapGestureRecognizer) {
        
        UIView.animateKeyframes(withDuration: 1, delay: 0, options: [], animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 2/3, animations: {
                self.sideMenu.alpha = 0
                self.sideMenu.frame.origin.y = -10
            })
            
            UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 2/3, animations: {
                self.screenImageView.layer.transform = CATransform3DIdentity
                self.screenImageView.frame.size = self.view.frame.size
                self.screenImageView.frame.origin = CGPoint.init(x: 0, y: 0)
            })
            
        },completion: {_ in
            self.screenImageView.removeFromSuperview()
            self.sideMenu.removeFromSuperview()
            
            for subView in self.view.subviews as [UIView]{
                if subView.tag < 2000{
                    subView.isHidden = false
                }
            }
        })
    }
    
    
    public func captureScreen(_ shouldSave: Bool = false) -> UIImage? {
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, shouldSave {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        return screenshotImage
    }
    
}
