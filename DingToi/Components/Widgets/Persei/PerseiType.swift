//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import CoreGraphics

enum ContentType: String, CustomStringConvertible {
    case music = "check.png"
    case films = "icon_map.png"
    case contact = "icon_input_red.png"
    case about = "btnclose.png"
    
    func next(_ index: Int = 0) -> ContentType {
        print("==> Selected: \(index)")
        switch index {
        case 0:
            return .music
        case 1:
            return .films
        case 2:
            return .contact
        case 3:
            return .about
        default:
            return .music
        }
    }
    
    var image: UIImage {
        let image =  UIImage(named: rawValue)!
        return image
    }
    
    var description: String {
        switch self {
        case .music:
            return "New Music"
        case .films:
            return "New Films"
        case .contact:
            return "New Contact"
        case .about:
            return "New About"
        }
    }
}

extension CGRect {
    init(boundingCenter center: CGPoint, radius: CGFloat) {
        assert(0 <= radius, "radius must be a positive value")
        self = CGRect(origin: center, size: .zero).insetBy(dx: -radius, dy: -radius)
    }
}
