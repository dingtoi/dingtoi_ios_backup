//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import UIKit

public struct LabelSwitchConfig {
    public struct GradientBack {
        var colors: [CGColor]
        var startPoint: CGPoint
        var endPoint: CGPoint
    }

    public var text: String
    public var textColor: UIColor
    public var font: UIFont
    public var backgroundColor: UIColor
    public var backGradient: GradientBack?
    public var backImage: UIImage?
    
    public init(text: String, textColor: UIColor, font: UIFont, backgroundColor: UIColor) {
        self.text = text
        self.textColor = textColor
        self.font = font
        self.backgroundColor = backgroundColor
    }
    
    public init(text: String, textColor: UIColor, font: UIFont, gradientColors: [CGColor], startPoint: CGPoint, endPoint: CGPoint) {
        self.init(text: text, textColor: textColor, font: font, backgroundColor: .white)
        self.backGradient = GradientBack(colors: gradientColors, startPoint: startPoint, endPoint: endPoint)
    }
    
    public init(text: String, textColor: UIColor, font: UIFont, image: UIImage?) {
        self.init(text: text, textColor: textColor, font: font, backgroundColor: .white)
        self.backImage = image
    }
    
    public static let defaultLeft = LabelSwitchConfig(text: "Left",
                                                 textColor: .white,
                                                      font: .boldSystemFont(ofSize: 20),
                                           backgroundColor: UIColor.red)
    
    public static let defaultRight = LabelSwitchConfig(text: "Right",
                                                  textColor: .white,
                                                       font: .boldSystemFont(ofSize: 20),
                                            backgroundColor: UIColor.blue)
}

public enum LabelSwitchState {
    case L
    case R
}



