//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import UIKit

public class LabelSwitchBackView: UIView {
    public var gradientLayer: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.isHidden = true
        return layer
    }()
    
    public var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.isHidden = true
        return view
    }()
    
    public override var frame: CGRect {
        didSet {
            gradientLayer.frame = bounds
            imageView.frame = bounds
        }
    }
    
    public init() {
        super.init(frame: .zero)
        addSubview(imageView)
        layer.addSublayer(gradientLayer)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
