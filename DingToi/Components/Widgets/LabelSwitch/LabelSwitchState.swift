//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public struct LabelSwitchPartState {
    public var backMaskFrame: CGRect = .zero
}

public struct LabelSwitchUIState {
    public var backgroundColor: UIColor = .clear
    public var circleFrame:CGRect = .zero
    public var leftPartState  = LabelSwitchPartState()
    public var rightPartState = LabelSwitchPartState()
}
