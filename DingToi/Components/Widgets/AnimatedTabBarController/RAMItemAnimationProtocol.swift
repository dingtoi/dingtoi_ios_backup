//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import UIKit

public protocol RAMItemAnimationProtocol {
    func playAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel)
    func deselectAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel, defaultTextColor: UIColor, defaultIconColor: UIColor)
    func selectedState(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel)
}

open class RAMItemAnimation: NSObject, RAMItemAnimationProtocol {
    
    struct Constants {
        struct AnimationKeys {
            static let Scale = "transform.scale"
            static let Rotation = "transform.rotation"
            static let KeyFrame = "contents"
            static let PositionY = "position.y"
            static let Opacity = "opacity"
        }
    }
    
    // MARK: properties
    // The duration of the animation
    @IBInspectable open var duration: CGFloat = 0.8
    
    //  The text color in selected state.
    @IBInspectable open var textSelectedColor: UIColor = .black
    
    //  The icon color in selected state.
    @IBInspectable open var iconSelectedColor: UIColor = .clear
    
    //Start animation, method call when UITabBarItem is selected
    open func playAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel _: UILabel) {
        fatalError("override method in subclass")
    }
    
    //Start animation, method call when UITabBarItem is unselected
    open func deselectAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel _: UILabel, defaultTextColor _: UIColor, defaultIconColor _: UIColor) {
        fatalError("override method in subclass")
    }
    
    //Method call when TabBarController did load
    open func selectedState(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel _: UILabel) {
        fatalError("override method in subclass")
    }
    
    //(Optional) Method call when TabBarController did load
    open func deselectedState(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel _: UILabel) {}
}
