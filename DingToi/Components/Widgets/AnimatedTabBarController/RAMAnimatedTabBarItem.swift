//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

extension RAMAnimatedTabBarItem {
    
    open override var badgeValue: String? {
        get {
            return badge?.text
        }
        set(newValue) {
            if newValue == nil {
                badge?.removeFromSuperview()
                badge = nil
                return
            }
            if let iconView = iconView, let contanerView = iconView.icon.superview, badge == nil {
                badge = RAMBadge.badge()
                badge!.addBadgeOnView(contanerView)
            }
            badge?.text = newValue
        }
    }
}


open class RAMAnimatedTabBarItem: UITabBarItem {
    
    // animation for UITabBarItem.
    // use RAMFumeAnimation, RAMBounceAnimation, RAMRotationAnimation, RAMFrameItemAnimation, RAMTransitionAnimation
    // or create custom anmation inherit RAMItemAnimation
    @IBOutlet open var animation: RAMItemAnimation!
    
    @IBInspectable open var yOffSet: CGFloat = 0
    
    open override var isEnabled: Bool {
        didSet {
            iconView?.icon.alpha = isEnabled == true ? 1 : 0.5
            iconView?.textLabel.alpha = isEnabled == true ? 1 : 0.5
        }
    }
    
    @IBInspectable open var textFontSize: CGFloat = 10
    @IBInspectable open var textColor: UIColor = UIColor.black
    @IBInspectable open var iconColor: UIColor = UIColor.clear
    open var bgDefaultColor: UIColor = UIColor.clear
    open var bgSelectedColor: UIColor = UIColor.clear
    open var badge: RAMBadge?
    open var iconView: (icon: UIImageView, textLabel: UILabel)?
    @IBOutlet open var _img: UIImage?
    @IBOutlet open var _imgSelected: UIImage?
    
    //animation --> RAMFumeAnimation, RAMBounceAnimation, RAMRotationAnimation, RAMFrameItemAnimation, RAMTransitionAnimation
    public init(title: String?, image: UIImage?, selectedImage: UIImage?, animation: RAMItemAnimation?, tag: Int = 0) {
        super.init()
        //Remove image & selectedImage default & set title default
        self.image = nil
        self.selectedImage = nil
        self.title = title
        
        self.animation = animation
        self.tag = tag
        self._img = image
        self._imgSelected = selectedImage
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // Start selected animation
    open func playAnimation() {
        assert(animation != nil, "add animation in UITabBarItem")
        guard animation != nil && iconView != nil else {
            return
        }
        animation.playAnimation(iconView!.icon, self._img, self._imgSelected, textLabel: iconView!.textLabel)
    }
    
    // Start unselected animation
    open func deselectAnimation() {
        guard animation != nil && iconView != nil else {
            return
        }
        animation.deselectAnimation(
            iconView!.icon, self._img, self._imgSelected,
            textLabel: iconView!.textLabel,
            defaultTextColor: textColor,
            defaultIconColor: iconColor)
    }
    
    // Set SELECTED state without animation
    open func selectedState() {
        guard animation != nil && iconView != nil else {
            return
        }
        animation.selectedState(iconView!.icon, self._img, self._imgSelected, textLabel: iconView!.textLabel)
    }
    
    // Set DESELECTED state without animation
    open func deselectedState() {
        guard animation != nil && iconView != nil else {
            return
        }
        animation.deselectedState(iconView!.icon, self._img, self._imgSelected, textLabel: iconView!.textLabel)
    }
}

