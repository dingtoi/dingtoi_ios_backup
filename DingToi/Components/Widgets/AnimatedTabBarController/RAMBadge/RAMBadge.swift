//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

open class RAMBadge: UILabel {

    internal var topConstraint: NSLayoutConstraint?
    internal var centerXConstraint: NSLayoutConstraint?

    open class func badge() -> RAMBadge {
        return RAMBadge(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)

        layer.backgroundColor = UIColor.red.cgColor
        layer.cornerRadius = frame.size.width / 2

        configureNumberLabel()

        translatesAutoresizingMaskIntoConstraints = false

        // constraints
        createSizeConstraints(frame.size)
    }

    open override var intrinsicContentSize: CGSize {
        var contentSize = super.intrinsicContentSize
        contentSize.width += 10.0
        return contentSize
    }

    public required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // PRAGMA: create

    internal func createSizeConstraints(_ size: CGSize) {
        let widthConstraint = NSLayoutConstraint(
            item: self,
            attribute: NSLayoutConstraint.Attribute.width,
            relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual,
            toItem: nil,
            attribute: NSLayoutConstraint.Attribute.notAnAttribute,
            multiplier: 1,
            constant: size.width)
        addConstraint(widthConstraint)

        let heightConstraint = NSLayoutConstraint(
            item: self,
            attribute: NSLayoutConstraint.Attribute.height,
            relatedBy: NSLayoutConstraint.Relation.equal,
            toItem: nil,
            attribute: NSLayoutConstraint.Attribute.notAnAttribute,
            multiplier: 1,
            constant: size.height)
        addConstraint(heightConstraint)
    }

    fileprivate func configureNumberLabel() {
        textAlignment = .center
        font = UIFont.systemFont(ofSize: 13)
        textColor = UIColor.white
    }

    // PRAGMA: helpers

    open func addBadgeOnView(_ onView: UIView) {

        onView.addSubview(self)

        // create constraints
        let top = NSLayoutConstraint(item: self,
                                           attribute: NSLayoutConstraint.Attribute.top,
                                           relatedBy: NSLayoutConstraint.Relation.equal,
                                           toItem: onView,
                                           attribute: NSLayoutConstraint.Attribute.top,
                                           multiplier: 1,
                                           constant: 3)
        onView.addConstraint(top)
        topConstraint = top

        let centerX = NSLayoutConstraint(item: self,
                                               attribute: NSLayoutConstraint.Attribute.centerX,
                                               relatedBy: NSLayoutConstraint.Relation.equal,
                                               toItem: onView,
                                               attribute: NSLayoutConstraint.Attribute.centerX,
                                               multiplier: 1,
                                               constant: 10)
        onView.addConstraint(centerX)
        centerXConstraint = centerX
    }
}
