//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import QuartzCore
import UIKit

//Chuyển động thay đổi nhiều hình
open class RAMFrameItemAnimation: RAMItemAnimation {
    //List image load from resources with name from plist file
    @nonobjc fileprivate var listAnimationImages: Array<CGImage> = Array()
    
    var selectedImageEnd: UIImage!
    
    // A Boolean value indicated plaing revers animation when UITabBarItem unselected,
    // if false image change immediately, defalut value true
    @IBInspectable open var isDeselectAnimation: Bool = true
    
    // path to array of image names from plist file
    @IBInspectable open var imagesPath: String!
    
    // Case1: Load from plist file
    public init(imagesPath: String) {
        super.init()
        self.createLoadImagesArray(imagesPath: imagesPath)
    }
    
    // Case1: Load from plist file
    func createLoadImagesArray(imagesPath: String) {
        guard let path = Bundle.main.path(forResource: imagesPath, ofType: "plist") else {
            fatalError("don't found plist")
        }
        guard case let animationImagesName as [String] = NSArray(contentsOfFile: path) else {
            fatalError("data in file plist error format")
        }
        
        for name: String in animationImagesName {
            if let image = UIImage(named: name)?.cgImage {
                self.listAnimationImages.append(image)
            }
        }
        
        // selected image --> image last index in file plist
        let selectedImageName = animationImagesName[animationImagesName.endIndex - 1]
        selectedImageEnd = UIImage(named: selectedImageName)
    }
    
    // Case2: Load from list UIImage
    public init(imagesList: Array<UIImage>) {
        super.init()
        self.setAnimationImages(imagesList: imagesList)
    }
    
    // Case2: Load from list UIImage
    open func setAnimationImages(imagesList: Array<UIImage>) {
        var animationImages = Array<CGImage>()
        for image in imagesList {
            if let cgImage = image.cgImage {
                animationImages.append(cgImage)
            }
        }
        self.listAnimationImages = animationImages
        
        // selected image --> image last index in self.listAnimationImages
        selectedImageEnd = imagesList[imagesList.endIndex - 1]
    }
    
    
    // Method call when TabBarController did load for itembar selected
    open override func selectedState(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel) {
        icon.image = self.selectedImageEnd
        textLabel.textColor = textSelectedColor
    }
    
    // Start animation, method call when UITabBarItem is selected
    open override func playAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel) {
        textLabel.textColor = textSelectedColor
        playFrameAnimation(icon, images: listAnimationImages)
    }
    
    // Start animation, method call when UITabBarItem is unselected
    open override func deselectAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel, defaultTextColor: UIColor, defaultIconColor _: UIColor) {
        textLabel.textColor = defaultTextColor
        
        if self.isDeselectAnimation {
            playFrameAnimation(icon, images: listAnimationImages.reversed())
        }
    }
    
    // Play Animation
    @nonobjc func playFrameAnimation(_ icon: UIImageView, images: Array<CGImage>) {
        let frameAnimation = CAKeyframeAnimation(keyPath: Constants.AnimationKeys.KeyFrame)
        frameAnimation.calculationMode = kCAAnimationDiscrete
        frameAnimation.duration = TimeInterval(duration)
        frameAnimation.values = images
        frameAnimation.repeatCount = 1
        frameAnimation.isRemovedOnCompletion = false
        frameAnimation.fillMode = kCAFillModeForwards
        icon.layer.add(frameAnimation, forKey: nil)
    }
}

