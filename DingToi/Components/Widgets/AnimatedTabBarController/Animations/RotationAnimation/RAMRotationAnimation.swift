//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import QuartzCore
import UIKit

//Xoay vòng tròn
open class RAMRotationAnimation: RAMItemAnimation {
    
    public enum RAMRotationDirection {
        case left
        case right
    }

    // Animation direction (left, right)
    open var direction: RAMRotationDirection!

    public override init() {
        super.init()
        direction = RAMRotationDirection.right
        //direction = RAMRotationDirection.left
    }
    
    // Method call when TabBarController did load for itembar selected
    open override func selectedState(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel) {
        textLabel.textColor = textSelectedColor
        
        self.createPulseAnimation(icon)
        
        if let iconImage = imgSelected {
            let renderMode = iconSelectedColor.cgColor.alpha == 0 ? UIImage.RenderingMode.alwaysOriginal :
                UIImage.RenderingMode.alwaysTemplate
            
            let renderImage = iconImage.withRenderingMode(renderMode)
            icon.image = renderImage
            icon.tintColor = iconSelectedColor
        }
    }
    
    // Start animation, method call when UITabBarItem is selected
    open override func playAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel) {
        textLabel.textColor = textSelectedColor
        playRoatationAnimation(icon, img, imgSelected)
    }

    // Start animation, method call when UITabBarItem is unselected
    open override func deselectAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel, defaultTextColor: UIColor, defaultIconColor: UIColor) {
        textLabel.textColor = defaultTextColor

        self.removePulseAnimation(icon)
        
        if let iconImage = img {
            let renderMode = defaultIconColor.cgColor.alpha == 0 ? UIImage.RenderingMode.alwaysOriginal :
                UIImage.RenderingMode.alwaysTemplate
            
            let renderImage = iconImage.withRenderingMode(renderMode)
            icon.image = renderImage
            icon.tintColor = defaultIconColor
        }
    }
    
    // Play Animation
    func playRoatationAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?) {
        let rotateAnimation = CABasicAnimation(keyPath: Constants.AnimationKeys.Rotation)
        rotateAnimation.fromValue = 0.0

        var toValue = CGFloat.pi * 2
        if direction != nil && direction == RAMRotationDirection.left {
            toValue = toValue * -1.0
        }

        rotateAnimation.toValue = toValue
        rotateAnimation.duration = TimeInterval(duration)

        icon.layer.add(rotateAnimation, forKey: nil)

        self.createPulseAnimation(icon)
        
        if let iconImage = imgSelected {
            let renderMode = iconSelectedColor.cgColor.alpha == 0 ? UIImage.RenderingMode.alwaysOriginal :
                UIImage.RenderingMode.alwaysTemplate
            
            let renderImage = iconImage.withRenderingMode(renderMode)
            icon.image = renderImage
            icon.tintColor = iconSelectedColor
        }
    }
    
    // PulseAnimation
    var pulseLayers = [CAShapeLayer]()
    func createPulseAnimation(_ icon: UIImageView) {
        icon.layer.sublayers?.removeAll()
        self.pulseLayers.removeAll()
        let height = icon.image?.size.height ?? Sizes.SIZE_TABBAR_HEIGHT/2.0
        
        for _ in 0...2 {
            let circularPath = UIBezierPath(arcCenter: .zero, radius: height, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
            let pulseLayer = CAShapeLayer()
            pulseLayer.path = circularPath.cgPath
            pulseLayer.lineWidth = 1.5
            pulseLayer.fillColor = UIColor.clear.cgColor
            pulseLayer.lineCap = kCALineCapRound
            pulseLayer.position = CGPoint(x: icon.frame.size.width/2.0, y: icon.frame.size.width/2.0)
            icon.layer.addSublayer(pulseLayer)
            self.pulseLayers.append(pulseLayer)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.animatePulse(index: 0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                self.animatePulse(index: 1)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6, execute: {
                    self.animatePulse(index: 2)
                })
            })
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
            self.removePulseAnimation(icon)
        })
    }
    
    func animatePulse(index: Int) {
        if self.pulseLayers.count > index {
            self.pulseLayers[index].strokeColor = UIColor.rgb(fromHexString: "#8062FD").cgColor // #471E5B - #8062FD
            
            let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 2.0
            scaleAnimation.fromValue = 0.0
            scaleAnimation.toValue = 0.9
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            self.pulseLayers[index].add(scaleAnimation, forKey: "scale")
            
            let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
            opacityAnimation.duration = 2.0
            opacityAnimation.fromValue = 0.9
            opacityAnimation.toValue = 0.0
            opacityAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            opacityAnimation.repeatCount = .greatestFiniteMagnitude
            self.pulseLayers[index].add(opacityAnimation, forKey: "opacity")
        }
    }
    
    func removePulseAnimation(_ icon: UIImageView) {
        icon.layer.sublayers?.removeAll()
        self.pulseLayers.removeAll()
    }
    
}

