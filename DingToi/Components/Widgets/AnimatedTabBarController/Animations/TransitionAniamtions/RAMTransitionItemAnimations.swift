//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

//Lật hình
open class RAMTransitionItemAnimations: RAMItemAnimation {
    
    // Options for animating. Default TransitionNone
    open var transitionOptions: UIViewAnimationOptions!
    
    override init() {
        super.init()
        //transitionOptions = UIViewAnimationOptions()
        transitionOptions = UIViewAnimationOptions.transitionFlipFromLeft
        //transitionOptions = UIViewAnimationOptions.transitionFlipFromRight
        //transitionOptions = UIViewAnimationOptions.transitionFlipFromTop
        //transitionOptions = UIViewAnimationOptions.transitionFlipFromBottom
    }
    
    // Method call when TabBarController did load for itembar selected
    open override func selectedState(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel) {
        selectedColor(icon, img, imgSelected, textLabel: textLabel)
    }
    
    // Start animation, method call when UITabBarItem is selected
    open override func playAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel) {
        selectedColor(icon, img, imgSelected, textLabel: textLabel)
        UIView.transition(with: icon, duration: TimeInterval(duration), options: transitionOptions, animations: {
        }, completion: { _ in
        })
    }
    
    // Start animation, method call when UITabBarItem is unselected
    open override func deselectAnimation(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel, defaultTextColor: UIColor, defaultIconColor: UIColor) {
        
        if let iconImage = img {
            let renderMode = defaultIconColor.cgColor.alpha == 0 ? UIImageRenderingMode.alwaysOriginal :
                UIImageRenderingMode.alwaysTemplate
            let renderImage = iconImage.withRenderingMode(renderMode)
            icon.image = renderImage
            icon.tintColor = defaultIconColor
        }
        textLabel.textColor = defaultTextColor
    }
    
    
    func selectedColor(_ icon: UIImageView,_ img: UIImage?,_ imgSelected: UIImage?, textLabel: UILabel) {
        if let iconImage = imgSelected {
            let renderMode = iconSelectedColor.cgColor.alpha == 0 ? UIImageRenderingMode.alwaysOriginal :
                UIImageRenderingMode.alwaysTemplate
            
            let renderImage = iconImage.withRenderingMode(renderMode)
            icon.image = renderImage
            icon.tintColor = iconSelectedColor
        }
        textLabel.textColor = textSelectedColor
    }
    
}

