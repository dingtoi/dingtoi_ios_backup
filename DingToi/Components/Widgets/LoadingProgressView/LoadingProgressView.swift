//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class LoadingProgressView : UIView {
    
    @IBOutlet weak var _IndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var _baseView: UIView!
    @IBOutlet weak var _title: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = UINib(nibName: String(describing: type(of: self)), bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = UIScreen.main.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        backgroundColor = UIColor.clear
        _baseView.layer.cornerRadius = 10.0
        _baseView.layer.masksToBounds = true
        _IndicatorView.transform = CGAffineTransform(scaleX: 2.50, y: 2.50);
        _IndicatorView.startAnimating()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func updateStatus(value: String) {
        self._title.text = value
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
}

