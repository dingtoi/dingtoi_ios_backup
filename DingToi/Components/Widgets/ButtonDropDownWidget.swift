//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import DeviceKit

class SizeDropDown {
    static let groupOfSmall: [Device] = [.iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE, .simulator(.iPhone5), .simulator(.iPhone5c), .simulator(.iPhone5s), .simulator(.iPhoneSE)]
    static let groupOfMedium: [Device] = [.iPhone6, .iPhone6s, .iPhone7, .iPhone8, .simulator(.iPhone6), .simulator(.iPhone6s), .simulator(.iPhone7), .simulator(.iPhone8)]
    static let groupOfLarge: [Device] = [.iPhone6Plus,.iPhone6sPlus, .iPhone7Plus, .iPhone8Plus, .iPhoneX, .simulator(.iPhone6Plus), .simulator(.iPhone6sPlus), .simulator(.iPhone7Plus), .simulator(.iPhone8Plus), .simulator(.iPhoneX)]
    
    public static var FONT_TEXT_OF_DROPDOWN: CGFloat {
        if Device.current.isOneOf(groupOfSmall) {
            return 1.8
        } else if Device.current.isOneOf(groupOfLarge) {
            return 3.0
        }
        return 2.5
    }
    
    public static var FONT_PERSONAL_INFO_TEXT_IN_LIST: CGFloat {
        if Device.current.isOneOf(groupOfSmall) {
            return 3.0
        } else if Device.current.isOneOf(groupOfLarge) {
            return 5.3
        }
        
        return 4.5
    }
}

public class ButtonDropDownWidget : UIButton {
    
    private var _title:String = ""
    public var title:String {
        get{
            return self._title
        }
        set(newValue){
            self._title = newValue
        }
    }
    
    private var _fontTitle:UIFont = UIFont(name:"Roboto-Regular", size: SizeDropDown.FONT_TEXT_OF_DROPDOWN)!
    public var fontTitle:UIFont {
        get{
            return self._fontTitle
        }
        set(newValue){
            self._fontTitle = newValue
        }
    }
    
    private var _fontTitleChoose:UIFont = UIFont(name:"Roboto-Regular", size: SizeDropDown.FONT_PERSONAL_INFO_TEXT_IN_LIST)!
    public var fontTitleChoose:UIFont {
        get{
            return self._fontTitleChoose
        }
        set(newValue){
            self._fontTitleChoose = newValue
        }
    }
    
    private var _imageButton:UIImage? = UIImage(named: "dropdown2.png")
    public var imageButton:UIImage? {
        get{
            return self._imageButton
        }
        set(newValue){
            self._imageButton = newValue
        }
    }
    
    private var _selectedValue : String = ""
    public var selectedValue : String {
        get {
            return _selectedValue
        }
        set(newValue) {
            _selectedValue = newValue
        }
    }
    
    private var _arraySelectedValue : Array<Int> = []
    public var arraySelectedValue : Array<Int> {
        get {
            return _arraySelectedValue
        }
        set(newValue) {
            _arraySelectedValue = newValue
        }
    }
    
    private var _arraySelected : Array<String> = []
    public var arraySelected : Array<String> {
        get {
            return _arraySelected
        }
        set(newValue) {
            _arraySelected = newValue
        }
    }
    
    private var _displayValue : String = ""
    public var displayValue : String {
        get {
            return _displayValue
        }
        set(newValue) {
            _displayValue = newValue
        }
    }
    
    private var _isShowImage : Bool = true
    public var isShowImage : Bool {
        get {
            return _isShowImage
        }
        set(newValue) {
            _isShowImage = newValue
        }
    }
    
    private var _backgroundImage: UIImageView?
    
    init(_ title: String ,_ view: UIView,_ alignment: UIControlContentHorizontalAlignment, _ font: UIFont?,_ action: Selector,_ target:Any,_ isShowImage: Bool = true) {
        let frameBtn:CGRect = CGRect.init(x: 0, y: 0, width: view.frame.width , height: view.frame.height)
        super.init(frame: frameBtn)
        self.contentHorizontalAlignment = .center
        self.backgroundColor = UIColor.clear
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.rgb(fromHexString: "#b2c6cd").cgColor
        self.layer.cornerRadius = 2
        self.layer.masksToBounds = true
        self.setTitleColor(UIColor.gray, for: UIControlState.normal)
        
        self.titleLabel?.frame = CGRect.init(x: 0, y: 0, width: view.frame.width - view.frame.height, height: view.frame.height)
        if let f = font {
            self._fontTitle = f
        }
        self._title = title
        let text = "<p align='center'><font face='" + self._fontTitle.familyName.description + "' size='" + self._fontTitle.pointSize.description + "'>"+title+"</font>"
        self.setAttributedTitle(text.convertFromHtml_1(), for: UIControlState.normal)
        
        self.titleLabel?.lineBreakMode = .byTruncatingTail
        self.titleLabel?.font = font
        self.titleLabel?.backgroundColor = UIColor.clear
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.minimumScaleFactor = 0.5
        self.titleLabel?.textAlignment = .center
        self.titleLabel?.layoutIfNeeded()
        
        self.addTarget(target, action: action , for: UIControlEvents.touchUpInside)
        
        let widthImage = view.frame.height - 5
        let heightImage = view.frame.height - 5
        let frameIcon:CGRect = CGRect.init(x: view.frame.width - widthImage, y: 2.5, width: widthImage, height: heightImage )
        _backgroundImage = UIImageView.init(frame: frameIcon)
        self.isShowImage = isShowImage
        if self.isShowImage {
            _backgroundImage?.image = self._imageButton
        }else{
            _backgroundImage?.image = nil
        }
        _backgroundImage?.layoutIfNeeded()
        
        self.titleEdgeInsets = UIEdgeInsetsMake( 0, 5, 0, widthImage - 5 )
        self.contentHorizontalAlignment = alignment
        
        self.insertSubview(_backgroundImage!, at: 1)
        view.addSubview(self)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func setValidate(){
        self.layer.borderColor = UIColor.rgb(fromHexString: "#b2c6cd").cgColor
        if self.isShowImage {
            self._backgroundImage?.image = UIImage(named: "dropdown2.png")
        }else{
            self._backgroundImage?.image = nil
        }
    }
    
    public func setValidateDefault(){
        self.layer.borderColor = UIColor.rgb(fromHexString: "#b2c6cd").cgColor
        if self.isShowImage {
            self._backgroundImage?.image = UIImage(named: "dropdown2.png")
        }else{
            self._backgroundImage?.image = nil
        }
        
        let text = "<p align='center'><font face='" + self._fontTitle.familyName.description + "' size='" + self._fontTitle.pointSize.description + "'>"+self._title+"</font>"
        self.setAttributedTitle(text.convertFromHtml_1(), for: UIControlState.normal)
        self.titleEdgeInsets = UIEdgeInsetsMake( 0, 5, 0, frame.height - 10 )
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
        self.titleLabel?.layoutIfNeeded()
        
        self.selectedValue = ""
        self.displayValue = ""
    }
    
    public func setUnValidate(){
        self.layer.borderColor = UIColor.red.cgColor
        if self.isShowImage {
            self._backgroundImage?.image = UIImage(named: "dropdown2.png")
        }else{
            self._backgroundImage?.image = nil
        }
        let text = "<p align='center'><font face='" + self._fontTitle.familyName.description + "' color='#FF0000' size='" + self._fontTitle.pointSize.description + "'>"+self._title+"</font>"
        self.setAttributedTitle(text.convertFromHtml_1(), for: UIControlState.normal)
    }
    
    public func setUnValidate(_ title: String){
        self.layer.borderColor = UIColor.red.cgColor
        if self.isShowImage {
            self._backgroundImage?.image = UIImage(named: "dropdown2.png")
        }else{
            self._backgroundImage?.image = nil
        }
        let text = "<p align='center'><font face='" + self._fontTitle.familyName.description + "' color='#FF0000' size='" + self._fontTitle.pointSize.description + "'>"+title+"</font>"
        self.setAttributedTitle(text.convertFromHtml_1(), for: UIControlState.normal)
    }
    
    public func setColorAfterChoose(){
        self.setTitleColor(UIColor.black, for: UIControlState.normal)
        self.contentHorizontalAlignment = .left
        self.titleEdgeInsets = UIEdgeInsetsMake( 0, 0, 0, 0 )
        self.titleLabel?.font = UIFont(name: self._fontTitle.familyName.description, size: self._fontTitle.pointSize)
    }
    
    public func setColorAfterChoose(align: UIControlContentHorizontalAlignment){
        self.setTitleColor(UIColor.black, for: UIControlState.normal)
        self.contentHorizontalAlignment = align
        self.titleEdgeInsets = UIEdgeInsetsMake( 0, 0, 0, 0 )
        self.titleLabel?.font = UIFont(name: self._fontTitle.familyName.description, size: self._fontTitle.pointSize)
    }
    
    public func setFormatTitleDropListView(display:String, beginPos:Bool = true, valueCompare: Array<String>, require:String){
        let fontStr = "<font face='"+self._fontTitleChoose.familyName.description+"' size='"+self._fontTitleChoose.pointSize.description+"'>"
        if beginPos { //insert begin
            if let pos = display.index(of: " ") { // search begin
                var posCut = pos
                var checkTwo = false
                for itemp in valueCompare {
                    if itemp.index(of: " ") != nil{
                        checkTwo = true
                    }
                }
                if checkTwo {
                    for itemp in valueCompare {
                        if let index = (display.capitalized).index(of: (itemp.capitalized)) {
                            if itemp.index(of: " ") != nil{
                                posCut = index + itemp.count
                                break
                            }
                        }
                    }
                }
                if let begin = display.substring(location: 0, length: posCut), let end = display.substring(location: (posCut+1), length: display.count - (posCut+1)){ // cut string
                    if valueCompare.count > 0 {
                        var check = false
                        for itemp in valueCompare {
                            if begin.capitalized == itemp.capitalized {
                                check = true
                                let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto-Regular'>"+begin+".</font>"+fontStr+""+end+""+"</font><font size = '1' face='Roboto'></font></p>"
                                self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
                                return
                            }
                        }
                        if !check {
                            let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto-Regular'></font>"+fontStr+""+display+""+"</font><font size = '1' face='Roboto'></font></p>"
                            self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
                        }
                    }else{
                        let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto-Regular'></font>"+fontStr+""+display+""+"</font><font size = '1' face='Roboto'></font></p>"
                        self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
                    }
                }else{
                    let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto-Regular'></font>"+fontStr+""+display+""+"</font><font size = '1' face='Roboto'></font></p>"
                    self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
                }
            }else{ // no character " "
                let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto-Regular'></font>"+fontStr+""+display+""+"</font><font size = '1' face='Roboto'></font></p>"
                self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
            }
        }else{ //insert end
            if let pos = display.lastIndex(of: " ") { //search end
                var posCut = pos
                var checkTwo = false
                for itemp in valueCompare {
                    if itemp.index(of: " ") != nil{
                        checkTwo = true
                    }
                }
                if checkTwo {
                    for itemp in valueCompare {
                        if let index = (display.capitalized).index(of: (itemp.capitalized)) {
                            posCut = index - 1
                        }
                    }
                }
                if let begin = display.substring(location: 0, length: posCut), let end = display.substring(location: (posCut+1), length: display.count - (posCut+1)){ // cut string
                    if valueCompare.count > 0 {
                        var check = false
                        for itemp in valueCompare {
                            if end.capitalized == itemp.capitalized {
                                check = true
                                let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto'></font>"+fontStr+""+begin+""+"</font><font size = '1' face='Roboto-Regular'>."+itemp+"</font></p>"
                                self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
                                return
                            }
                        }
                        if !check {
                            let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto-Regular'></font>"+fontStr+""+display+""+"</font><font size = '1' face='Roboto'></font></p>"
                            self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
                        }
                    }else{
                        let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto-Regular'></font>"+fontStr+""+display+""+"</font><font size = '1' face='Roboto'></font></p>"
                        self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
                    }
                }else{
                    let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto-Regular'></font>"+fontStr+""+display+""+"</font><font size = '1' face='Roboto'></font></p>"
                    self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
                }
            }else{ // no character " "
                let attrStr = "<p align='center'><font face='Roboto-Regular' color='#BD081C' size='4.0'>"+require+"</font><font size = '1' face='Roboto-Regular'></font>"+fontStr+""+display+""+"</font><font size = '1' face='Roboto'></font></p>"
                self.setAttributedTitle(attrStr.convertFromHtml_1(), for: .normal)
            }
        }
    }
    
    public func setFormatTitleDropListView(display:String, colorHex: String)  {
        let text = "<p align='center'><font face='" + self._fontTitle.familyName.description + "' color='\(colorHex)' size='" + self._fontTitle.pointSize.description + "'><b>"+display+"</b></font>"
        self.setAttributedTitle(text.convertFromHtml_1(), for: UIControlState.normal)
    }
}

