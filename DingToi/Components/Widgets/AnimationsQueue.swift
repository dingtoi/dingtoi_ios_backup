//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

class AnimationsQueue {
    private var playing = false
    private var animations = [(TimeInterval, () -> Void, () -> Void)]()
    
    init() {
    }
    
    func add(withDuration duration: TimeInterval,
             initializations: @escaping () -> Void,
             animations: @escaping () -> Void) {
        self.animations.append((duration, initializations, animations))
    }
    
    func run() {
        if !playing {
            playing = true
            DispatchQueue.main.async {
                self.next()
            }
        }
    }
    
    func queue(withDuration duration: TimeInterval, initializations: @escaping () -> Void, animations: @escaping () -> Void) {
        self.animations.append((duration, initializations, animations))
        if !playing {
            playing = true
            DispatchQueue.main.async {
                self.next()
            }
        }
    }
    
    private func next() {
        if animations.count > 0 {
            let animation = animations.removeFirst()
            animation.1()
            UIView.animate(withDuration: animation.0, animations: animation.2, completion: { finished in
                self.next()
            })
        } else {
            playing = false
        }
    }
}
