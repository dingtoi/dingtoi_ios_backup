//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import UIKit

class LiquittableCircle : UIView {
    
    var points: [CGPoint] = []
    var isGrow = false {
        didSet {
            grow(isGrow: isGrow)
        }
    }
    var _radius: CGFloat {
        didSet {
            setup()
        }
    }
    var color: UIColor = UIColor.red
    var growColor: UIColor = UIColor.white
    
    init(center: CGPoint, radius: CGFloat, color: UIColor, growColor: UIColor?) {
        let frame = CGRect(x: center.x - radius, y: center.y - radius, width: 2 * radius, height: 2 * radius)
        self._radius = radius
        self.color = color
        if growColor != nil {
            self.growColor = growColor!
        }
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func move(dt: CGPoint) {
        let point = CGPoint(x: center.x + dt.x, y: center.y + dt.y)
        self.center = point
    }
    
    private func setup() {
        self.frame = CGRect(x: center.x - self._radius, y: center.y - self._radius, width: 2 * self._radius, height: 2 * self._radius)
        let bezierPath = UIBezierPath(ovalIn: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self._radius * 2, height: self._radius * 2)))
        draw(path: bezierPath)
    }
    
    func draw(path: UIBezierPath) {
        self.layer.sublayers?.each { $0.removeFromSuperlayer() }
        let layer = CAShapeLayer(layer: self.layer)
        layer.lineWidth = 3.0
        layer.fillColor = self.color.cgColor
        layer.path = path.cgPath
        self.layer.addSublayer(layer)
        if isGrow {
            grow(isGrow: true)
        }
    }
    
    func grow(isGrow: Bool) {
        if isGrow {
            grow(self.growColor, radius: self._radius, shininess: 1.6)
        } else {
            self.layer.shadowRadius = 0
            self.layer.shadowOpacity = 0
        }
    }
    
    func circlePoint(rad: CGFloat) -> CGPoint {
        return CGMath.circlePoint(center, radius: self._radius, rad: rad)
    }
    
}

extension UIView {
    public func grow(_ baseColor: UIColor, radius: CGFloat, shininess: CGFloat) {
        guard let sublayers = layer.sublayers as? [CAShapeLayer]  else { return }
        
        let growColor = baseColor
        growShadow(radius, growColor: growColor, shininess: shininess)
        let circle = CAShapeLayer()
        circle.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: radius * 2.0, height: radius * 2.0)).cgPath
        let circleGradient = CircularGradientLayer(colors: [growColor, UIColor(white: 1.0, alpha: 0)])
        circleGradient.frame = CGRect(x: 0, y: 0, width: radius * 2.0, height: radius * 2.0)
        circleGradient.opacity = 0.25
        for sub in sublayers {
            sub.fillColor = UIColor.clear.cgColor
        }
        circleGradient.mask = circle
        layer.addSublayer(circleGradient)
    }
    
    public func growShadow(_ radius: CGFloat, growColor: UIColor, shininess: CGFloat) {
        let origin = self.center.minus(self.frame.origin).minus(CGPoint(x: radius * shininess, y: radius * shininess))
        let ovalRect = CGRect(origin: origin, size: CGSize(width: 2 * radius * shininess, height: 2 * radius * shininess))
        let shadowPath = UIBezierPath(ovalIn: ovalRect)
        self.layer.shadowColor = growColor.cgColor
        self.layer.shadowRadius = radius
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.shadowOpacity = 1.0
        self.layer.shouldRasterize = true
        self.layer.shadowOffset = CGSize.zero
        self.layer.masksToBounds = false
        self.clipsToBounds = false
    }
}
