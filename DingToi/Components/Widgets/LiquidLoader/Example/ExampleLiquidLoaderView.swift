//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

class ExampleLiquidLoaderView: UIView {
    
    func addLiquidLoader() {
        let lineColor = UIColor(red: 77 / 255.0, green: 255 / 255.0, blue: 182 / 255.0, alpha: 1.0)
        let growColor = UIColor.red
        
        let lineFrame = CGRect(x: self.frame.width * 0.5 - 100, y: 100, width: 200, height: 100)
        let lineLoader = LiquidLoader(frame: lineFrame, effect: .growLine(lineColor,7,10.0, growColor))
        
        let circleFrame = CGRect(x: self.frame.width * 0.5 - 100, y: 200, width: 200, height: 200)
        let circleColor = UIColor(red: 77 / 255.0, green: 182 / 255.0, blue: 255 / 255.0, alpha: 1.0)
        let circleLoader = LiquidLoader(frame: circleFrame, effect: .growCircle(circleColor,10,1.0, growColor))
        
        let circleMatColor = UIColor(red: 255 / 255.0, green: 188 / 255.0, blue: 188 / 255.0, alpha: 1.0)
        let circleMatFrame = CGRect(x: self.frame.width * 0.5 - 25, y: 450, width: 50, height: 50)
        let circleMat = LiquidLoader(frame: circleMatFrame, effect: .circle(circleMatColor,8,5.0, growColor))
        
        let lineMatColor = UIColor(red: 255 / 255.0, green: 255 / 255.0, blue: 188 / 255.0, alpha: 1.0)
        let lineMatFrame = CGRect(x: self.frame.width * 0.5 - 25, y: 500, width: 50, height: 50)
        let lineMat = LiquidLoader(frame: lineMatFrame, effect: .line(lineMatColor,4,1.0, growColor))
        
        self.addSubview(lineLoader)
        self.addSubview(circleLoader)
        self.addSubview(circleMat)
        self.addSubview(lineMat)
    }
}
