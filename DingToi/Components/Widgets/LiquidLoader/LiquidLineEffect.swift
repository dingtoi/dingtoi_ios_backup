//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import UIKit

class LiquidLineEffect : LiquidLoadEffect {
    
    var circleInter: CGFloat!
    
    override func setupShape() -> [LiquittableCircle] {
        return Array(0..<numberOfCircles).map { i in
            let x: CGFloat = self.circleInter + self.circleRadius + (CGFloat(i) * (self.circleInter + (self.circleRadius * CGFloat(2))))
            let y: CGFloat = self.loader.frame.height * 0.5
            return LiquittableCircle(
                center: CGPoint.init(x: x, y: y),
                radius: self.circleRadius,
                color: self.color,
                growColor: self.growColor
            )
        }
        
        //var list: [LiquittableCircle]  = []
        //for i in 0 ..< numberOfCircles {
        //    let x: CGFloat = (self.circleInter + self.circleRadius + (CGFloat(i) * (self.circleInter + (self.circleRadius * CGFloat(2)))))
        //    let y: CGFloat = self.loader.frame.height * 0.5
        //    let point = CGPoint.init(x: x, y: y)
        //    let item = LiquittableCircle.init(center: point, radius: self.circleRadius, color: self.color, growColor: self.growColor)
        //    list.append(item)
        //}
        //return list
    }
    
    override func movePosition(_ key: CGFloat) -> CGPoint {
        if loader != nil {
            return CGPoint(
                x:  (circles.last!.frame.rightBottom.x + circleInter)  * sineTransform(key),
                y: loader.frame.height * 0.5
            )
        } else {
            return CGPoint.zero
        }
    }
    
    func sineTransform(_ key: CGFloat) -> CGFloat {
        return sin(key * CGFloat(Double.pi)) * 0.5 + 0.5
    }
    
    override func update() {
        switch key {
        case 0.0...2.0:
            key += 2.0/(duration*60)
        default:
            key = 0.0
        }
    }
    
    override func willSetup() {
        if circleRadius == nil {
            circleRadius = loader.frame.width * 0.05
        }
        self.circleInter = (loader.frame.width - 2 * circleRadius * CGFloat(numberOfCircles)) / CGFloat(numberOfCircles + 1)
        self.engine = SimpleCircleLiquidEngine(radiusThresh: self.circleRadius, angleThresh: 0.2)
        let moveCircleRadius = circleRadius * moveScale
        self.moveCircle = LiquittableCircle(center: CGPoint(x: 0, y: loader.frame.height * 0.5), radius: moveCircleRadius, color: color, growColor: growColor)
    }
    
    override func resize() {
        circles.map { circle in
            return (circle, circle.center.minus(self.moveCircle!.center).length())
            }.each { (circle, distance) in
                let normalized = 1.0 - distance / (self.circleRadius + self.circleInter)
                if normalized > 0.0 {
                    circle._radius = self.circleRadius + (self.circleRadius * self.circleScale - self.circleRadius) * normalized
                } else {
                    circle._radius = self.circleRadius
                }
        }
    }
    
}
