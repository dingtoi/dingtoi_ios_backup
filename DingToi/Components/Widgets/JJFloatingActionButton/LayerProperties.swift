//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public extension JJFloatingActionButton {
    /// The shadow color of the floating action button.
    /// Default is `UIColor.black`.
    ///
    @objc @IBInspectable dynamic var shadowColor: UIColor? {
        get {
            guard let cgColor = layer.shadowColor else {
                return nil
            }
            return UIColor(cgColor: cgColor)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    /// The shadow offset of the floating action button.
    /// Default is `CGSize(width: 0, height: 1)`.
    ///
    @objc @IBInspectable dynamic var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    /// The shadow opacity of the floating action button.
    /// Default is `0.4`.
    ///
    @objc @IBInspectable dynamic var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    /// The shadow radius of the floating action button.
    /// Default is `2`.
    ///
    @objc @IBInspectable dynamic var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
}

extension JJActionItem {
    /// The shadow color of the action item.
    /// Default is `UIColor.black`.
    ///
    @objc @IBInspectable dynamic var shadowColor: UIColor? {
        get {
            guard let cgColor = layer.shadowColor else {
                return nil
            }
            return UIColor(cgColor: cgColor)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    /// The shadow offset of the action item.
    /// Default is `CGSize(width: 0, height: 1)`.
    ///
    @objc @IBInspectable dynamic var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    /// The shadow opacity of the action item.
    /// Default is `0.4`.
    ///
    @objc @IBInspectable dynamic var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    /// The shadow radius of the action item.
    /// Default is `2`.
    ///
    @objc @IBInspectable dynamic var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
}

