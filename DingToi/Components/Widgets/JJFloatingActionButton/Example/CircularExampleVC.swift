//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

internal class CircularExampleViewController: UIViewController {
    fileprivate let actionButton = JJFloatingActionButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //JJItemAnimationConfiguration.angleStartIndexRadius = 0
        //JJItemAnimationConfiguration.angleEndIndexRadius = 0.3 * .pi
        JJFloatingActionButton.COLOR_BG_ITEM = .clear
        actionButton.buttonColor = UIColor.rgb(fromHexString: "#00A2DF")
        actionButton.buttonImageColor = UIColor.rgb(fromHexString: "#00A2DF")
        
        actionButton.itemAnimationConfiguration = .circularSlideIn(withRadius: 120)
        actionButton.buttonAnimationConfiguration = .rotation(toAngle: .pi * 3 / 4)
        actionButton.buttonAnimationConfiguration.opening.duration = 0.8
        actionButton.buttonAnimationConfiguration.closing.duration = 0.6
        
        actionButton.addItem(image: #imageLiteral(resourceName: "Baloon")) { item in
            Helper.showAlert(for: item)
        }
        
        actionButton.addItem(image: #imageLiteral(resourceName: "Like")) { item in
            Helper.showAlert(for: item)
        }
        
        actionButton.addItem(image: #imageLiteral(resourceName: "Owl")) { item in
            Helper.showAlert(for: item)
        }
        
        actionButton.addItem(image: #imageLiteral(resourceName: "Favourite")) { item in
            Helper.showAlert(for: item)
        }
        
        actionButton.display(inViewController: self)
    }
}
