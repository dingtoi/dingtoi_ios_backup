//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

internal class BasicsExampleViewController: UIViewController {
    fileprivate let actionButton = JJFloatingActionButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //JJItemAnimationConfiguration.angleStartIndexRadius = 0
        //JJItemAnimationConfiguration.angleEndIndexRadius = 0.3 * .pi
        
        actionButton.addItem(title: "Heart", image: #imageLiteral(resourceName: "add_black.png")) { item in
            Helper.showAlert(for: item)
        }
        
        actionButton.addItem(title: "Like", image: #imageLiteral(resourceName: "Like")) { item in
            Helper.showAlert(for: item)
        }
        
        actionButton.addItem(title: "Balloon", image: #imageLiteral(resourceName: "Baloon")) { item in
            Helper.showAlert(for: item)
        }
        
        actionButton.display(inViewController: self)
    }
}
