//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation

/// Button state.
///
@objc public enum JJFloatingActionButtonState: Int {
    /// No items are visible
    ///
    case closed
    
    /// Items are fully visible
    ///
    case open
    
    /// During opening animation
    ///
    case opening
    
    /// During closing animation
    ///
    case closing
}

