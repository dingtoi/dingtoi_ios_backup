//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation

/// Floating action button delegate protocol
///
@objc public protocol JJFloatingActionButtonDelegate {
    /// Is called before opening animation. Button state is .opening.
    ///
    @objc optional func floatingActionButtonWillOpen(_ button: JJFloatingActionButton)
    
    /// Is called after opening animation. Button state is .opened.
    ///
    @objc optional func floatingActionButtonDidOpen(_ button: JJFloatingActionButton)
    
    /// Is called before closing animation. Button state is .closing.
    ///
    @objc optional func floatingActionButtonWillClose(_ button: JJFloatingActionButton)
    
    /// Is called after closing animation. Button state is .closed.
    ///
    @objc optional func floatingActionButtonDidClose(_ button: JJFloatingActionButton)
}
