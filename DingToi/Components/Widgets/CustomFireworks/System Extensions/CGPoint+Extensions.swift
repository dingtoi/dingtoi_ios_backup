//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

extension CGPoint {

    public mutating func add(vector: CGVector) {
        self.x += vector.dx
        self.y += vector.dy
    }

    public func adding(vector: CGVector) -> CGPoint {
        var copy = self
        copy.add(vector: vector)
        return copy
    }

    public mutating func multiply(by value: CGFloat) {
        self.x *= value
        self.y *= value
    }
}
