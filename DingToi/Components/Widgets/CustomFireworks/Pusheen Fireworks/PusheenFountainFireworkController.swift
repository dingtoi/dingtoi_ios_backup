//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public final class PusheenFountainFireworkController: FountainFireworkController {
    public override func createFirework(at origin: CGPoint, sparkSize: CGSize, scale: CGFloat) -> Firework {
        return PusheenFountainFirework(origin: origin, sparkSize: sparkSize, scale: scale)
    }
}
