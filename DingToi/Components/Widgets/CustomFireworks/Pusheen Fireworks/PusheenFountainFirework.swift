//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public final class PusheenFountainFirework: FountainFirework {
    public override var sparkViewFactory: SparkViewFactory {
        return PusheenSparkViewFactory()
    }
}
