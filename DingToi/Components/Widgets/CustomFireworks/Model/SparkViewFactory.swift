//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public protocol SparkViewFactoryData {
    var size: CGSize { get }
    var index: Int { get }
}

public protocol SparkViewFactory {
    func create(with data: SparkViewFactoryData) -> SparkView
}

public struct DefaultSparkViewFactoryData: SparkViewFactoryData {
    public let size: CGSize
    public let index: Int
}
