//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public final class DuckSparkViewFactory: SparkViewFactory {
    public func create(with data: SparkViewFactoryData) -> SparkView {
        guard let image = UIImage(named: "duck") else {
            fatalError("Couldn't find a duck!")
        }
        return ImageSparkView(image: image, size: data.size)
    }
}
