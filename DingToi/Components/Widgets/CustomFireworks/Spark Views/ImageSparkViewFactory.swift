//
//  MyProject
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public struct ImageSparkViewFactoryData: SparkViewFactoryData {
    public let image: UIImage
    public let size: CGSize
    public let index: Int
}

public struct ImageSparkViewFactory: SparkViewFactory {
    public func create(with data: SparkViewFactoryData) -> SparkView {
        guard let data = data as? ImageSparkViewFactoryData else {
            fatalError("Wrong data.")
        }
        return ImageSparkView(image: data.image, size: data.size)
    }
}
