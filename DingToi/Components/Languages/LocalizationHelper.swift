//  Created by boys vip on 12/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation

public let LangEN = "en"
public let LangVI = "vi"

public class LocalizationHelper {
    
    public  var enBundle: Bundle?
    public  var viBundle: Bundle?
    
    public init() {
        if let enBundlePath = Bundle.main.path(forResource: LangEN, ofType: "lproj") {
            enBundle = Bundle(path: enBundlePath)
        }
        if let jaBundlePath = Bundle.main.path(forResource: LangVI, ofType: "lproj") {
            viBundle = Bundle(path: jaBundlePath)
        }
    }
    
    public static let shared = LocalizationHelper()
    
    public func localized(_ key: String?) -> String? {
        guard let key = key else {
            return nil
        }
        var bundle: Bundle?
        switch Preferences.shared.currentLocale() {
        case LangEN:
            bundle = enBundle
        case LangVI:
            bundle = viBundle
        default:
            bundle = enBundle
        }
        return NSLocalizedString(key, tableName: nil, bundle: bundle!, value: key, comment: key)
    }
    
    public func en(_ key: String) -> String {
        if let enBundle = self.enBundle {
            return NSLocalizedString(key, tableName: nil, bundle: enBundle, value: key, comment: key)
        }
        return ""
    }
    
    public func vi(_ key: String) -> String {
        if let viBundle = self.viBundle {
            return NSLocalizedString(key, tableName: nil, bundle: viBundle, value: key, comment: key)
        }
        return ""
    }
    
    public func localized(_ key: String, _ locale: String) -> String {
        var bundle: Bundle?
        switch locale {
        case LangEN:
            bundle = enBundle
        case LangVI:
            bundle = viBundle
        default:
            bundle = viBundle //Default if other language
        }
        return NSLocalizedString(key, tableName: nil, bundle: bundle!, value: key, comment: key)
    }
    
}
