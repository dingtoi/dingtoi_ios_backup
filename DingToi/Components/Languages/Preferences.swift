//  Created by boys vip on 12/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation

public class Preferences {
    public static let kCurrentLocale = "CurrentLocale"
    public static let kDefaultLocale = LangVI
    
    public static let shared = Preferences()
    
    public func currentLocale() -> String {
        if let locale = UserDefaults.standard.value(forKey: Preferences.kCurrentLocale) {
            return locale as! String
        }
        return Preferences.kDefaultLocale
    }
    
    public func setCurrentLocale(_ locale: String) {
        UserDefaults.standard.set(locale, forKey: Preferences.kCurrentLocale)
        UserDefaults.standard.synchronize()
    }
    
}
