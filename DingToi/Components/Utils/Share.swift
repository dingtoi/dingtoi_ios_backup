//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import UIKit

public class Share {
    public static var isAgain = false
    public static var transactionCode: String?
    public static let checkRequest = false
    public static let _colorMenu = "#FFFFFF" //"#004C8E"
    
    #if DEBUG
    public static let onOfflineMode = false
    #else
    public static let onOfflineMode = true //default = true, false check remoteconfig for dev success
    #endif
    
    
    private static var _isLogin: Bool? = true
    public static var isLogin: Bool? {
        get{
            return self._isLogin
        }
        set(newValue){
            self._isLogin = newValue
        }
    }
    
    var baseVC: BaseViewController?
//    typealias Action = (Array<SpinerModel>) -> (Void)
//    var completion: Action?
//    func getOMNIListSearch(searchString: String, baseVC: BaseViewController?,_ completion: (@escaping (Array<SpinerModel>) -> (Void))  ) {
//        self.baseVC = baseVC
//        self.completion = completion
//
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let loadingView: LoadingView = LoadingView(frame: CGRect(origin: .zero, size: CGSize.init(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)))
//        loadingView.tag = 888888
//        appDelegate.window!.addSubview(loadingView)
//
//        //Call API
//        if Share.checkRequest {
//            //APIManager().OfficesSearch(request: OfficeSearchRequestModel(address: searchString), self)
//        }else {
//            var result = Array<SpinerModel>.init()
//            result.append(SpinerModel(id: "1", content: "Test 01", data: nil))
//            result.append(SpinerModel(id: "2", content: "Test 02", data: nil))
//            result.append(SpinerModel(id: "3", content: "Test 03", data: nil))
//            OnSuccess_OMNIListSearch(result: result)
//        }
//    }
    
//    public func OnSuccess_OMNIListSearch(result: Array<SpinerModel>) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window!.viewWithTag(888888)?.removeFromSuperview()
//
//        completion!(result)
//    }
    
    public func OnError_OMNIListSearch(error: ResponseErrorModel) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.viewWithTag(888888)?.removeFromSuperview()
        
        self.baseVC?.showDialog(String.kErrorMessageTitle, "ERROR OMNIListSearch", rootVC: appDelegate.window?.rootViewController)
    }
}
