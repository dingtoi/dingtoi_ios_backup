//
//  FingerprintStatus.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/30/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import LocalAuthentication

class FingerprintStatus: NSObject {
    public static let shared = FingerprintStatus()
    
    func status() -> Bool {
        return LocalAuthManager.shared.biometricType != .none
    }
}

class LocalAuthManager: NSObject {

    public static let shared = LocalAuthManager()
    private let context = LAContext()
    private let reason = "Your Request Message"
    private var error: NSError?

    public enum BiometricType: String {
        case none
        case touchID
        case faceID
    }

    private override init() {

    }

    // check type of local authentication device currently support
    var biometricType: BiometricType {
        guard self.context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            return .none
        }

        if #available(iOS 11.0, *) {
            switch context.biometryType {
            case .none:
                return .none
            case .touchID:
                return .touchID
            case .faceID:
                return .faceID
            }
        } else {
            return self.context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) ? .touchID : .none
        }
    }
}

