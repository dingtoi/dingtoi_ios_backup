//
//  BatteryInfo.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/31/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import UIKit

class BatteryInfo: NSObject {
    public static let shared = BatteryInfo()
    
    func batteryLevel() -> String {
        let device = UIDevice.current;
        device.isBatteryMonitoringEnabled = true
        var batteryLevel: Float = 0.0;
        let batteryCharge = device.batteryLevel
        if (batteryCharge > 0) {
            batteryLevel = batteryCharge * 100.0;
        }
        return "\(Int(batteryLevel)) %"
    }
}
