//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation
import UIKit
//self.classForCoder.description()

class StackView: UIView {
    private var top: Node? = Node()
    
    func push(key: UIView) -> UIView? {
        // Nếu phần tử top Stack rỗng --> Tạo phần tử đầu tiên trong Stack
        if top == nil {
            top = Node()
        }
        
        // Nếu key phần tử top Stack rỗng
        if top?.key ==  nil {
            top?.key = key
            return peek()
        } else { // Ngược lại nếu key phần tử top Stack không rỗng --> Thì tạo một nút mới và gắn key vào nút --> Liên kết với phần tử top Stack --> Gán nút mới tạo là top Stack
            let newNode = Node()
            newNode.key = key
            newNode.next = top
            top = newNode
            return peek()
        }
    }
    
    func pop() -> UIView? {
        // Nếu phần tử top Stack rỗng --> dừng
        guard top != nil else {
            return nil
        }
        
        // Nếu key phần tử top Stack rỗng --> dừng
        guard top?.key != nil else {
            return nil
        }
        
        // Nếu phần tử sau top Stack không rỗng --> Gán phần tử sau đó = top Stack
        if let nextNode = top?.next {
            top = nextNode
        } else {
            top = nil
        }
        
        return peek()
    }
    
    func peek() -> UIView? {
        // Kiểm tra xem danh sách có rỗng ko
        guard let topItem = top?.key else {
            // Nếu rỗng thì trả về nil
            return nil
        }
        
        // Nếu không rỗng thì trả về phần tử top Stack
        return topItem
    }
    
    func popEquals(view: UIView) -> UIView? {
        // Nếu phần tử top Stack rỗng --> dừng
        guard top != nil else {
            return nil
        }
        
        //Duyệt các phần tử trong Stack
        while top?.key != nil {
            if top?.key.classForCoder.description() == view.classForCoder.description() { // Nếu tồn tại --> trả về
                return top?.key
            }else{
                top = top?.next!
            }
        }
        return nil
    }
    
    func pushEquals(view: UIView) -> UIView? {
        // Nếu phần tử top Stack rỗng --> Tạo phần tử đầu tiên trong Stack
        if top == nil {
            top = Node()
        }
        
        // Nếu key phần tử top Stack rỗng
        if top?.key ==  nil {
            top?.key = view
            return peek()
        } else { // Ngược lại nếu key phần tử top Stack không rỗng
            //Duyệt các phần tử trong Stack
            while top != nil && top?.key != nil {
                if top?.key.classForCoder.description() == view.classForCoder.description() { // Nếu tồn tại --> trả về
                    return peek()
                }else{
                    top = top?.next
                }
            }
            // Ngược lại nếu không tồn tại phần tử trong Stack --> Tạo mới và thêm vào Stack
            let newNode = Node()
            newNode.key = view
            newNode.next = top
            top = newNode
            return peek()
        }
    }
    
    func pushUIView(view: UIView){
        for item in self.subviews {
            item.removeFromSuperview()
        }
        
        if let temp = self.push(key: view) {
            self.addSubview(temp)
        }
    }
    
    func popUIView() {
        if let temp = self.pop() {
            for item in self.subviews {
                item.removeFromSuperview()
            }
            self.addSubview(temp)
        }
    }
    
}
