//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import Foundation
import DeviceKit

public struct Constants {
    public static let TAG_MENU_VIEW:Int = 111
}

public struct Colors {
    public static let mainBackground = UIColor.colorFromRGB(red: 254/255.0, green: 255/255.0, blue: 255/255.0)
    public static let borderColor = UIColor.colorFromRGB(red: 200/255.0, green: 200/255.0, blue: 200/255.0)
    public static let tabbarBarBackground = UIColor.colorFromRGB(red: 4/255, green: 48/255, blue: 107/255)
    public static let topTabbarBarBackground = UIColor.colorFromRGB(red: 1/255, green: 29/255, blue: 66/255)
    public static let navigationBarBackground = UIColor.colorFromRGB(red: 1/255, green: 29/255, blue: 66/255)
    public static let mainOrangeButtonBackground = UIColor.colorFromRGB(red: 246/255, green: 166/255, blue: 50/255)
    public static let mainOrangeTextColor = UIColor.colorFromRGB(red: 226/255, green: 152/255, blue: 57/255)
    public static let orangeBorder = UIColor.colorFromRGB(red: 199/255, green: 124/255, blue: 0/255)
    public static let lineColor = UIColor.colorFromRGB(red: 8/255, green: 60/255, blue: 130/255)
    public static let homeCellBackdroundColor = UIColor.colorFromRGB(red: 2/255, green: 40/255, blue: 80/255)
    public static let seperatorLineColor = UIColor.colorFromRGB(red: 17/255, green: 58/255, blue: 100/255)
    public static let seperatorCellColor = UIColor.colorFromRGB(red: 195/255, green: 196/255, blue: 197/255)
    public static let alertBackground = UIColor.colorFromRGB(red: 241/255, green: 242/255, blue: 243/255)
}

public struct Fonts {
    public static var FONT_TEXT_IN_LB_AND_TXT_OF_INFOR: CGFloat {
        return UIDevice().autoSize(12, 14, 16, 16)
    }
    
    public static var FONT_TEXT_CELL_MAP: CGFloat {
        return UIDevice().autoSize(14, 16, 18, 18)
    }
}

public struct FunctionName {
//    case FrontCamera = 0
//    case RearCamera = 1
//    case FrontFlash = 3
//    case RearFlash = 4
//    case DiskStorage = 5
//    case FreeDiskStorage = 6
//    case Bluetooth = 7
//    case Wifi = 8
//    case Fingerprinter = 9
//    case ReceiveCall = 10
//    case CallOut = 11
//    case SendAndReceiveSMS = 12
    
    // Section 0
    public static var HARDWARE = 0
    public static var CAMERA = 1
    public static var VOLUME = 2
    
    // Section 1
    public static var WIFI = 0
    public static var BLUETOOTH = 1
    public static var FINGERPRINTER = 2
    
    // Section 2
    public static var VOICE_CALL = 0
    public static var TEXTING = 1
    public static var DATA_TRANSFER = 2
    public static var SPECIAL_APP = 3
}
