//
//  CameraStatus.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/29/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import UIKit

class CameraStatus {
    class func camera(_ location: UIImagePickerControllerCameraDevice) -> Bool {
        return UIImagePickerController.isCameraDeviceAvailable(location)
    }
    
    class func flash(_ location: UIImagePickerControllerCameraDevice) -> Bool {
        return UIImagePickerController.isFlashAvailable(for: location)
    }
}
