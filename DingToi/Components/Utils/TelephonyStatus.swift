//
//  CallsStatus.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/31/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import CoreTelephony
import UIKit
import MessageUI

class TelephonyStatus {
    public static let shared = TelephonyStatus()
    
    class func callStatus() -> Bool {
        let isCapableToCall: Bool
        if UIApplication.shared.canOpenURL(NSURL(string: "tel://")! as URL) {
            // Check if iOS Device supports phone calls
            // User will get an alert error when they will try to make a phone call in airplane mode
            if let mnc = CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode, !mnc.isEmpty {
                // iOS Device is capable for making calls
                isCapableToCall = true
            } else {
                // Device cannot place a call at this time. SIM might be removed
                isCapableToCall = false
            }
        } else {
            // iOS Device is not capable for making calls
            isCapableToCall = false
        }
        
        return isCapableToCall
    }
    
    class func smsStatus() -> Bool {
        return MFMessageComposeViewController.canSendText()
    }
    
    class func dataSendStatus() -> Bool {
        return MFMessageComposeViewController.canSendAttachments()
    }
}
