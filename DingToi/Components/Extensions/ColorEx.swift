//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import UIKit

extension UIColor {
    
    public static func colorFromHEX(_ hex: NSInteger) -> UIColor {
        return UIColor(red:((CGFloat)((hex & 0xFF0000) >> 16))/255.0, green:((CGFloat)((hex & 0xFF00) >> 8))/255.0, blue:((CGFloat)(hex & 0xFF))/255.0, alpha:1)
    }
    
    public static func colorFromRGB(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red, green: green, blue: blue, alpha: CGFloat(1.0))
    }
}
