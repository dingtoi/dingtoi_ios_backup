//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import Foundation
import DeviceKit

public class FontSizes {
    public static var FONT_TEXT: CGFloat {
        return UIDevice().autoSize(12, 14, 16, 16)
    }
    
    public static var FONT_HTML: CGFloat {
        return UIDevice().autoSize(12, 13, 14, 15)
    }
    
    public static var FONT_HTML_MENU: CGFloat {
        return UIDevice().autoSize(12, 13, 14, 15)
    }
    
    public static var FONT_TABBAR_TITLE: CGFloat {
        return UIDevice().autoSize(12, 12, 14, 14)
    }
    
    public static var FONT_TEXT_IN_LB_AND_TXT_OF_INFOR: CGFloat {
        return UIDevice().autoSize(12, 14, 16, 16)
    }
    
    static var FONT_MENU_CELL_TITLE: CGFloat {
        return UIDevice().autoSize(15, 17, 22, 22)
    }
}

public class Sizes {
    public static var SIZE_TABBAR_HEIGHT:CGFloat {
        return UIDevice().autoSize(50, 57, 62, 94) //iPhoneX=89
    }
    
    public static var SIZE_ICON_TABBAR:CGFloat {
        return UIDevice().autoSize(24, 30, 34, 34)
    }
    
    public static var SIZE_ICON_TABBAR_SMALL:CGFloat {
        return UIDevice().autoSize(10, 12, 15, 15)
    }
    
    public static var SIZE_ICON_TABBAR_BIG:CGFloat {
        return UIDevice().autoSize(20, 28, 32, 32)
    }
    
    public static var SIZE_ICON_MENU_MAP: CGFloat {
        return UIDevice().autoSize(12, 14, 16, 16)
    }
    
    public static var SIZE_INDICATOR_POS: CGFloat {
        return UIDevice().autoSize(40, 45, 50, 50)
    }
    
    static var SIZE_HEIGHT_CELL_MAP:CGFloat {
        return UIDevice().autoSize(70, 85, 100, 100)
    }
}
