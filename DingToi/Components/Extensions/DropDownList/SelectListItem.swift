//
//  SelectListItem.swift
//  HoozingProject
//
//  Created by boys vip on 7/21/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation
import HandyJSON

public class SelectListItem: HandyJSON {
    var Disabled: Bool?
    var Group: SelectListGroup?
    var Selected: Bool?
    var Text: String?
    var Value: String?
    var Description: String?
    
    public init(Disabled: Bool?, Group: SelectListGroup?, Selected: Bool?, Text: String?, Value: String?, Description: String?) {
        self.Disabled = Disabled
        self.Group = Group
        self.Selected = Selected
        self.Text = Text
        self.Value = Value
        self.Description = Description
    }
    public required init() {}
}
