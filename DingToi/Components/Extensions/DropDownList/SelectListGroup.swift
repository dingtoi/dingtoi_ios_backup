//
//  SelectListGroup.swift
//  HoozingProject
//
//  Created by boys vip on 7/21/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation
import HandyJSON

public class SelectListGroup: HandyJSON {
    var Disabled: Bool?
    var Name: String?
    
    public init(Disabled: Bool?, Name: String?) {
        self.Disabled = Disabled
        self.Name = Name
    }
    public required init() {}
}
