//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import UIKit

extension String {
    public static let REMEMBER_USERNAME = "REMEMBER_USERNAME"
    public static let REMEMBER_NAME = "REMEMBER_NAME"
    public static let kMaxFileSize: String = "Only select file with size <= 5Mb"
    public static var kErrorMessageTitle = "ERROR"
    public static let kErrorMessageTimeout = "The session is over. Please log in again !"
    public static let kNextStepTitle = "MESSAGE"
    public static let kPhoneFormat:String = "Phone number invalidate !"
    public static let kEmailFormat: String = "Email invalidate!"
    
    public func convertFromHtml_1() -> NSAttributedString {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,
                                                                .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
    
    public func convertFromHtml_2() -> NSAttributedString {
        let defaultFont = "Roboto-Light"
        let defaultSize = FontSizes.FONT_HTML
        let paragraphFont = "Roboto-Regular"
        let paragraphSize = FontSizes.FONT_HTML_MENU
        
        let htmlStyle = "<style>p {font-family:\(paragraphFont); font-size:\(paragraphSize)px; color='#555555'; background-color: transparent; line-height: 1.3;} body {font-family:\(defaultFont); font-size:\(defaultSize)px;}}</style>"
        
        let attrStrStyle1 = "\(htmlStyle)<body align='center'><p align='center'>\(self)</p>"
        
        guard let data = attrStrStyle1.data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,
                                                                .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
    
    public func convertFromHtml_2(_ defaultFont: String = "Roboto-Light",_ paragraphFont: String = "Roboto-Regular") -> NSAttributedString {
        let defaultFont = defaultFont
        let defaultSize = FontSizes.FONT_HTML
        let paragraphFont = paragraphFont
        let paragraphSize = FontSizes.FONT_HTML_MENU
        
        let htmlStyle = "<style>p {font-family:\(paragraphFont); font-size:\(paragraphSize)px; color='#555555'; background-color: transparent; line-height: 0.5;} body {font-family:\(defaultFont); font-size:\(defaultSize)px;}}</style>"
        
        let attrStrStyle1 = "\(htmlStyle)<body valign='top'><p align='center'>\(self)</p>"
        
        guard let data = attrStrStyle1.data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,
                                                                .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
    
    public func substring(location: Int, length: Int) -> String? {
        guard location >= 0 && length >= 0 && (self.count >= location + length) else { return nil }
        let start = index(startIndex, offsetBy: location)
        let end = index(startIndex, offsetBy: location + length)
        return String(self[start..<end]) //substring(with: start..<end)
    }
    
    public func index(of target: String) -> Int? {
        if let range = self.range(of: target) {
            return distance(from: startIndex, to: range.lowerBound)
        } else {
            return nil
        }
    }
    
    public func lastIndex(of target: String) -> Int? {
        if let range = self.range(of: target, options: .backwards) {
            return distance(from: startIndex, to: range.lowerBound)
        } else {
            return nil
        }
    }
    
    public func indicesOf(string: String) -> [Int] {
        var indices = [Int]()
        var searchStartIndex = self.startIndex
        
        while searchStartIndex < self.endIndex,
            let range = self.range(of: string, range: searchStartIndex..<self.endIndex),
            !range.isEmpty
        {
            let index = distance(from: self.startIndex, to: range.lowerBound)
            indices.append(index)
            searchStartIndex = range.upperBound
        }
        
        return indices
    }
    
    public func size(_ font: UIFont) -> CGSize {
        let fontAttribute = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttribute)
        return size;
    }
    
    public func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    public func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    public func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedStringKey.font: font]
        return self.size(withAttributes: fontAttributes)
    }
    
    public func utf8DecodedString()-> String {
        let data = self.data(using: .utf8)
        if let message = String(data: data!, encoding: .nonLossyASCII){
            return message
        }
        return ""
    }
    
    public func utf8EncodedString()-> String {
        let messageData = self.data(using: .nonLossyASCII)
        let text = String(data: messageData!, encoding: .utf8)
        return text!
    }
    
    public func base64ToImage() -> UIImage? {
        if let url = URL(string: self),let data = try? Data(contentsOf: url),let image = UIImage(data: data) {
            return image
        }
        return nil
    }
    
    public var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
    
    public func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
    public func toInt() -> Int? {
        return NumberFormatter().number(from: self)?.intValue
    }
    
    public func toInt64() -> Int64? {
        return NumberFormatter().number(from: self)?.int64Value
    }
    
    
    //Check value String
    public var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    public var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
        } catch {
            return false
        }
    }
    
    public var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    public var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && (res.range.length == 10 || res.range.length == 11)
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    public var isPhoneTableNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && (res.range.length == 7 || res.range.length == 8)
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    public var isCardID: Bool {
        if self.count >= 9 && self.count <= 12  {
            return true
        }
        return false
    }
    
    public var isCharSpecial : Bool {//Kiểm tra chuổi có chứa các ký tự đặc biệt
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, self.count)) != nil {
            print("could not handle special characters")
            return true
        }else{
            return false
        }
    }
    
    public var isUsername: Bool {//Tên đăng nhập không chứa các ký tự đặc biệt và ký tự có dấu
        if self.isCharSpecial || ConverHelper.checkVN(self) {
            return false
        }
        return true
    }
    
    public var isPassword: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
            if(regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil) {
                return true
            }
        } catch {
            return false
        }
        return false
    }
    
    public var isPassword_1: Bool { //Mật khẩu không chứa ký tự có dấu
        if ConverHelper.checkVN(self) {
            return false
        }
        return true
    }
    
    //Mật khẩu dài ít nhất tám ký tự, một ký tự đặc biệt, một chữ hoa, một chữ thường và một chữ số
    public var isPassword_2: Bool {
        if(self.count>=8 && self.count<=20){
        }else{
            return false
        }
        let nonUpperCase = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
        let letters = self.components(separatedBy: nonUpperCase)
        let strUpper: String = letters.joined()
        
        let smallLetterRegEx  = ".*[a-z]+.*"
        let samlltest = NSPredicate(format:"SELF MATCHES %@", smallLetterRegEx)
        let smallresult = samlltest.evaluate(with: self)
        
        let numberRegEx  = ".*[0-9]+.*"
        let numbertest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = numbertest.evaluate(with: self)
        
        //let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        //var isSpecial :Bool = false
        //if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, self.count)) != nil {
        //    print("could not handle special characters")
        //    isSpecial = true
        //}else{
        //    isSpecial = false
        //}
        return (strUpper.count >= 1) && smallresult && numberresult // && isSpecial
    }
    
    public func isURLGifImage() -> Bool {
        let imageFormats = ["gif"]
        if let ext = self.getExtension() {
            return imageFormats.contains(ext)
        }
        return false
    }
    
    public func getExtension() -> String? {
        let ext = (self as NSString).pathExtension
        if ext.isEmpty {
            return nil
        }
        return ext
    }
    //Check value String ./
    
    
    // 2018-02-28T00:14:56.4348949+08:00
    // yyyy-MM-dd   >>>  e.g.:  1969-12-31
    // yyyy-MM-dd   >>>  e.g.:  1970-01-01
    // yyyy-MM-dd HH:mm     >>>  e.g.:   1969-12-31 16:00
    // yyyy-MM-dd HH:mm     >>>  e.g.:   1970-01-01 00:00
    // yyyy-MM-dd HH:mmZ    >>>  e.g.:   1969-12-31 16:00-0800
    // yyyy-MM-dd HH:mmZ    >>>  e.g.:   1970-01-01 00:00+0000
    // yyyy-MM-dd HH:mm:ss.SSSZ     >>>  e.g.:   1969-12-31 16:00:00.000-0800
    // yyyy-MM-dd HH:mm:ss.SSSZ     >>>  e.g.:   1970-01-01 00:00:00.000+0000
    // yyyy-MM-dd'T'HH:mm:ss.SSSZ   >>>  e.g.:   1969-12-31T16:00:00.000-0800
    // yyyy-MM-dd'T'HH:mm:ss.SSSZ   >>>  e.g.:   1970-01-01T00:00:00.000+0000
    // yyyy-MM-dd'T'HH:mm:ss.SSSXXX     >>>  e.g.: 2001-07-04T12:08:56.235-07:00
    public func stringToDate(_ format: String = "yyyy-MM-dd'T'HH:mm:ssZ") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format //"yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = NSTimeZone.local
        
        if let endDate = dateFormatter.date(from: self) {
            //print("Date Format: \(String(describing: dateFormatter.dateFormat))")
            return endDate
        }else {
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss Z"
            if let endDate = dateFormatter.date(from: self) {
                //print("Date Format: \(String(describing: dateFormatter.dateFormat))")
                return endDate
            }else {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
                if let endDate = dateFormatter.date(from: self) {
                    //print("Date Format: \(String(describing: dateFormatter.dateFormat))")
                    return endDate
                }else {
                    print("Not convert date: \(self)")
                }
            }
        }
        return nil
    }
    
    public func stringToDateText(_ format: String = "dd/MM/yyyy") -> String {
        if let date = self.stringToDate() {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = format
            return dateFormatter.string(from: date)
        }
        return ""
    }
    
    public func stringToTimeText(_ format: String = "hh:mm a") -> String {
        if let date = self.stringToDate() {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = format
            return dateFormatter.string(from: date)
        }
        return ""
    }
    
    public func convertFormatNumber(_ value: Int64) -> String?{
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.groupingSeparator = ","
        if let formattedNumber: String =  numberFormatter.string(from: NSNumber(value:value)){
            return formattedNumber
        }else{
            return nil
        }
    }
}
