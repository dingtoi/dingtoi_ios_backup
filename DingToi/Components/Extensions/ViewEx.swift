//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import UIKit
import SnapKit
import KGModal

extension UIView {
    func colorOfPoint(point: CGPoint) -> UIColor {
        let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)

        var pixelData: [UInt8] = [0, 0, 0, 0]

        let context = CGContext(data: &pixelData, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)

        context!.translateBy(x: -point.x, y: -point.y)

        self.layer.render(in: context!)

        let red: CGFloat = CGFloat(pixelData[0]) / CGFloat(255.0)
        let green: CGFloat = CGFloat(pixelData[1]) / CGFloat(255.0)
        let blue: CGFloat = CGFloat(pixelData[2]) / CGFloat(255.0)
        let alpha: CGFloat = CGFloat(pixelData[3]) / CGFloat(255.0)

        let color: UIColor = UIColor(red: red, green: green, blue: blue, alpha: alpha)

        return color
    }
    
    @IBInspectable var borderColor: UIColor {
        set {
            layer.borderColor = newValue.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            } else {
                return UIColor.white
            }
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    func roundedBottom(size: CGFloat){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.bottomRight , .bottomLeft],
                                     cornerRadii: CGSize(width: size, height: size))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    
    public func setupDefaultBorder() {
        self.addRadius()
        self.addBorder()
    }
    
    public func addBorder(_ borderWidth: CGFloat = 0.5, borderColor: UIColor = Colors.borderColor) {
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
    }
    
    public func addCircleRadius(masksToBounds: Bool = true) {
        layer.cornerRadius = self.frame.height/2
        layer.masksToBounds = masksToBounds
    }
    
    public func addRadius(_ radius: CGFloat = 5, masksToBounds: Bool = true) {
        layer.cornerRadius = radius
        layer.masksToBounds = masksToBounds
    }
    
    public func configure(_ radius: CGFloat = 5, borderColor: UIColor = Colors.borderColor, borderWidth: CGFloat = 0.5) {
        addRadius(radius, masksToBounds: true)
        addBorder(borderWidth, borderColor: borderColor)
    }
    
    public func addViewShadowTop() {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: -0.5)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    public func showPopup() {
        //KGModal.sharedInstance().show(withContentView: self, andAnimated: true)
        //KGModal.sharedInstance().closeButtonType = .none
        //KGModal.sharedInstance().modalBackgroundColor = Constants.Colors.tabbarBarBackground
        //KGModal.sharedInstance().backgroundDisplayStyle = .solid
        
        // Attach to the top most window
        switch (UIApplication.shared.statusBarOrientation) {
        case .landscapeLeft:
            self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 270 / 180))
            
        case .landscapeRight:
            self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 90 / 180))
            
        case .portraitUpsideDown:
            self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 180 / 180))
            
        default:
            break
        }
        
        guard let topView = UIApplication.shared.windows.first else {
            return
        }
        self.tag = 2008
        topView.addSubview(self)
        
        self.snp.makeConstraints { (maker) in
            maker.width.equalTo(self.frame.width)
            maker.height.equalTo(self.frame.height)
            maker.centerX.centerY.equalTo(topView)
        }
        
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseInOut, animations: {
            self.layer.transform = CATransform3DMakeScale(1, 1, 1)
        }, completion: nil)
    }
    
    public func hidePopup() {
        guard let topView = UIApplication.shared.windows.first else {
            return
        }
        topView.viewWithTag(2008)?.removeFromSuperview()
    }
    
    public func loadXib(xibName:String) {
        if let view = Bundle.main.loadNibNamed(xibName, owner: self, options: nil)?.first as? UIView {
            
            self.addSubview(view)
            view.frame = CGRect.infinite
            
            view.snp.makeConstraints({ (maker) in
                maker.top.bottom.leading.trailing.equalTo(self)
            })
        }
    }
    
    public func constraintByIdentifier(identifier:String) -> NSLayoutConstraint? {
        for constraint in self.constraints {
            if constraint.identifier == identifier {
                return constraint
            }
        }
        return nil
    }
    
    public func animateNextView(_ duration: Double = 0.75) {
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = duration
        transition.subtype = kCATransitionFromRight
        self.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
    }
    
    public func animatePrevView(_ duration: Double = 0.75) {
        let transition = CATransition()
        transition.type = kCATransitionReveal
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = duration
        transition.subtype = kCATransitionFromLeft
        self.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
    }
}



// MARK: Image
extension UIView {
    
    public func getSnapshotImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let snapshotImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return snapshotImage
    }
    
    public func roundCornersWithLayerMask(cornerRadii: CGFloat, corners: UIRectCorner) {
        let path = UIBezierPath(roundedRect: bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: cornerRadii, height: cornerRadii))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }
    
    public func addCircleShadow(shadowRadius: CGFloat = 2,
                         shadowOpacity: Float = 1.0,
                         shadowColor: CGColor = UIColor.rgb(fromHexString: "#4c000000").cgColor,
                         shadowOffset: CGSize = CGSize.zero) {
        layer.cornerRadius = frame.size.height / 2
        layer.masksToBounds = false
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
    }
    
    public func addShadow(cornerRadius: CGFloat = 2.0,
                   shadowRadius: CGFloat = 2.0,
                   shadowOpacity: Float = 1.0,
                   shadowColor: CGColor = UIColor.rgb(fromHexString: "#4c000000").cgColor,
                   shadowOffset: CGSize = CGSize.zero) {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = false
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
    }
    
    public func removeDashedLine() {
        _ = layer.sublayers?.filter({ $0.name == "DashedTopLine" }).map({ $0.removeFromSuperlayer() })
    }
    
    public func addDashedLine(color: UIColor = UIColor.lightGray) {
        removeDashedLine()
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.name = "DashedTopLine"
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [2,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}

extension UIView {
    public func parentView<T: UIView>(of type: T.Type) -> T? {
        guard let view = self.superview else {
            return nil
        }
        return (view as? T) ?? view.parentView(of: T.self)
    }
}

extension UITableViewCell {
    //Recursively
    public var myTableView: UITableView? {
        return self.parentView(of: UITableView.self)
    }
    
    //Using loop
    public var parentTableView: UITableView? {
        var view = self.superview
        while (view != nil && view!.isKind(of: UITableView.self) == false) {
            view = view!.superview
        }
        return view as? UITableView
    }
}

extension UICollectionViewCell {
    //Recursively
    public var myCollectionView: UICollectionView? {
        return self.parentView(of: UICollectionView.self)
    }
    
    //Using loop
    public var parentCollectionView: UICollectionView? {
        var view = self.superview
        while (view != nil && view!.isKind(of: UICollectionView.self) == false) {
            view = view!.superview
        }
        return view as? UICollectionView
    }
}

extension CALayer {
    public func addBorder(edgeList: [UIRectEdge], color: UIColor, thickness: CGFloat) {
        for edge in edgeList {
            let border = CALayer()
            switch edge {
            case UIRectEdge.top:
                border.frame = CGRect.init(x: 0, y: 0, width: frame.width, height: thickness)
                break
            case UIRectEdge.bottom:
                border.frame = CGRect.init(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
                break
            case UIRectEdge.left:
                border.frame = CGRect.init(x: 0, y: 0, width: thickness, height: frame.height)
                break
            case UIRectEdge.right:
                border.frame = CGRect.init(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
                break
            default:
                break
            }
            border.backgroundColor = color.cgColor;
            self.addSublayer(border)
        }
    }
}

extension UIView {
    public func addTapGesture(_ target: Any?, _ action: Selector?) {
        let tapGesture = UITapGestureRecognizer(target: target, action: action)
        self.addGestureRecognizer(tapGesture)
        self.isUserInteractionEnabled = true
    }
}

extension UIView {
    
    func removeConstraints() {
        removeConstraints(constraints)
    }
    
    func deactivateAllConstraints() {
        NSLayoutConstraint.deactivate(getAllConstraints())
    }
    
    func getAllSubviews() -> [UIView] {
        return UIView.getAllSubviews(view: self)
    }
    
    func getAllConstraints() -> [NSLayoutConstraint] {
        
        var subviewsConstraints = getAllSubviews().flatMap { (view) -> [NSLayoutConstraint] in
            return view.constraints
        }
        
        if let superview = self.superview {
            subviewsConstraints += superview.constraints.compactMap{ (constraint) -> NSLayoutConstraint? in
                if let view = constraint.firstItem as? UIView {
                    if view == self {
                        return constraint
                    }
                }
                return nil
            }
        }
        
        return subviewsConstraints + constraints
    }
    
    class func getAllSubviews(view: UIView) -> [UIView] {
        return view.subviews.flatMap { subView -> [UIView] in
            return [subView] + getAllSubviews(view: subView)
        }
    }
}
