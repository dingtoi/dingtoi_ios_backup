//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import UIKit

extension Array {
    var toStringJSON : String? {
        do {
            let data = try JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted])
            return String(data: data, encoding: .utf8)
        } catch {
            return nil
        }
    }
    
    func getDisplayDropdowModel(_ value: String) -> String? {
        for item in self {
            if item is SelectListItem {
                if let temp = (item as! SelectListItem).Value, temp.capitalized == value.capitalized {
                    return (item as! SelectListItem).Text
                }
            }
        }
        return nil
    }
    
    func getValueDropdowModel(_ display: String) -> String? {
        for item in self {
            if item is SelectListItem {
                if let temp = (item as! SelectListItem).Text, temp.capitalized == display.capitalized {
                    return (item as! SelectListItem).Value
                }
            }
        }
        return nil
    }
    
    func getListValueDropdowModel() -> Array<Int> {
        var list = Array<Int>.init()
        for item in self {
            if item is SelectListItem {
                list.append(((item as! SelectListItem).Value?.toInt())!)
            }
        }
        return list
    }
    
    func take(_ n: Int) -> [Element] {
        if self.count == 0 {
            return []
        }
        let size = self.count < n ? self.count : n
        var result: [Element] = []
        for index in 0...size-1 {
            result.append(self[index])
        }
        return result
    }
    
    func each(_ f: (Element) -> ()) {
        for item in self {
            f(item)
        }
    }
    
    func eachWithIndex(_ f: (Int, Element) -> ()) {
        if self.count <= 0 {
            return
        }
        for i in 0...self.count-1 {
            f(i, self[i])
        }
    }
    
    func zip<U>(_ other: [U]) -> [(Element, U)] {
        var result = [(Element, U)]()
        for (p, q) in Swift.zip(self, other) {
            result.append((p, q))
        }
        return result
    }
    
    func indexOf <U: Equatable> (_ item: U) -> Int? {
        if item is Element {
            return unsafeBitCast(self, to: [U].self).indexOf(item)
        }
        
        return nil
    }
    
    func find (_ f: (Element) -> Bool) -> Element? {
        for value in self {
            if f(value) {
                return value
            }
        }
        return nil
    }
    
    func mapWithIndex<U>(_ f: (Int, Element) -> U) -> [U] {
        if self.isEmpty {
            return []
        }
        var elements: [U] = []
        for i in 0...self.count-1 {
            let item = self[i]
            let newItem = f(i, item)
            elements.append(newItem)
        }
        return elements
    }
    
    func without<U: Equatable>(_ target: U) -> [U] {
        var results: [U] = []
        for item in self {
            if item as! U != target {
                results.append(item as! U)
            }
        }
        return results
    }
    
    func at(_ index: Int) -> Element? {
        if count > index {
            return self[index]
        }
        return nil
    }
    
    func flatten<U>() -> [U] {
        var res: [U] = []
        for array in self {
            if let arr = array as? [U] {
                for item in arr {
                    res.append(item)
                }
            }
        }
        return res
    }
}
