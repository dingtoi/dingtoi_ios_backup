//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import UIKit

extension UITextField {
    
    public var cursorOffset: Int? {
        guard let range = selectedTextRange else { return nil }
        return offset(from: beginningOfDocument, to: range.start)
    }
    
    public var cursorIndex: String.Index? {
        guard let cursorOffset = cursorOffset else { return nil }
        return text?.index((text?.startIndex)!, offsetBy: cursorOffset, limitedBy: (text?.endIndex)!)
    }
    
    public func setCursor(position: Int) {
        let position = self.position(from: beginningOfDocument, offset: position)!
        selectedTextRange = textRange(from: position, to: position)
    }
    
    public func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    public func addPaddingLeft(_ paddingWidth: Int = 10) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: paddingWidth, height: 0))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
