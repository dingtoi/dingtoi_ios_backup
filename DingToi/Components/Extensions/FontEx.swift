//
//  FontEx.swift
//  PrudProject
//
//  Created by Tran Viet Thuc on 1/20/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

extension UIFont {
    // Arial, Arial-BoldMT, Arial-ItalicMT, Arial-BoldItalicMT
    
    func withTraits(traits:UIFontDescriptorSymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
    }
    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
    
    func italic() -> UIFont {
        return withTraits(traits: .traitItalic)
    }
}
