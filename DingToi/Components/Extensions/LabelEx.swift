//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import UIKit

extension UILabel {
    public enum DrawLabel {
        case LEFT
        case RIGHT
        case CENTER
    }
    
    public func underline() {
        if let textString = self.text, textString.count > 0 {
            let attributedString = NSMutableAttributedString(string: textString)
            if (attributedString.length - 1) > 0 {
                attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
                attributedText = attributedString
            }
        }
    }
    
    public func underline(text:String) -> NSAttributedString {
        let underlineAttribute = [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.patternDash.rawValue | NSUnderlineStyle.styleSingle.rawValue]
        let range = (self.text! as NSString).range(of: text)
        let attributedString = NSMutableAttributedString(string: self.text!)
        
        attributedString.addAttributes(underlineAttribute, range: range)
        return attributedString
    }
    
    public func drawBorderTextLabel(_ text: String?,_ type: DrawLabel,_ radius: CGFloat = 10.0,_ textColor: UIColor = .black,_ borderColor: UIColor = .green) {
        if let txt = text {
            self.text = txt
            self.backgroundColor = borderColor
            self.layer.borderColor = borderColor.cgColor
            self.layer.borderWidth = 1.0
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = true
            self.textColor = textColor
            
            if type == .LEFT {
                let width = (self.text?.widthOfString(usingFont: self.font) ?? 0.0)
                let height = self.frame.height
                let padding: CGFloat = 0.0
                let x = self.frame.origin.x - padding
                self.frame = CGRect.init(x: x, y: self.frame.origin.y, width: width + 40.0 - padding, height: height)
                self.textAlignment = .left
            }else if type == .CENTER {
                let width = self.text?.widthOfString(usingFont: self.font) ?? 0.0
                let height = self.frame.height
                self.frame = CGRect.init(x: self.frame.origin.x, y: self.frame.origin.y, width: width + 20.0, height: height)
                self.textAlignment = .center
            }else if type == .RIGHT {
                let width = (self.text?.widthOfString(usingFont: self.font) ?? 0.0) + 40.0
                let height = self.frame.height
                let padding: CGFloat = 0.0
                let x = self.frame.origin.x + (self.frame.width - width) - padding
                self.frame = CGRect.init(x: x, y: self.frame.origin.y, width: width - padding, height: height)
                self.textAlignment = .right
            }
            self.layoutIfNeeded()
        }else {
            self.text = ""
            self.backgroundColor = .clear
            self.borderColor = .clear
            self.borderWidth = 0.0
        }
    }
    
    func drawAnimationTextLabel(typedText: String, characterDelay: TimeInterval = 0.5) {
        text = ""
        var writingTask: DispatchWorkItem?
        writingTask = DispatchWorkItem { [weak weakSelf = self] in
            for character in typedText {
                DispatchQueue.main.async {
                    weakSelf?.text!.append(character)
                }
                Thread.sleep(forTimeInterval: characterDelay/100)
            }
        }
        
        if let task = writingTask {
            //let queue = DispatchQueue(label: "typespeed", qos: DispatchQoS.userInteractive)
            //queue.asyncAfter(deadline: .now() + 0.05, execute: task)
            DispatchQueue.global().async(execute: task)
        }
    }
    
    func setTextSpacingBy(value: Double) {
      if let textString = self.text {
        let attributedString = NSMutableAttributedString(string: textString)
        attributedString.addAttribute(.kern, value: value, range: NSRange(location: 0, length: attributedString.length - 1))
        attributedText = attributedString
      }
    }
}
