//
//  SannerProtocol.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/3/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
protocol ScannerProtocol {
    func scanner()
}

protocol ScannerHandler: ScannerProtocol {
    var delegate: ScannerDelegate? { get set }
    var dataSource: ScannerDataSource? { get set }
}

protocol ScannerDelegate {
    
}

protocol ScannerDataSource {
    func scanned(deviceInfo: DeviceInfo, data: Array<Array<InfoCell>>)
}
