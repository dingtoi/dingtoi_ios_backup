//
//  TransactionProtocol.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/6/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation

protocol TransactionProtocol {
    func checkTransactionCode(transactionCode: String)
}

protocol TransactionHandler: TransactionProtocol {
    var delegate: TransactionDelegate? { get set }
    var dataSource: TransactionDataSource? { get set }
    
}

protocol TransactionDelegate {
    
}

protocol TransactionDataSource {
    func onSuccess(response: TransactionCodeResponse)
    func onError(error: ResponseErrorModel)
}
