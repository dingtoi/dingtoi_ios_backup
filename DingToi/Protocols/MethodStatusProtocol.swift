//
//  MethodStatusProtocol.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/3/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol MethodStatusDelegate {
    
    @available(iOS 10.0, *)
    func bluetoothScanned(status: CBManagerState)
    
}
