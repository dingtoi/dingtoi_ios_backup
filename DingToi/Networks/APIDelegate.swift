//  Created by boys vip on 6/18/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit

public protocol Register_Delegate {
    //func OnSuccess_Register(result: ResponseRegisterModel)
    func OnError_Register(error: ResponseErrorModel)
}

public protocol TransactionCodeDelegate {
    func onSuccessTransactionCode(response: TransactionCodeResponse)
    func onErrorTransactionCode(error: ResponseErrorModel)
}
