//  Created by boys vip on 6/18/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation
import AlamofireImage
import Alamofire
import HandyJSON

//let parameters: Parameters = [
//    "Start": "",
//    "Length": "",
//    "CreatedBefore": ""
//]

//if let object = LoginResponseModel.deserialize(from: success) {
//    delegate.OnSuccess(success: object)
//}else{
//    let temp:ResponseErrorModel = ResponseErrorModel.init(Code: "", Codes: "", Message: self.err_Default)
//    delegate.OnError(error: temp)
//}

public final class HttpsRequest {
    static let CODE_TIME_OUT = "9999"
    static let CODE_AUTHEN = "401"
    static let CODE_DEFAULT = "888"
    
    var headers: HTTPHeaders = [
        "Content-Type": "application/json-patch+json",
        "Accept": "application/json",
        //"Authorization": "bearer token"
    ]
    
    public let err_Connect = "Không có kết nối mạng, vui lòng thử lại sau !"
    public let err_TimeOut = "Quá thời gian kết nối, vui lòng thử lại sau !"
    public let err_Lost = "Mất kết nối mạng, vui lòng thử lại sau !"
    public let err_Default = "Máy chủ đang bận, vui lòng thử lại sau !"
    
    //=====================================================================================
    public func GET(_ url: String,_ parameters: Parameters?,_ token: String?,_ success: (@escaping (DataResponse<Data>, String) -> (Void)), _ error: (@escaping (DataResponse<Data>, String) -> (Void)),_ encoding: ParameterEncoding = JSONEncoding(options: [])) {
        if let tk = token {
            headers = [
            "Content-Type": "application/json-patch+json",
            "Accept": "application/json",
            "Authorization": "bearer " + tk
            ]
        }else{
            headers = [
            "Content-Type": "application/json-patch+json",
            "Accept": "application/json"
            ]
        }
        print("API>>> Parameter: \(String(describing: parameters?.toJsonString()))")
        Alamofire.request(url, method: .get, parameters: parameters, encoding: encoding, headers: headers).validate(statusCode: 200..<600).responseData { response in
                //print("API>>>  Request: \(String(describing: response.request))")
                //print("API>>>  Response: \(String(describing: response.response))")
                //print("API>>>  Result: \(response.result)")
                print("API>>>  Status: \(String(describing: response.response?.statusCode))")
                if response.response?.statusCode == 200 {
                    if let data = response.result.value, let utf8Text = String(data: data, encoding: .utf8) {
                        //print("API>>>  DataSuccess: \(utf8Text)")
                        success(response, utf8Text)
                    }else {
                        print("API>>> Not Response Data: \(response.debugDescription)")
                        let err = ResponseErrorModel.init(Code: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, Codes: "", Message: self.err_Default)
                        error(response, err.toJSONString() ?? "")
                    }
                }else{
                    if self.sectionTimeout(response) {
                        return
                    }
                    if let data = response.result.value, let utf8Text = String(data: data, encoding: .utf8) {
                        print("API>>>  DataError: \(utf8Text)")
                        error(response, utf8Text)
                    }else{
                        if let err = response.result.error as? URLError {
                            switch err.code {
                                case .notConnectedToInternet:
                                    print("No Internet Connection")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                case .timedOut:
                                    print("Request Time Out")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                case .networkConnectionLost:
                                    print("Connection Lost")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                default:
                                    print("Default Error")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                            }
                        }else {
                            print("API>>>>>> Exception GET: \(String(describing: response.error))")  //Exception
                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                            error(response, err.toJSONString() ?? "")
                        }
                    }
                }
        }
    }
    
    //=====================================================================================
    public func POST(_ url: String,_ parameters: Parameters?,_ token: String?,_ success: (@escaping (DataResponse<Data>, String) -> (Void)),_ error: (@escaping (DataResponse<Data>, String) -> (Void)),_ encoding: ParameterEncoding = JSONEncoding(options: [])){
        if let tk = token {
            headers = [
                "Content-Type": "application/json-patch+json",
                "Accept": "application/json",
                "Authorization": "bearer " + tk
            ]
        }else{
            headers = [
                "Content-Type": "application/json-patch+json",
                "Accept": "application/json"
            ]
        }
        print("API>>> Parameter: \(String(describing: parameters?.toJsonString()))")
        Alamofire.request(url, method: .post, parameters: parameters, encoding: encoding, headers: headers).validate(statusCode: 200..<600)
            .responseData { response in
                //print("API>>>  Request: \(String(describing: response.request))")
                //print("API>>>  Response: \(String(describing: response.response))")
                //print("API>>>  Result: \(response.result)")
                print("API>>>  Status: \(String(describing: response.response?.statusCode))")
                if response.response?.statusCode == 200 {
                    if let data = response.result.value,
                       let utf8Text = String(data: data, encoding: .utf8) {
                        //print("API>>>  DataSuccess: \(utf8Text)")
                        success(response, utf8Text)
                    }else {
                        print("API>>> Not Response Data: \(response.debugDescription)")
                        let err = ResponseErrorModel.init(Code: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, Codes: "", Message: self.err_Default)
                        error(response, err.toJSONString() ?? "")
                    }
                }else{
                    if self.sectionTimeout(response) {
                        return
                    }
                    if let data = response.result.value,
                       let utf8Text = String(data: data, encoding: .utf8) {
                        print("API>>>  DataError: \(utf8Text)")
                        error(response, utf8Text)
                    }else{
                        if let err = response.result.error as? URLError {
                            switch err.code {
                                case .notConnectedToInternet:
                                    print("No Internet Connection")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                case .timedOut:
                                    print("Request Time Out")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                case .networkConnectionLost:
                                    print("Connection Lost")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                default:
                                    print("Default Error")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                            }
                        }else {
                            print("API>>>>>>  Exception POST: \(String(describing: response.error))")  //Exception
                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                            error(response, err.toJSONString() ?? "")
                        }
                    }
                }
        }
    }
    
    //=====================================================================================
    public func POST_CHECKVERSION(_ url: String,_ parameters: Parameters?,_ token: String?,_ success: (@escaping (String) -> (Void)),_ error: (@escaping (String) -> (Void)),_ encoding: ParameterEncoding = JSONEncoding(options: [])){
        if let tk = token {
            headers = [
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "bearer " + tk
            ]
        }else{
            headers = [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
        }
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: encoding, headers: headers).validate(statusCode: 200..<600)
            .responseData { response in
                //print("API>>>  Request: \(String(describing: response.request))")
                //print("API>>>  Response: \(String(describing: response.response))")
                //print("API>>>  Result: \(response.result)")
                print("API>>>  Status: \(String(describing: response.response?.statusCode))")
                if response.response?.statusCode == 200 {
                    if let data = response.result.value, let utf8Text = String(data: data, encoding: .utf8) {
                        //print("API>>>  DataSuccess: \(utf8Text)")
                        success(utf8Text)
                    }else {
                        print("API>>> Not Response Data: \(response.debugDescription)")
                        let err = ResponseErrorModel.init(Code: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, Codes: "", Message: self.err_Default)
                        error(err.toJSONString() ?? "")
                    }
                }else{
                    if self.sectionTimeout(response) {
                        return
                    }
                    if let err = response.result.error as? URLError {
                        switch err.code {
                            case .notConnectedToInternet:
                                print("No Internet Connection")
                                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                                error(err.toJSONString() ?? "")
                            case .timedOut:
                                print("Request Time Out")
                                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                                error(err.toJSONString() ?? "")
                            case .networkConnectionLost:
                                print("Connection Lost")
                                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                                error(err.toJSONString() ?? "")
                            default:
                                print("Default Error")
                                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                error(err.toJSONString() ?? "")
                        }
                    }else {
                        print("API>>>>>>  Exception POST: \(String(describing: response.error))")  //Exception
                        let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                        error(err.toJSONString() ?? "")
                    }
                }
        }
    }
    
    
    //=====================================================================================
    public func PUT(_ url: String,_ parameters: Parameters?,_ token: String?,_ success: (@escaping (DataResponse<Data>, String) -> (Void)),_ error: (@escaping (DataResponse<Data>, String) -> (Void)),_ encoding: ParameterEncoding = JSONEncoding(options: [])){
        if let tk = token {
            headers = [
                "Content-Type": "application/json-patch+json",
                "Accept": "application/json",
                "Authorization": "bearer " + tk
            ]
        }else{
            headers = [
                "Content-Type": "application/json-patch+json",
                "Accept": "application/json"
            ]
        }
        print("API>>> Parameter: \(String(describing: parameters?.toJsonString()))")
        Alamofire.request(url, method: .put, parameters: parameters, encoding: encoding, headers: headers).validate(statusCode: 200..<600)
            .responseData { response in
                //print("API>>>  Request: \(String(describing: response.request))")
                //print("API>>>  Response: \(String(describing: response.response))")
                //print("API>>>  Result: \(response.result)")
                print("API>>>  Status: \(String(describing: response.response?.statusCode))")
                if response.response?.statusCode == 200 {
                    if let data = response.result.value,
                       let utf8Text = String(data: data, encoding: .utf8) {
                            //print("API>>>  DataSuccess: \(utf8Text)")
                            success(response, utf8Text)
                    }else {
                        print("API>>> Not Response Data: \(response.debugDescription)")
                        let err = ResponseErrorModel.init(Code: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, Codes: "", Message: self.err_Default)
                        error(response, err.toJSONString() ?? "")
                    }
                }else{
                    if self.sectionTimeout(response) {
                        return
                    }
                    if let data = response.result.value,
                       let utf8Text = String(data: data, encoding: .utf8) {
                            print("API>>>  DataError: \(utf8Text)")
                            error(response, utf8Text)
                    }else{
                        if let err = response.result.error as? URLError {
                            switch err.code {
                                case .notConnectedToInternet:
                                    print("No Internet Connection")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                case .timedOut:
                                    print("Request Time Out")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                case .networkConnectionLost:
                                    print("Connection Lost")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                default:
                                    print("Default Error")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                            }
                        }else {
                            print(">>>>>>  Exception PUT: \(String(describing: response.error))")  //Exception
                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                            error(response , err.toJSONString() ?? "")
                        }
                    }
                }
        }
    }
    
    
    //=====================================================================================
    func DELETE(_ url: String,_ parameters: Parameters?,_ token: String?,_ success: (@escaping (DataResponse<Data>, String) -> (Void)),_ error: (@escaping (DataResponse<Data>, String) -> (Void)),_ encoding: ParameterEncoding = JSONEncoding(options: [])) {
        if let tk = token {
            headers = [
                "Content-Type": "application/json-patch+json",
                "Accept": "application/json",
                "Authorization": "bearer " + tk
            ]
        }else{
            headers = [
                "Content-Type": "application/json-patch+json",
                "Accept": "application/json"
            ]
        }
        print("API>>> Parameter: \(String(describing: parameters?.toJsonString()))")
        Alamofire.request(url, method: .delete, parameters: parameters, encoding: encoding, headers: headers)
            .validate(statusCode: 200..<600)
            .responseData { response in
                //print("API>>>  Request: \(String(describing: response.request))")
                //print("API>>>  Response: \(String(describing: response.response))")
                //print("API>>>  Result: \(response.result)")
                print("API>>>  Status: \(String(describing: response.response?.statusCode))")
                if response.response?.statusCode == 200 {
                    if let data = response.result.value,
                       let utf8Text = String(data: data, encoding: .utf8) {
                            //print("API>>>  DataSuccess: \(utf8Text)")
                            success(response, utf8Text)
                    }else {
                        print("API>>> Not Response Data: \(response.debugDescription)")
                        let err = ResponseErrorModel.init(Code: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, Codes: "", Message: self.err_Default)
                        error(response, err.toJSONString() ?? "")
                    }
                }else{
                    if self.sectionTimeout(response) {
                        return
                    }
                    if let data = response.result.value,
                       let utf8Text = String(data: data, encoding: .utf8) {
                            print("API>>>  DataError: \(utf8Text)")
                            error(response, utf8Text)
                    }else{
                        if let err = response.result.error as? URLError {
                            switch err.code {
                                case .notConnectedToInternet:
                                    print("No Internet Connection")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                case .timedOut:
                                    print("Request Time Out")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                case .networkConnectionLost:
                                    print("Connection Lost")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                default:
                                    print("Default Error")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                            }
                        }else {
                            print(">>>>>>  Exception DELETE: \(String(describing: response.error))")  //Exception
                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                            error(response , err.toJSONString() ?? "")
                        }
                    }
                }
        }
    }
    
    
    //=====================================================================================
    func POST_IMAGE(_ url: String,_ parameters: Parameters,_ nameImage: String,_ listImage: [UIImage?]?,_ token: String?,_ success: (@escaping (DataResponse<Any>?, String) -> (Void)),_ error: (@escaping (DataResponse<Any>?, String) -> (Void))) {
        if let tk = token {
            headers = [
                "Content-Type": "multipart/form-data",
                "Accept": "application/json",
                "Authorization": "bearer " + tk
            ]
        }else{
            headers = [
                "Content-Type": "multipart/form-data",
                "Accept": "application/json"
            ]
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                }
                for image in listImage! {
                    if let img = image, let imgData = UIImageJPEGRepresentation(img, 0.5) {
                        multipartFormData.append(imgData, withName: nameImage, fileName: "fileimage.jpg", mimeType: "image/jpg")
                    }
                }
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) {
                (result) in
                switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            let zero: String? = "Response could not be serialized, input data was nil or zero length."
                            if response.error != nil && response.error?.localizedDescription != zero { //Error
                                if let data = response.data,
                                    let utf8Text = String(data: data, encoding: .utf8) {
                                    print("API>>>  DataError: \(utf8Text)")
                                    error(response, utf8Text)
                                }else{
                                    if let err = response.result.error as? URLError {
                                        switch err.code {
                                            case .notConnectedToInternet:
                                                print("No Internet Connection")
                                                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                                                error(response, err.toJSONString() ?? "")
                                            case .timedOut:
                                                print("Request Time Out")
                                                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                                                error(response, err.toJSONString() ?? "")
                                            case .networkConnectionLost:
                                                print("Connection Lost")
                                                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                                                error(response, err.toJSONString() ?? "")
                                            default:
                                                print("Default Error")
                                                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                                error(response, err.toJSONString() ?? "")
                                        }
                                    }else {
                                        print("API>>> Not Response Data: \(response.debugDescription)")
                                        let err = ResponseErrorModel.init(Code: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, Codes: "", Message: self.err_Default)
                                        error(response, err.toJSONString() ?? "")
                                    }
                                }
                            }else{//Success
                                if let res = response.response {
                                    if res.statusCode == 200 {
                                        if let data = response.data,
                                            let utf8Text = String(data: data, encoding: .utf8) {
                                            print("API>>>  DataSuccess: \(utf8Text)")
                                            success(response, utf8Text)
                                        }else {
                                            print("API>>> Not Response Data: \(response.debugDescription)")
                                            let err = ResponseErrorModel.init(status: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                            error(response, err.toJSONString() ?? "")
                                        }
                                    }else{
                                        if let err = response.result.error as? URLError {
                                            switch err.code {
                                                case .notConnectedToInternet:
                                                    print("No Internet Connection")
                                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                                                    error(response, err.toJSONString() ?? "")
                                                case .timedOut:
                                                    print("Request Time Out")
                                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                                                    error(response, err.toJSONString() ?? "")
                                                case .networkConnectionLost:
                                                    print("Connection Lost")
                                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                                                    error(response, err.toJSONString() ?? "")
                                                default:
                                                    print("Default Error")
                                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                                    error(response, err.toJSONString() ?? "")
                                            }
                                        }else {
                                            if let data = response.data,
                                                let utf8Text = String(data: data, encoding: .utf8) {
                                                print("API>>>  DataError: \(utf8Text)")
                                                if let err = ResponseErrorModel.deserialize(from: utf8Text) {
                                                    error(response, err.toJSONString() ?? "")
                                                }
                                            }else {
                                                print("API>>> Not Response Data: \(response.debugDescription)")
                                                let err = ResponseErrorModel.init(status: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                                error(response, err.toJSONString() ?? "")
                                            }
                                        }
                                    }
                                }else{
                                    print("API>>> Not Response Data")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                }
                            }
                        }
                    case .failure(let ex): //Error
                        print("Error in upload: \(ex.localizedDescription)")
                        let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                        error(nil, err.toJSONString() ?? "")
                }
        }
    }
    
    
    //=====================================================================================
    func PUT_IMAGE(_ url: String,_ parameters: Parameters,_ nameImage: String,_ listImage: [UIImage?]?,_ token: String?,_ success: (@escaping (DataResponse<Any>?, String) -> (Void)),_ error: (@escaping (DataResponse<Any>?, String) -> (Void))) {
        if let tk = token {
            headers = [
                "Content-Type": "multipart/form-data",
                "Accept": "application/json",
                "Authorization": "bearer " + tk
            ]
        }else{
            headers = [
                "Content-Type": "multipart/form-data",
                "Accept": "application/json"
            ]
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
            }
            for image in listImage! {
                if let img = image, let imgData = UIImageJPEGRepresentation(img, 0.5) {
                    multipartFormData.append(imgData, withName: nameImage, fileName: "fileimage.jpg", mimeType: "image/jpg")
                }
            }
        }, usingThreshold: UInt64.init(), to: url, method: .put, headers: headers) {
            (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let zero: String? = "Response could not be serialized, input data was nil or zero length."
                    if response.error != nil && response.error?.localizedDescription != zero { //Error
                        if let data = response.data,
                            let utf8Text = String(data: data, encoding: .utf8) {
                            print("API>>>  DataError: \(utf8Text)")
                            error(response, utf8Text)
                        }else{
                            if let err = response.result.error as? URLError {
                                switch err.code {
                                    case .notConnectedToInternet:
                                        print("No Internet Connection")
                                        let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                                        error(response, err.toJSONString() ?? "")
                                    case .timedOut:
                                        print("Request Time Out")
                                        let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                                        error(response, err.toJSONString() ?? "")
                                    case .networkConnectionLost:
                                        print("Connection Lost")
                                        let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                                        error(response, err.toJSONString() ?? "")
                                    default:
                                        print("Default Error")
                                        let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                        error(response, err.toJSONString() ?? "")
                                }
                            }else {
                                print("API>>> Not Response Data: \(response.debugDescription)")
                                let err = ResponseErrorModel.init(Code: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, Codes: "", Message: self.err_Default)
                                error(response, err.toJSONString() ?? "")
                            }
                        }
                    }else{//Success
                        if let res = response.response {
                            if res.statusCode == 200 {
                                if let data = response.data,
                                    let utf8Text = String(data: data, encoding: .utf8) {
                                    print("API>>>  DataSuccess: \(utf8Text)")
                                    success(response, utf8Text)
                                }else {
                                    print("API>>> Not Response Data")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                }
                            }else{
                                if let err = response.result.error as? URLError {
                                    switch err.code {
                                        case .notConnectedToInternet:
                                            print("No Internet Connection")
                                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                                            error(response, err.toJSONString() ?? "")
                                        case .timedOut:
                                            print("Request Time Out")
                                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                                            error(response, err.toJSONString() ?? "")
                                        case .networkConnectionLost:
                                            print("Connection Lost")
                                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                                            error(response, err.toJSONString() ?? "")
                                        default:
                                            print("Default Error")
                                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                            error(response, err.toJSONString() ?? "")
                                    }
                                }else {
                                    print("API>>> Not Response Data")
                                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                                    error(response, err.toJSONString() ?? "")
                                }
                            }
                        }else{
                            print("API>>> Not Response Data")
                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                            error(response, err.toJSONString() ?? "")
                        }
                    }
                }
            case .failure(let ex): //Error
                print("Error in upload: \(ex.localizedDescription)")
                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                error(nil, err.toJSONString() ?? "")
            }
        }
    }
    
    
    //=====================================================================================
    func GET_IMAGE(_ url: String,_ token: String?,_ success: (@escaping (DataResponse<Data>, UIImage?) -> (Void)),_ error: (@escaping (DataResponse<Data>, String) -> (Void))) {
        if let tk = token {
            headers = [
                "Content-Type": "application/json-patch+json",
                "Accept": "application/json",
                "Authorization": "bearer " + tk
            ]
        }else{
            headers = [
                "Content-Type": "application/json-patch+json",
                "Accept": "application/json"
            ]
        }
        
        Alamofire.request(url).responseData { (response) in
            if response.error == nil {
                print(response.result)
                if let data = response.data {
                    success(response, UIImage(data: data))
                }else {
                    print("API>>> Not Response Data: \(response.debugDescription)")
                    let err = ResponseErrorModel.init(Code: response.response?.statusCode.description ?? HttpsRequest.CODE_DEFAULT, Codes: "", Message: self.err_Default)
                    error(response, err.toJSONString() ?? "")
                }
            }else{
                if self.sectionTimeout(response) {
                    return
                }
                if let err = response.result.error as? URLError {
                    switch err.code {
                        case .notConnectedToInternet:
                            print("No Internet Connection")
                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Connect, errors: nil)
                            error(response, err.toJSONString() ?? "")
                        case .timedOut:
                            print("Request Time Out")
                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_TimeOut, errors: nil)
                            error(response, err.toJSONString() ?? "")
                        case .networkConnectionLost:
                            print("Connection Lost")
                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_TIME_OUT, message: self.err_Lost, errors: nil)
                            error(response, err.toJSONString() ?? "")
                        default:
                            print("Default Error")
                            let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                            error(response, err.toJSONString() ?? "")
                    }
                }else {
                    print(">>>>>>  Exception GET: \(String(describing: response.error))")  //Exception
                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                    error(response , err.toJSONString() ?? "")
                }
            }
        }
    }
    
    //Timeout
    func sectionTimeout(_ response: DataResponse<Data>) -> Bool {
        if let statusCode = response.response?.statusCode, statusCode == 401 {
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            if let nav = appDelegate._rootNavigationController {
//                if let rootVC = nav.visibleViewController as? BaseViewController {
//                    rootVC.sessionTimeout()
//                }
//            }
            return true
        }
        return false
    }
}

extension Dictionary {
    func toJsonString() -> String? {
        let jsonData = try? JSONSerialization.data(withJSONObject: self, options: [])
        guard jsonData != nil else {return nil}
        let jsonString = String(data: jsonData!, encoding: .utf8)
        guard jsonString != nil else {return nil}
        return jsonString!
    }
}
