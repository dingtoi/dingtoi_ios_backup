//  Created by boys vip on 6/18/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation
import HandyJSON

public class ResponseErrorModel: HandyJSON {
    var status: String?
    var message: String?
    var errors: Dictionary<String,String>?
    var error: ErrorModel?
    
    init(status: String?, message: String?, errors: Dictionary<String,String>?) {
        self.status = status
        self.message = message
        self.errors = errors
    }
    
    var Code: String?
    var Codes: String?
    var Message: String?
    
    init(Code: String?, Codes: String?, Message: String?) {
        self.Code = Code
        self.Codes = Codes
        self.Message = Message
    }
    
    public required init() {}
}

public class ErrorModel: HandyJSON {
    var code: String?
    var message: Array<MessageModel>?
    
    public required init() {}
}

public class MessageModel: HandyJSON {
    var msg: String?
    var type: String?
    var path: String?
    
    public required init() {}
}
