//  Created by boys vip on 6/18/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation
import HandyJSON

//let param:[String : Any]! = [
//    "UserName": request.UserName ?? "",
//    "Password": request.Password ?? ""
//]

//if let object = ResponseRegisterModel.deserialize(from: success) {
//    delegate.OnSuccess_Register(result: object)
//}else{
//    let err = ResponseErrorModel.init(status: HttpsRequest.CODE, message: self.err_Default, errors: nil)
//    delegate.OnError_Register(error: err)
//}

public final class APIManager {
    public static let shared = APIManager()
    private let checkReportError = true
    public static let url = "http://dingtoi.com:30080/api/v1"
    let err_Default = "Máy chủ đang bận, vui lòng thử lại sau !"
    
    let url_Register_POST = "\(APIManager.url)/api/register"
    let urlTransactionCheck = "\(APIManager.url)/transaction/check"
    
    
    //Hàm Đăng Ký
    func Register(phone_number: String, full_name: String, dob: String, national_id: String, gender: String, password: String, confirmed_password: String, delegate: Register_Delegate) {
        let param:[String : Any]! = [
            "phone_number": phone_number,
            "full_name": full_name,
            "dob": dob,
            "national_id": national_id,
            "gender": gender,
            "password": password,
            "confirmed_password": confirmed_password
        ]
        
        HttpsRequest.init().POST(url_Register_POST, param, nil, { (response, success) -> (Void) in
            print("\(success)")
        }, { (response, error) -> (Void) in
            if let err = ResponseErrorModel.deserialize(from: error) {
                delegate.OnError_Register(error: err)
            }else{
                if let err = ResponseErrorModel.deserialize(from: error) {
                    delegate.OnError_Register(error: err)
                }else{
                    print(">>>  Exception Pase Json -------------------------")
                    let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                    delegate.OnError_Register(error: err)
                }
//                if self.checkReportError {
//                    SendMailReport.sendMail(param.toJsonString(),error.description)
//                }
            }
        })
    }
    
    // Check Transaction Code
    func checkTransactionCode(transactionCode: String, delegate: TransactionCodeDelegate) {
        
        HttpsRequest.init().GET("\(urlTransactionCheck)/\(transactionCode)", nil, nil, { (response, success) -> (Void) in
            if let result = TransactionCodeResponse.deserialize(from: success) {
                delegate.onSuccessTransactionCode(response: result)
            } else {
                print(">>>  Exception Pase Json -------------------------")
                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                delegate.onErrorTransactionCode(error: err)
            }
        }, {(response, error) -> (Void) in
            if let err = ResponseErrorModel.deserialize(from: error) {
                delegate.onErrorTransactionCode(error: err)
            }else{
                print(">>>  Exception Pase Json -------------------------")
                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                delegate.onErrorTransactionCode(error: err)
            }
        })
    }
    
}
