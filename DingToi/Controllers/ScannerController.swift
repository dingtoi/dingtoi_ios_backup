//
//  ScannerController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/3/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth
import SystemServices
import AVFoundation

class ScannerController: ScannerHandler {
    var session: AVCaptureSession?
    var input: AVCaptureDeviceInput?
    var delegate: ScannerDelegate?
    var dataSource: ScannerDataSource?
    
    func scanner() {
        self.dataSource?.scanned(deviceInfo: self.deviceInfo(), data: self.data())
    }
    
    func data() -> Array<Array<InfoCell>> {
        var infos = Array<InfoCell>()
        let harewareItem = Items(key1: "Phone's operating system - latest version", value1: SystemServices.shared().systemsVersion, key2: "Phone's processcor", value2: UIDevice.current.getCPUName(), key3: "Storage capacity of phone", value3: "\(DiskStatus.usedDiskSpace() ?? "N/A") of \(DiskStatus.totalDiskSpace() ?? "N/A") used")
        
        infos.append(InfoCell(key: "Hardware verification", value: "Processor, OS, storage", imageName: "icon_hardware", isFailed: false, items: harewareItem))
        
        let cameraItem = Items(key1: "Camera", value1: "\(CameraStatus.camera(.rear))", key2: "Flash", value2: "\(self.checkFlashStatus())", key3: nil, value3: nil)
        let isWorking = CameraStatus.camera(.rear) && self.checkFlashStatus()
        infos.append(InfoCell(key: "Camera + Flash", value: "Camera + Flash \(isWorking ? "working" : "not working")", imageName: "icon_camera", isFailed: !isWorking, items: cameraItem))
//        let cameraItem = Items(key1: "Camera", value1: "\(self.checkCameraStatus())", key2: "Flash", value2: "\(self.checkFlashStatus())", key3: nil, value3: nil)
//        let isWorking = self.checkCameraStatus() && self.checkFlashStatus()
//        infos.append(InfoCell(key: "Camera + Flash", value: "Camera + Flash \(isWorking ? "working" : "not working")", imageName: "icon_camera", isFailed: !isWorking, items: cameraItem))
        
        let volumeItem = Items(key1: "Volume", value1: "\(self.checkVolumeControl())", key2: "Speaker", value2: "\(self.checkSpeaker())", key3: nil, value3: nil)
        infos.append(InfoCell(key: "Volume adjustment", value: nil, imageName: "icon_volume", isFailed: !(self.checkVolumeControl() && self.checkSpeaker()), items: volumeItem))
        
//        infos.append(InfoCell(key: "Brightness adjustment", value: <#T##String?#>, imageName: <#T##String?#>, isFailed: <#T##Bool?#>, items: <#T##Items?#>))
        
        var wifi = Array<InfoCell>()
        wifi.append(InfoCell(key: "Wifi", value: "Wifi \(NetworkStatus.shared.status() ? "working" : "not working")", imageName: "icon_wifi", isFailed: !NetworkStatus.shared.status(), items: nil))
        wifi.append(InfoCell(key: "Bluetooth", value: nil, imageName: "icon_bluetooth", isFailed: nil, items: nil))
        wifi.append(InfoCell(key: "Finger Scanner", value: "Finger Scanner \(FingerprintStatus.shared.status() ? "working" : "not working")", imageName: "icon_fingerprint", isFailed: !FingerprintStatus.shared.status(), items: nil))
        
        var advance = Array<InfoCell>()
        let voiceCallItem = Items(key1: "Outbound", value1: "\(TelephonyStatus.callStatus())", key2: "Inbound", value2: "true", key3: nil, value3: nil)
        advance.append(InfoCell(key: "Voice calls", value: nil, imageName: "icon_call", isFailed: !TelephonyStatus.callStatus(), items: voiceCallItem))
        let textingItem = Items(key1: "Outbound", value1: "\(TelephonyStatus.smsStatus())", key2: "Inbound", value2: "true", key3: nil, value3: nil)
        advance.append(InfoCell(key: "Texting", value: nil, imageName: "icon_sms", isFailed: TelephonyStatus.smsStatus(), items: textingItem))
        
        let dataTransferItem = Items(key1: "Download", value1: "true", key2: "Upload", value2: "\(TelephonyStatus.dataSendStatus())", key3: nil, value3: nil)
        advance.append(InfoCell(key: "Data transfer", value: nil, imageName: "data", isFailed: TelephonyStatus.dataSendStatus(), items: dataTransferItem))
//        infocells.append(InfoCell(key: "Used Time", value: ""))
//        infocells.append(InfoCell(key: "Battery Recharged Time", value: ""))
//        infocells.append(InfoCell(key: "Maximum Battery Capacity", value: ""))
//        infocells.append(InfoCell(key: "Dead Pixels", value: ""))
//        infocells.append(InfoCell(key: "Rear Camera Function", value: CameraStatus.camera(.rear) ? "YES" : "NO", imageName: "Camera"))
//        infocells.append(InfoCell(key: "Support Bluetooth", value: self.bluetoothSupport))
//        infocells.append(InfoCell(key: "Support Wifi", value: NetworkStatus.shared.status() ? "YES" : "NO"))
//        infocells.append(InfoCell(key: "Disk Storage Capacity", value: DiskStatus.totalDiskSpace))
//        infocells.append(InfoCell(key: "Free Disk Storage", value: DiskStatus.freeDiskSpace))
//        infocells.append(InfoCell(key: "Activated", value: ""))
//        infocells.append(InfoCell(key: "Fingerprinter", value: FingerprintStatus.shared.status() ? "YES" : "NO"))
//        infocells.append(InfoCell(key: "Receive incoming calls", value: ""))
//        infocells.append(InfoCell(key: "Callout", value: "Telephony working", imageName: "Battery", isFailed: false))
//        infocells[FunctionName.SendAndReceiveSMS.hashValue] =
//        infocells.append(InfoCell(key: "Send and receive text and data", value: ""))
//        infocells.append(InfoCell(key: "Support Front Flash", value: CameraStatus.flash(.front) ? "YES" : "NO"))
//        infocells.append(InfoCell(key: "Support Rear Flash", value: CameraStatus.flash(.rear) ? "YES" : "NO"))
//        infocells[]
//        infocells.append(InfoCell(key: "Volume and Speaker control", value: ""))
//        infocells.append(InfoCell(key: "Screen Brightness control", value: ""))
//        infocells.append(InfoCell(key: "IOS version", value: UIDevice.current.systemVersion))
//        infocells.append(InfoCell(key: "Check Dead Pixel", value: "", isButton: true))
        
        return [infos, wifi, advance]
    }
    
    func deviceInfo() -> DeviceInfo {
        return DeviceInfo(deviceName: UIDevice.modelName)
    }
    
    func checkVolumeControl() -> Bool {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(true)
            print(audioSession.outputVolume)
            return true
        }  catch {
           return false
       }
    }
    
    func checkSpeaker() -> Bool {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(true)
            print(audioSession.outputVolume)
            let currentRoute = audioSession.currentRoute
            if currentRoute.outputs.count == 0 {
                return false
            } else {
                for output in currentRoute.outputs {
                    print("\(output.portName) - \(output.portType)")
                }
            }
            return true
        } catch {
            return false
        }
    }
}

extension ScannerController {
    
    func checkFlashStatus() -> Bool {
        if let device = self.getDevice(position: .back) {
            return flashOn(device: device)
        }
        return false
    }
    
    func checkCameraStatus() -> Bool {
        self.session = AVCaptureSession()
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if UIImagePickerController.isCameraDeviceAvailable(.rear), authStatus != .authorized {
            return false
        }
        if let camera = self.getDevice(position: .back) {
            do {
                input = try AVCaptureDeviceInput(device: camera)
            } catch let error as NSError {
                print("==> Start camera error:")
                print(error)
                return false
            }
            
            if let input = self.input, session!.canAddInput(input) {
                print("==> Camera working")
                // Device working
                return true
            } else {
                print("==> Check camera: Can not use camera")
                // Can not use camera
            }
        } else {
            print("==> Check camera: Device not support camera")
            // Device not support camera
        }
        return false
    }
    
    //Get the device (Front or Back)
    func getDevice(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices: NSArray = AVCaptureDevice.devices() as NSArray;
        for de in devices {
            let deviceConverted = de as! AVCaptureDevice
            if(deviceConverted.position == position){
               return deviceConverted
            }
        }
       return nil
    }
    
    func flashOn(device:AVCaptureDevice) -> Bool {
        do {
            if (device.hasTorch) {
                try device.lockForConfiguration()
                device.torchMode = .on
                device.flashMode = .on
                device.torchMode = .off
                device.flashMode = .off
                device.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
            return false
        }
        return true
    }
}
