//
//  TransactionController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/6/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation

class TransactionController: TransactionHandler, TransactionCodeDelegate {
    var rootVC: BaseViewController?
    var delegate: TransactionDelegate?
    var dataSource: TransactionDataSource?
    
    init(rootVC: BaseViewController) {
        self.rootVC = rootVC
    }
    
    func checkTransactionCode(transactionCode: String) {
        self.rootVC?.showProgress()
        APIManager.shared.checkTransactionCode(transactionCode: transactionCode, delegate: self)
    }
    
    func onSuccessTransactionCode(response: TransactionCodeResponse) {
        self.rootVC?.hideProgress()
        self.dataSource?.onSuccess(response: response)
    }
    
    func onErrorTransactionCode(error: ResponseErrorModel) {
        self.rootVC?.hideProgress()
        self.dataSource?.onError(error: error)
    }
}
