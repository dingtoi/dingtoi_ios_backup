//  Created by boys vip on 5/23/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit
import DeviceKit
//import Crashlytics
import AVFoundation
import MediaPlayer
import AudioToolbox
import CoreData
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, AVAudioPlayerDelegate, AudioServicesPlaySystemSoundDelegate, SSASideMenuDelegate, UITabBarControllerDelegate {
    
    var window: UIWindow?
    internal var _rootNavigationController : UINavigationController?
    var rootNavigationController: UINavigationController {
        get {
            return _rootNavigationController!
        }
    }
    var applicationStateString: String {
        if UIApplication.shared.applicationState == .active {
            return "active"
        } else if UIApplication.shared.applicationState == .background {
            return "background"
        }else {
            return "inactive"
        }
    }
    
    static let API_KEY_Directions = "AIzaSyBZX4uT2O_hpVAJ9E8WBO7whde493u1xxY"
    let API_KEY = "AIzaSyAeuC7r4ku1aLRyRtvodBh-ywqy7JD1BMc"
    let testRequestIdentifier: String = "testIdentifier"
    
    //FCM
    func WriteLogError() {
        NSSetUncaughtExceptionHandler { exception in
            print("\(Thread.callStackSymbols.debugDescription)")
        }
        signal(SIGABRT) { _ in
            print("\(Thread.callStackSymbols.debugDescription)")
        }
        signal(SIGILL) { _ in
            print("\(Thread.callStackSymbols.debugDescription)")
        }
        signal(SIGSEGV) { _ in
            print("\(Thread.callStackSymbols.debugDescription)")
        }
        signal(SIGFPE) { _ in
            print("\(Thread.callStackSymbols.debugDescription)")
        }
        signal(SIGBUS) { _ in
            print("\(Thread.callStackSymbols.debugDescription)")
        }
        signal(SIGPIPE) { _ in
            print("\(Thread.callStackSymbols.debugDescription)")
        }
    }
    
    //CLSLog("message %@ %@", "one", "two")
    func CLSLog(_ format: String = "", _ args: CVarArg..., file: String = #file, function: String = #function, line: Int = #line) {
        let formatString: String!
        if let filename =  file.components(separatedBy: "/").last?.components(separatedBy: ".").first {
            formatString = "\(filename).\(function) line \(line) $ \(format)"
        } else {
            formatString = "\(file).\(function) line \(line) $ \(format)"
        }
        
//        let UUID = UIDevice.current.identifierForVendor?.uuidString
//        let userEmail = UserDefaults.standard.object(forKey: String.REMEMBER_USERNAME) as? String
//        let name = UserDefaults.standard.object(forKey: String.REMEMBER_NAME) as? String
//        Crashlytics.sharedInstance().setUserIdentifier(UUID ?? "Not Value User Identifier")
//        Crashlytics.sharedInstance().setUserEmail(userEmail ?? "Not Value User Email")
//        Crashlytics.sharedInstance().setUserName(name)
        
        logMessage(formatString)
        
//        #if targetEnvironment(simulator)
//        NSLogv("==> \(userEmail ?? "Test User Error") -==> %@", getVaList([formatString, args]))
//        #elseif DEBUG
//        CLSNSLogv("==> \(userEmail ?? "Test User Error") -==> %@", getVaList([formatString, args]))
//        #else
//        CLSLogv("==> \(userEmail ?? "Test User Error") -==> %@", getVaList([formatString, args]))
//        #endif
    }
    
    func logMessage(_ message: String) {
//        let userInfo = ["message" : message]
//        let error = NSError(domain: "TemplaceProject", code: 1, userInfo: userInfo)
//        Crashlytics.sharedInstance().recordCustomExceptionName("API Error", reason: message, frameArray: [])
//        Crashlytics.sharedInstance().recordError(error, withAdditionalUserInfo: userInfo)
    }
    
//    func initializeFCM(_ application: UIApplication) {
//        print("==> initialize____FCM")
//
//        if FirebaseApp.app() == nil {
//            FirebaseApp.configure() //Setup FCM && Firebase Crashes Report
//            Fabric.with([Crashlytics.self]) //Setup Crashlytics
//            Fabric.sharedSDK().debug = true //Open debug Crashlytics
//
//            let UUID = UIDevice.current.identifierForVendor?.uuidString
//            let userEmail = UserDefaults.standard.object(forKey: String.REMEMBER_USERNAME) as? String
//            let name = UserDefaults.standard.object(forKey: String.REMEMBER_NAME) as? String
//            Crashlytics.sharedInstance().setUserIdentifier(UUID ?? "Not Value User Identifier")
//            Crashlytics.sharedInstance().setUserEmail(userEmail ?? "Not Value User Email")
//            Crashlytics.sharedInstance().setUserName(name)
//        }
//
//        if #available(iOS 10.0, *) {  // enable new way for notifications on iOS 10
//            let center = UNUserNotificationCenter.current()
//            center.delegate = self // For iOS 10 display notification (sent via APNS)
//            center.requestAuthorization(options: [.alert, .sound]) { (accepted, error) in
//                if !accepted{
//                    print("==> Notification access denied.")
//                    let alert = UIAlertController(title: "Notification Access", message: "In order to use this application, turn on notification permissions.", preferredStyle: .alert)
//                    let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//                    alert.addAction(alertAction)
//                    self.window?.rootViewController?.present(alert , animated: true, completion: nil)
//                }else{
//                    print("==> Notification access accepted.")
//                    DispatchQueue.main.async {
//                        UIApplication.shared.registerForRemoteNotifications()
//                    }
//                }
//            }
//            Messaging.messaging().delegate = self // For iOS 10 data message (sent via FCM)
//        }
//        //IOS 8.0 - 9.0
//        let notificationCategory = UIMutableUserNotificationCategory()
//        let categories = Set<UIUserNotificationCategory>(arrayLiteral: notificationCategory)
//        let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .sound], categories: categories)
//        application.registerUserNotificationSettings(settings)
//
//
//        application.registerForRemoteNotifications()
//        DispatchQueue.main.async {
//            UIApplication.shared.registerForRemoteNotifications()
//        }
//        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
//    }
    
//    @objc func tokenRefreshNotification(_ notification: Notification) {
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("==> Error fetching remote instange ID: \(error)")
//            } else if let result = result {
//                print("==> InstanceID token: \(result.token)")
//                // Connect to FCM since connection may have failed when attempted before having a token.
//                self.ConnectToFCMSaveToken()
//            }
//        }
//    }
    
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("==> Refresh Token: \(String(describing: fcmToken))")
//        ConnectToFCMSaveToken()
//    }
    
//    func ConnectToFCMSaveToken() {
//        Messaging.messaging().shouldEstablishDirectChannel = true
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("==> Error fetching remote instange ID: \(error)")
//            } else if let result = result {
//                //MARK: Save Device Token in UserDefaults
//                let UUID = UIDevice.current.identifierForVendor?.uuidString
//                print("==> TokenFCM: " + result.token)
//                print("==> UUID: \(String(describing: UUID))")
//                UserDefaults.standard.set(result.token, forKey: "deviceToken") //setObject
//                UserDefaults.standard.set(UUID, forKey: "deviceID")
//                UserDefaults.standard.synchronize()
//
//                //Read
//                //UserDefaults.standard.string(forKey: "deviceToken")
//                //UserDefaults.standard.synchronize()
//
//                //Remove
//                //UserDefaults.standard.removeObject(forKey: "deviceToken")
//                //UserDefaults.standard.synchronize()
//            }
//        }
//    }
    
    // Receive data message on iOS 10 devices while app is in the background.
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        guard let data =
//            try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
//            let prettyPrinted = String(data: data, encoding: .utf8) else {
//                return
//        }
//        print("==> Received message:\n\(prettyPrinted)")
//        pushNotification("Thông Báo","Nội dung")
//    }
    
    // Receive data message on iOS 10 devices while app is in the foreground.
//    func application(received remoteMessage: MessagingRemoteMessage) {
//        guard let data =
//            try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
//            let prettyPrinted = String(data: data, encoding: .utf8) else {
//                return
//        }
//        print("==> Received message:\n\(prettyPrinted)")
//        pushNotification("Thông Báo","Nội dung")
//    }
    
    func pushNotification(_ title: String,_ body: String){
        if #available(iOS 10.0, *) {
            //Push Notification on IOS 10.0
            let content = UNMutableNotificationContent()
            content.title = title
            content.body = body
            content.sound = UNNotificationSound.default()
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest(identifier: self.testRequestIdentifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request, withCompletionHandler: {(error) in
                if let error = error {
                    print("==> Uh oh! i had an error: \(error)")
                }
            })
        } else {
            //Push Notification on IOS 8.0 - 9.0
            let notification = UILocalNotification()
            notification.alertAction = title
            notification.alertBody = body
            notification.fireDate = NSDate(timeIntervalSinceNow: 0) as Date
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.category = "INVITE_CATEGORY"
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
    
    //Receive Push Notification on IOS 8.0 - 9.0
    // iOS 8.0 - 9.0, called when received response (default open, dismiss or custom action) for a notification
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        print("1 -> didReceive notification: UILocalNotification")
    }
    
    //Receive Push Notification on IOS 8.0 - 9.0 ==> event click action
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: @escaping () -> Void) {
        print("2 -> action snooze notification handler when app in background")
        completionHandler()
    }
    
    //Receive Push Notification on IOS 8.0 - 9.0
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, withResponseInfo responseInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
        print("3 -> handleActionWithIdentifier UILocalNotification")
        completionHandler()
    }
    
    // iOS9, called when presenting notification in background
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if UIApplication.shared.applicationState == .active {
            print("==> Handle background notification")
            //Parsing userinfo:
            //if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
            //{
            //    let alertMsg = info["alert"] as! String
            //    var alert: UIAlertView!
            //    alert = UIAlertView(title: "", message: alertMsg, delegate: nil, cancelButtonTitle: "OK")
            //    alert.show()
            //}
        }else {
            print("==> Handle foreground notification")
        }
    }
    
    // iOS10+, called when presenting notification in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("==> ApplicationState willPresent: \(applicationStateString) willPresentNotification: \(userInfo)")
        //TODO: Handle foreground notification
        completionHandler([.alert, .sound])
    }
    
    // iOS10+, called when received response (default open, dismiss or custom action) for a notification
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("==> ApplicationState didReceive: \(applicationStateString) didReceiveResponse: \(userInfo)")
        //TODO: Handle background notification
        if response.notification.request.identifier == self.testRequestIdentifier {
            print("==> Click in Background")
        }else{
            print("==> Click in Foreground")
        }
        completionHandler()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Messaging.messaging().apnsToken = deviceToken
//        Messaging.messaging().isAutoInitEnabled = true
//        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
//        print("==> Device Token: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("==> Fail To Register For Remote Notifications With Error \(error.localizedDescription)")
    }
    //FCM./
    
    
//    func switchSlideMenu() {
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let sideMenu = SSASideMenu(contentViewController: UINavigationController(rootViewController: ScannerDetailViewController()), leftMenuViewController: LeftMenuVC(), rightMenuViewController: RightMenuVC())
//        sideMenu.backgroundImage = UIImage(named: "bgMenuRL.png")
//        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
//        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
//        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
//        sideMenu.delegate = self
//
//        self._rootNavigationController = UINavigationController(rootViewController: sideMenu)
//        self.window?.rootViewController = self._rootNavigationController
//        self.window?.makeKeyAndVisible()
//    }
    
    func sideMenuWillShowMenuViewController(_ sideMenu: SSASideMenu, menuViewController: UIViewController) {
        print("Will Show \(menuViewController)")
    }
    
    func sideMenuDidShowMenuViewController(_ sideMenu: SSASideMenu, menuViewController: UIViewController) {
        print("Did Show \(menuViewController)")
    }
    
    func sideMenuDidHideMenuViewController(_ sideMenu: SSASideMenu, menuViewController: UIViewController) {
        print("Did Hide \(menuViewController)")
    }
    
    func sideMenuWillHideMenuViewController(_ sideMenu: SSASideMenu, menuViewController: UIViewController) {
        print("Will Hide \(menuViewController)")
    }
    
    func sideMenuDidRecognizePanGesture(_ sideMenu: SSASideMenu, recongnizer: UIPanGestureRecognizer) {
        print("Did Recognize PanGesture \(recongnizer)")
    }
    
//    func switchTabbarVC() {
//        let transition = CATransition()
//        transition.duration = 1
//        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transition.type = kCATransitionReveal
//        transition.subtype = kCATransitionFromBottom
//        
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let navCon: UINavigationController = TabBarCustom_2.customIrregularityStyle(delegate: self)
//        navCon.view.layer.add(transition, forKey: nil)
//        self._rootNavigationController = navCon
//        self.window?.rootViewController = self._rootNavigationController
//        self.window?.makeKeyAndVisible()
//    }
    
    //UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        print("Selected: \(viewController.tabBarController?.selectedIndex ?? 0)-\(viewController.tabBarItem.title ?? "")")
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
    
//    func switchVietThucTabbarVC() {
//        let transition = CATransition()
//        transition.duration = 1
//        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transition.type = kCATransitionReveal
//        transition.subtype = kCATransitionFromBottom
//
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let navCon = UINavigationController(rootViewController: ExVietThucTabbarController())
//        self._rootNavigationController = navCon
//        self.window?.rootViewController = self._rootNavigationController
//        self.window?.makeKeyAndVisible()
//    }
    
//    func switchGuideNC() {
//        let transition = CATransition()
//        transition.duration = 1
//        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transition.type = kCATransitionReveal
//        transition.subtype = kCATransitionFromBottom
//
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let mainVC = MainVC()
//        mainVC.view.layer.add(transition, forKey: nil)
//        self._rootNavigationController = UINavigationController(rootViewController: mainVC)
//        self.window?.rootViewController = self._rootNavigationController
//        self.window?.makeKeyAndVisible()
//    }
    
    func switchLoginNC() {
    }
    
//    func switchMainVC() {
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let mainVC = MainVC()
//        self.window?.rootViewController = UINavigationController(rootViewController: mainVC)
//        self.window?.makeKeyAndVisible()
//    }
    
    func switchStartVC() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let startVC = StartViewController()
        self.window?.rootViewController = UINavigationController(rootViewController: startVC)
        self.window?.makeKeyAndVisible()
    }
    
    var launchOptions: [UIApplicationLaunchOptionsKey: Any]?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        print("==> Welcome")
        Language.initial()
        FirebaseApp.configure()
        //self.setupAlarmTime()
        
//        if Device.current.isOneOf(UIDevice.groupOfLargeX) {
//            UIApplication.shared.statusBarView?.backgroundColor = UIColor.rgb(fromHexString: Share._colorMenu)
//        }
        
        UserDefaults.standard.set("true", forKey: "skillapp")
        UserDefaults.standard.synchronize()
//        self.initializeFCM(application)
        
        //Google Map API
//        GMSServices.provideAPIKey(API_KEY)
//        GMSPlacesClient.provideAPIKey(API_KEY)
        //Google Map API ./
        
        //self.switchGuideNC()
        //self.switchSlideMenu()
        //self.switchTabbarVC()
//        self.switchVietThucTabbarVC()
        self.switchStartVC()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppWillTerminate), name: NSNotification.Name.UIApplicationWillTerminate, object:nil)
        
        // Handle notification
        self.launchOptions = launchOptions
        //if (launchOptions != nil) {
        //    if let localNotificationInfo = launchOptions?[UIApplicationLaunchOptionsKey.localNotification] as? UILocalNotification {
        //        // For local Notification
        //        showPendingView(userInfo: localNotificationInfo.userInfo)
        //    } else if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any]? {
        //        // For remote Notification
        //        //showPendingView(userInfo: remoteNotification) //Scheduler.checkIOS10 = true in Scheduler
        //        TypeActionNotification().Action(userInfo: remoteNotification)
        //    }
        //}
        
        KeyboardService.serviceSingleton = KeyboardService.init()
        return true
    }
    
    @objc func onAppWillTerminate(notification:NSNotification) {
        print("==> onAppWillTerminate")
        removeTokenSkillApp()
        sleep(3)
    }
    
    func removeTokenSkillApp(){
        UserDefaults.standard.set("true", forKey: "skillapp")
        UserDefaults.standard.synchronize()
        
        let skillapp = UserDefaults.standard.string(forKey: "skillapp")
        UserDefaults.standard.synchronize()
        print("==> Skill Application: \(String(describing: skillapp))")
        let deviceToken = UserDefaults.standard.string(forKey: "deviceToken")
        let deviceID = UserDefaults.standard.string(forKey: "deviceID")
        print("==> DeviceToken: \(String(describing: deviceToken))")
        print("==> DeviceID: \(String(describing: deviceID))")
        //Call API Logout ----
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        //audioPlayer?.pause()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //alarmScheduler.checkNotification() //Only true on IOS 8.0 - 9.0
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        print("==> didRegister  UIUserNotificationSettings ==> \(notificationSettings.types.rawValue)")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }
    
    //Alarm Time
    func setupAlarmTime() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, with: [.duckOthers, .defaultToSpeaker])
            try AVAudioSession.sharedInstance().setActive(true)
            UIApplication.shared.beginReceivingRemoteControlEvents()
        } catch let error as NSError{
            print("==> could not set & active session. err:\(error.localizedDescription)")
        }
    }
    
    func showPendingView(userInfo: [AnyHashable : Any]?) {
        print("==> ShowPendingView App is: \(self.applicationStateString)")
        if let notificationIdentifier = userInfo?["NotificationIdentifier"] as? String , notificationIdentifier == Scheduler.NotificationIdentifier || true {
            if self.applicationStateString == "active" {
                //let alarmTimeViewController = AlarmTimeViewController()
                //alarmTimeViewController.delegate = self
                //alarmTimeViewController.labelAlarm = userInfo?["label"] as? String ?? ""
                //alarmTimeViewController.locationAlarm = userInfo?["location"] as? String ?? ""
                //alarmTimeViewController.descriptionAlarm = userInfo?["description"] as? String ?? ""
                //let navigationController = UINavigationController(rootViewController: alarmTimeViewController)
                //self.window?.rootViewController?.present(navigationController, animated: true, completion: {})
            }else if self.applicationStateString == "background"{
                
            }else{//inactive
                
            }
        }
    }
    
    func hidePendingView() {
        audioPlayer?.stop()
        self.repeatCount = 0
    }
    
    func AlarmTimeNowWhenReceive(userInfo: [AnyHashable : Any]?) {
        var soundName: String = ""
        var index: Int = -1
        if let userInfo = userInfo {
            soundName = userInfo["soundName"] as? String ?? "bell"
            index = userInfo["index"] as? Int ?? 0
        }
        playSound(soundName)
        showPendingView(userInfo: userInfo)
        
        self.alarmModel = Alarms() //auto remove enabled = false
        self.alarmModel.alarms[index].enabled = false
    }
    
    func AlarmTimeNowWhenClickAction(identifier: String?, userInfo: [AnyHashable : Any]?) {
        var index: Int = -1
        var label: String = ""
        var description: String = ""
        var soundName: String = ""
        if let userInfo = userInfo {
            label = userInfo["label"] as? String ?? "Notification"
            description = userInfo["description"] as? String ?? "Description"
            soundName = userInfo["soundName"] as? String ?? "bell"
            index = userInfo["index"] as? Int ?? 0
        }
        self.alarmModel.alarms[index].onSnooze = false
        if identifier == "Alarm-ios-swift-snooze" {
            alarmScheduler.setNotificationForSnooze(idAlarm: "", label: label, location: "", description: description, snoozeMinute: 10, soundName: soundName, index: index)
            self.alarmModel.alarms[index].onSnooze = true
        }
        if identifier == "Alarm-ios-swift-stop" {
            audioPlayer?.stop()
            self.repeatCount = 0
        }
    }
    
    var audioPlayer: AVAudioPlayer?
    let alarmScheduler: AlarmSchedulerDelegate = Scheduler()
    var alarmModel: Alarms = Alarms()
    var repeatCount: Int = 30
    func playSound(_ soundName: String = "bell") {
        self.repeatCount = 30
        if #available(iOS 9.0, *) {
            AudioServicesPlaySystemSoundWithCompletion(kSystemSoundID_Vibrate) {
                self.audioServicesPlaySystemSoundCompleted(kSystemSoundID_Vibrate)
            }
        }else {
            let proc: AudioServicesSystemSoundCompletionProc = MyAudioServicesSystemSoundCompletionHandler
            AudioServicesAddSystemSoundCompletion(kSystemSoundID_Vibrate, nil, nil, proc, Unmanaged.passUnretained(self).toOpaque())
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        }
        
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: "bell", ofType: "mp3")!)
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer?.delegate = self
            audioPlayer?.prepareToPlay()
        } catch let error as NSError {
            audioPlayer = nil
            print("==> audioPlayer error \(error.localizedDescription)")
        }
        //negative number means loop infinity
        audioPlayer?.numberOfLoops = -1
        audioPlayer?.volume = 0.8
        audioPlayer?.play()
        
        let volumeView = MPVolumeView()
        volumeView.volumeSlider.value = 0.8
    }
    
    func audioServicesPlaySystemSoundCompleted(_ soundId: SystemSoundID) {
        repeatCount -= 1
        if #available(iOS 9.0, *) {
            if repeatCount > 0 {
                AudioServicesPlaySystemSoundWithCompletion(soundId) {
                    self.audioServicesPlaySystemSoundCompleted(soundId)
                }
            }
        } else {
            if repeatCount > 0 {
                AudioServicesPlaySystemSound(soundId)
            } else {
                AudioServicesRemoveSystemSoundCompletion(soundId)
            }
        }
    }
    //Alarm Time ./
    
    //RemoteConfig
//    func setupRemoteConfig() {
//        // Configure for dev mode, if needed
//        let remoteConfig = RemoteConfig.remoteConfig()
//        #if DEBUG
//        let expirationDuration: TimeInterval = 0
//        remoteConfig.configSettings = RemoteConfigSettings(developerModeEnabled: true)
//        #else
//        let expirationDuration: TimeInterval = 0//3600
//        #endif
//        
//        // default values
//        //let appDefaults: [String: NSObject] = [
//        //    "isRequest" : "true" as NSObject
//        //]
//        //remoteConfig.setDefaults(appDefaults)
//        if let path = Bundle.main.path(forResource: "RemoteConfig", ofType: "plist"),
//            let dict = NSDictionary(contentsOfFile: path) as? [String: NSObject] {
//            remoteConfig.setDefaults(dict)
//        }
//        
//        // set the values from the defaults
//        var isRequest = remoteConfig["isRequest"].boolValue
//        print("==> RemoteConfig value default isRequest: \(isRequest) ")
//        
//        // fetch the new values - Expires in 12h default
//        remoteConfig.fetch(withExpirationDuration: expirationDuration) { (status, error) in
//            print("==> RemoteConfig Fetch completed with status:", status, "(\(status.rawValue))")
//            if error == nil {
//                // activate the newly fetched values
//                remoteConfig.activateFetched()
//                // reset my variables
//                isRequest = remoteConfig["isRequest"].boolValue
//                //new value to update the UI
//                print("==> RemoteConfig value from firebase isRequest: \(isRequest) ")
//            }else {
//                print("==> RemoteConfig Don't get !")
//            }
//        }
//    }
    //RemoteConfig ./
    
    //Core Data stack
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "menke-dev.SimpleCam" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "TemplaceProject", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    //Core Data stack ./
 
    //Handle Incoming URLs - deeplinkMobiapps://
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:] ) -> Bool {
        print("==> deeplinkMobiapps: \(url)")
        
        // Determine who sent the URL.
        let sendingAppID = options[.sourceApplication]
        print("source application = \(sendingAppID ?? "Unknown")")
        
        // Process the URL.
        guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
            let path = components.path,
            let params = components.queryItems else {
                print("Invalid URL or album path missing")
                return false
        }
        
        if let index = params.first(where: { $0.name == "index" })?.value {
            print("path = \(path)")
            print("index = \(index)")
            return true
        } else {
            print("Photo index missing")
            return false
        }
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                print("==> deeplinkMobiapps: \(url)")
            }
        }
        return false
    }
    //Handle Incoming URLs ./
}

//Vibrate
func MyAudioServicesSystemSoundCompletionHandler(_ soundId: SystemSoundID, inClientData: UnsafeMutableRawPointer?) {
    let delegate = Unmanaged<AudioServicesPlaySystemSoundDelegate>.fromOpaque(inClientData!).takeUnretainedValue()
    delegate.audioServicesPlaySystemSoundCompleted(soundId)
}

@objc protocol AudioServicesPlaySystemSoundDelegate {
    func audioServicesPlaySystemSoundCompleted(_ soundId: SystemSoundID)
}
//Vibrate./

extension MPVolumeView {
    var volumeSlider: UISlider {
        self.showsRouteButton = false
        self.showsVolumeSlider = false
        self.isHidden = true
        var slider = UISlider()
        for subview in self.subviews {
            if subview.isKind(of: UISlider.self){
                slider = subview as! UISlider
                slider.isContinuous = false
                (subview as! UISlider).value = 1.0
                return slider
            }
        }
        return slider
    }
}

